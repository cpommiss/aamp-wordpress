<?php get_header(); ?>

<article class="container">
    <div class="row">
        <div class="content col-lg-8 col-md-12">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'parts/search/ajax/block', '' ); ?>
                <?php endwhile; ?>

                <?php
                    the_posts_pagination(
                        array(
                            'mid_size'  => 2,
                            'prev_text' => __( 'Previous', THEME__TEXTDOMAIN ),
                            'next_text' => __( 'Next', THEME__TEXTDOMAIN ),
                        )
                    );
                ?>
            <?php endif; ?>
        </div>
        <div class="sidebar col-lg-4 col-md-12">
            <?php dynamic_sidebar( 'sidebar-primary' ); ?>
        </div>
    </div>
</article>

<?php get_footer(); ?>