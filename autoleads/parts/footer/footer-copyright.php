
<span class="copyright"><?php echo sprintf( __( 'Copyright &copy; %s. All rights reserved. %s is a brand of AAMP Global.', THEME__TEXTDOMAIN ), date( 'Y' ), get_bloginfo( 'name' ) ); ?></span>
