<div class="navbar-utilities">
    <?php get_template_part( 'parts/navigation/navigation', 'social' ); ?>

    <form action="<?php echo \AAMP\Content\URL::get_shop_url(); ?>/search" class="form-inline">
        <input type="hidden" name="controller" value="search">
        <input type="hidden" name="orderby" value="position">
        <input type="hidden" name="orderway" value="desc">
        <input type="hidden" name="submit_search" value="Search">

        <input class="form-control mr-sm-2" name="search_query" type="text" placeholder="Search">
        <button type="submit">Search</button>
    </form>
</div>