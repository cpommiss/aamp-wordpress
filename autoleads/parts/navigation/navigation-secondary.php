
        <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'secondary',
                    'container'      => false,
                    'menu_class'     => 'navbar-nav navbar-secondary',
                    'walker'         => new \AAMP_Nav_Walker_Default()
                )
            )
        ?>

