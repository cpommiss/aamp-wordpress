<?php $image = ( ( has_post_thumbnail( get_the_ID() ) ) ? wp_get_attachment_image_url( get_post_thumbnail_id( get_the_ID() ), THEME__TEXTDOMAIN . '-square' ) : 'https://via.placeholder.com/600x600/1F355E/FFFFFF?text=no+image+available' ); ?>
<div class="search-tile post-tile result">
    <div class="post-tile__image" style="background-image: url(<?php echo $image; ?>);">
        <img src="<?php echo $image; ?>" alt="<?php echo get_the_title( get_the_ID() ); ?>">
    </div>
    <div class="post-tile__content">
        <h3 class="post-tile__title"><?php echo get_the_title( get_the_ID() ); ?></h3>

        <p><?php echo get_the_excerpt( get_the_ID() ); ?></p>

        <a href="<?php echo get_the_permalink( get_the_ID() ); ?>" class="post-tile__link"><?php _e( 'Read More', CHILD__TEXTDOMAIN ); ?></a>
    </div>
</div>