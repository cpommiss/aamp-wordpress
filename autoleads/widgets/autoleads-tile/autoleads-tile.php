<?php

    /*
    Widget Name: Autoleads - Content Tile
    Description: Creates a content tile.
    Author: Autoleads
    */

    class Autoleads_Tile extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-tile',

                __( CHILD__NAME . ' - Content Tile', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a content block.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(
                    'imagery'  => array(
                        'type'   => 'section',
                        'label'  => __( 'Imagery and Linkage', CHILD__TEXTDOMAIN ),
                        'hide'   => false,
                        'fields' => array(
                            'image'  => array(
                                'type'     => 'media',
                                'label'    => __( 'Choose an image', CHILD__TEXTDOMAIN ),
                                'choose'   => __( 'Choose image', CHILD__TEXTDOMAIN ),
                                'update'   => __( 'Set image', CHILD__TEXTDOMAIN ),
                                'library'  => 'image',
                                'fallback' => false
                            ),
                            'link'   => array(
                                'type'  => 'link',
                                'label' => __( 'Link to Page/URL', CHILD__TEXTDOMAIN )
                            ),
                            'target' => array(
                                'type'    => 'select',
                                'label'   => __( 'Link Behavior', CHILD__TEXTDOMAIN ),
                                'default' => '_self',
                                'options' => array(
                                    '_self'  => __( 'Open in same window', CHILD__TEXTDOMAIN ),
                                    '_blank' => __( 'Open in a new window', CHILD__TEXTDOMAIN )
                                )
                            )
                        )
                    ),
                    'headline' => array(
                        'type'   => 'section',
                        'label'  => __( 'Headline and Sub-headline', CHILD__TEXTDOMAIN ),
                        'hide'   => true,
                        'fields' => array(
                            'heading'    => array(
                                'type'  => 'text',
                                'label' => __( 'Headline/Title', CHILD__TEXTDOMAIN )
                            ),
                            'subheading' => array(
                                'type'  => 'textarea',
                                'label' => __( 'Sub-Headline', CHILD__TEXTDOMAIN ),
                                'rows'  => 4
                            ),
                            'style'      => array(
                                'type'    => 'select',
                                'label'   => __( 'Tile Style', CHILD__TEXTDOMAIN ),
                                'default' => '_self',
                                'options' => array(
                                    'style1' => __( 'Default', CHILD__TEXTDOMAIN ),
                                    'style2' => __( 'Show a "Learn More" button below the headline', CHILD__TEXTDOMAIN )
                                )
                            )
                        )
                    ),
                    'metrics'  => array(
                        'type'   => 'section',
                        'label'  => __( 'Metrics', CHILD__TEXTDOMAIN ),
                        'hide'   => true,
                        'fields' => array(
                            'min-height' => array(
                                'type'    => 'slider',
                                'label'   => __( 'Minimum Height (in pixels)', CHILD__TEXTDOMAIN ),
                                'default' => 350,
                                'min'     => 100,
                                'max'     => 1000
                            )
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-tile', __FILE__, 'Autoleads_Tile' );
