<?php
    $image      = $instance[ 'imagery' ][ 'image' ];
    $link       = sow_esc_url( $instance[ 'imagery' ][ 'link' ] );
    $target     = $instance[ 'imagery' ][ 'target' ];
    $min_height = ( ( isset( $instance[ 'metrics' ][ 'min-height' ] ) ) ? intval( $instance[ 'metrics' ][ 'min-height' ] ) : 0 );
?>
<div class="category-tile category-tile--<?php echo $instance[ 'headline' ][ 'style' ]; ?>">
    <a href="<?php echo $link; ?>" target="<?php echo $target; ?>" class="category-tile__inner">
        <h3 class="category-tile__title"><?php echo $instance[ 'headline' ][ 'heading' ]; ?></h3>

        <?php
            switch ( $instance[ 'headline' ][ 'style' ] ) :
                case 'style1' :
                    ?>
                    <p class="category-tile__description"><?php echo $instance[ 'headline' ][ 'subheading' ]; ?></p>
                    <?php
                    break;

                case 'style2' :
                    ?>
                    <span class="btn btn-default"><?php _e( 'Learn More', CHILD__TEXTDOMAIN ); ?></span>
                    <?php
                    break;
            endswitch;
        ?>
    </a>

    <div class="category-tile__background" style="background-image: url(<?php echo wp_get_attachment_image_url( $image, CHILD__TEXTDOMAIN . '-square' ); ?>);">&nbsp;</div>
</div>