<h3 class="widget__title">
    <a href="#">
        <?php _e( 'Keep In Touch', CHILD__TEXTDOMAIN ); ?> <span class="widget__subtitle"><?php _e( 'Join Our Mailing List', CHILD__TEXTDOMAIN ); ?></span>
    </a>
</h3>
<div class="widget__content">
    <iframe src="<?php echo get_bloginfo( 'url' ); ?>/gfembed/?f=2" width="100%" height="200" frameBorder="0" class="gfiframe"></iframe>
    <script src="<?php echo get_bloginfo( 'url' ); ?>/content/plugins/gravity-forms-iframe-develop/assets/scripts/gfembed.min.js" type="text/javascript"></script>
</div>