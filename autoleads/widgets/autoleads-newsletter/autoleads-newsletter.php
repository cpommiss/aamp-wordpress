<?php

    /*
    Widget Name: Autoleads - Newsletter Widget
    Description: Creates a newsletter subscription widget.
    Author: Autoleads
    */

    class Autoleads_Newsletter extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-newsletter',

                __( CHILD__NAME . ' - Newsletter Widget', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a newsletter subscription widget.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-newsletter', __FILE__, 'Autoleads_Newsletter' );
