<?php

    /*
    Widget Name: Autoleads - Video Carousel
    Description: Creates a carousel of video links.
    Author: Autoleads
    */

    class Autoleads_Video_Carousel extends SiteOrigin_Widget {
        public static $slick_settings = array(
            'slidesToShow'      => 3,
            'slidesToScroll'    => 3,
            'dots'              => false,
            'arrows'            => true,
            'responsive'        => array(
                array(
                    'breakpoint'    => 1200,
                    'settings'      => array(
                        'slidesToShow'      => 2,
                        'slidesToScroll'    => 2
                    )
                ),
                array(
                    'breakpoint'    => 992,
                    'settings'      => array(
                        'slidesToShow'      => 1,
                        'slidesToScroll'    => 1
                    )
                )
            )
        );

        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-video-carousel',

                __( CHILD__NAME . ' - Video Carousel', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a carousel of video links.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(
                    'videos' => array(
                        'type'       => 'repeater',
                        'label'      => __( 'Videos', CHILD__TEXTDOMAIN ),
                        'item_name'  => __( 'Video', CHILD__TEXTDOMAIN ),
                        'item_label' => array(
                            'selector'     => "[id*='heading']",
                            'update_event' => 'change',
                            'value_method' => 'val'
                        ),
                        'fields'     => array(
                            'imagery' => array(
                                'type'   => 'section',
                                'label'  => __( 'Imagery and Linkage', CHILD__TEXTDOMAIN ),
                                'hide'   => false,
                                'fields' => array(
                                    'image' => array(
                                        'type'     => 'media',
                                        'label'    => __( 'Image poster/preview', CHILD__TEXTDOMAIN ),
                                        'choose'   => __( 'Choose image', CHILD__TEXTDOMAIN ),
                                        'update'   => __( 'Set image', CHILD__TEXTDOMAIN ),
                                        'library'  => 'image',
                                        'fallback' => false
                                    ),
                                    'link-hosted' => array(
                                        'type'     => 'media',
                                        'label'    => __( 'Self-hosted Video', CHILD__TEXTDOMAIN ),
                                        'choose'   => __( 'Choose video', CHILD__TEXTDOMAIN ),
                                        'update'   => __( 'Set video', CHILD__TEXTDOMAIN ),
                                        'library'  => 'video',
                                        'fallback' => false
                                    ),
                                    'link'  => array(
                                        'type'  => 'link',
                                        'label' => __( 'Link to External Video URL', CHILD__TEXTDOMAIN )
                                    ),
                                )
                            ),
                            'text'    => array(
                                'type'   => 'section',
                                'label'  => __( 'Text', CHILD__TEXTDOMAIN ),
                                'hide'   => false,
                                'fields' => array(
                                    'heading' => array(
                                        'type'  => 'textarea',
                                        'label' => __( 'Headline/Title', CHILD__TEXTDOMAIN ),
                                        'rows' => 2
                                    )
                                )
                            )
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-video-carousel', __FILE__, 'Autoleads_Video_Carousel' );
