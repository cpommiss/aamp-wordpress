<?php if ( ( isset( $instance[ 'videos' ] ) ) && ( !empty( $instance[ 'videos' ] ) ) && ( count( $instance[ 'videos' ] ) ) ) : ?>
    <div class="video-tile-carousel" data-slick='<?php echo json_encode( Autoleads_Video_Carousel::$slick_settings ); ?>'>
    <?php foreach ( $instance[ 'videos' ] as $video ) : ?>
        <?php
            $poster         = $video[ 'imagery' ][ 'image' ];
            $video_id       = 'video-carousel-' . uniqid();
            $video_hosted   = intval( $video[ 'imagery' ][ 'link-hosted' ] );
            $video_url      = sow_esc_url( $video[ 'imagery' ][ 'link' ] );
        ?>
        <div class="video-tile">
            <?php if ( strlen( $video[ 'text' ][ 'heading' ] ) ) : ?><h3 class="video-tile__title"><?php echo nl2br( $video[ 'text' ][ 'heading' ] ); ?></h3><?php endif; ?>

            <div class="video-tile__inner">
                <a href="<?php echo ( ( $video_hosted ) ? ( '#' . $video_id ) : $video_url ); ?>" target="_blank" class="<?php echo ( ( $video_hosted ) ? 'afterglow' : 'fresco' ); ?> video-tile__link">
                    <div class="video-tile__card">
                        <span class="video-tile__button">
                            <?php _e( 'Play Video', CHILD__TEXTDOMAIN ); ?>
                        </span>
                    </div>
                </a>

                <div class="video-tile__background" style="background-image: url(<?php echo wp_get_attachment_image_url( $poster, CHILD__TEXTDOMAIN . '-square' ); ?>);">
                    <?php if ( $video_hosted ) : ?>
                        <?php $video_meta = wp_get_attachment_metadata( $video_hosted ); ?>

                        <?php if ( ( $video_meta ) && ( !empty( $video_meta ) ) ) : ?>
                            <video id="<?php echo $video_id; ?>" width="<?php echo $video_meta[ 'width' ]; ?>" height="<?php echo $video_meta[ 'height' ]; ?>" class="hidden">
                                <source type="<?php echo $video_meta[ 'mime_type' ]; ?>" src="<?php echo wp_get_attachment_url( $video_hosted ); ?>" />
                            </video>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach ?>
    </div>
<?php endif; ?>