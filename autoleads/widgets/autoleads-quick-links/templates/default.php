<div class="quick-links__outer" style="background-color: <?php echo $instance[ 'design' ][ 'bgcolor' ]; ?>; color: <?php echo $instance[ 'design' ][ 'color' ]; ?>;">
    <label class="quick-links__title" for="quick-links__menu"><?php echo $instance[ 'design' ][ 'heading' ]; ?></label>

    <div class="quick-links__menu-wrapper">
        <select id="quick-links__menu" class="quick-links__menu form-control">
            <option value=""><?php echo $instance[ 'design' ][ 'placeholder' ]; ?></option>
            <?php foreach ( $instance[ 'links' ][ 'links' ] as $link ) : ?>
                <option value="<?php echo $link[ 'url' ]; ?>"><?php echo $link[ 'title' ]; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>