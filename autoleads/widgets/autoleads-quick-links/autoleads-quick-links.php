<?php

    /*
    Widget Name: Autoleads - Quick Links
    Description: Creates a quick links bar with fast, easy access to various areas of the site.
    Author: Autoleads
    */

    class Autoleads_Quick_Links extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-quick-links',

                __( CHILD__NAME . ' - Quick Links', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a quick links bar with fast, easy access to various areas of the site.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(
                    'design' => array(
                        'type'   => 'section',
                        'label'  => __( 'Design and Style', CHILD__TEXTDOMAIN ),
                        'hide'   => false,
                        'fields' => array(
                            'heading' => array(
                                'type'  => 'text',
                                'label' => __( 'Title', CHILD__TEXTDOMAIN ),
                                'default' => 'I\'m looking for...'
                            ),
                            'placeholder' => array(
                                'type'  => 'text',
                                'label' => __( 'Quick Link Drop-down Placeholder Text', CHILD__TEXTDOMAIN ),
                                'default' => 'Choose Solution'
                            ),
                            'bgcolor' => array(
                                'type' => 'color',
                                'label' => __( 'Background Color', CHILD__TEXTDOMAIN ),
                                'default' => '#1f355e'
                            ),
                            'color' => array(
                                'type' => 'color',
                                'label' => __( 'Text Color', CHILD__TEXTDOMAIN ),
                                'default' => '#ffffff'
                            )
                        )
                    ),
                    'links' => array(
                        'type'   => 'section',
                        'label'  => __( 'Quick Links', CHILD__TEXTDOMAIN ),
                        'hide'   => false,
                        'fields' => array(
                            'links' => array(
                                'type'       => 'repeater',
                                'label'      => __( 'Links', CHILD__TEXTDOMAIN ),
                                'item_name'  => __( 'Link', CHILD__TEXTDOMAIN ),
                                'item_label' => array(
                                    'selector'     => "[id*='title']",
                                    'update_event' => 'change',
                                    'value_method' => 'val'
                                ),
                                'fields' => array(
                                    'title' => array(
                                        'type'  => 'text',
                                        'label' => __( 'Title', CHILD__TEXTDOMAIN ),
                                    ),
                                    'url'   => array(
                                        'type'  => 'link',
                                        'label' => __( 'URL/Page to Link To', CHILD__TEXTDOMAIN )
                                    )
                                )
                            )
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-quick-links', __FILE__, 'Autoleads_Quick_Links' );
