<?php

    /*
    Widget Name: Autoleads - Call-to-Action (Search by Vehicle)
    Description: Creates a search-by-vehicle call to action.
    Author: Autoleads
    */

    class Autoleads_CTA_SBV extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-cta-sbv',

                __( CHILD__NAME . ' - Call-to-Action (Search by Vehicle)', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a content block.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-cta-sbv', __FILE__, 'Autoleads_CTA_SBV' );
