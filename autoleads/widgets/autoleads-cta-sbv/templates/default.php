<div class="cta-sbv">
    <h3 class="text-center"><?php _e( 'Search by Vehicle', CHILD__TEXTDOMAIN ); ?></h3>

    <form class="cta-sbv__search" action="<?php echo \AAMP\Content\URL::get_shop_url(); ?>/" method="get" data-api-region="2" data-api-url="<?php bloginfo( 'url' ); ?>/rest/">
        <input type="hidden" name="fc" value="module">
        <input type="hidden" name="module" value="aampsearchbyvehicle">
        <input type="hidden" name="controller" value="results">
        <input type="hidden" name="aamp-sbv-region" value="2">
        <input type="hidden" name="aamp-sbv-trim" value="">

        <ul>
            <li>
                <select name="aamp-sbv-make" disabled="disabled" data-blank="<?php _e( 'Make', CHILD__TEXTDOMAIN ); ?>" data-loading="<?php _e( 'Loading', CHILD__TEXTDOMAIN ); ?>" data-select="<?php _e( 'Select Make', CHILD__TEXTDOMAIN ); ?>">
                    <option value=""><?php _e( 'Make', CHILD__TEXTDOMAIN ); ?></option>
                </select>
            </li>
            <li>
                <select name="aamp-sbv-model" disabled="disabled" data-blank="<?php _e( 'Model', CHILD__TEXTDOMAIN ); ?>" data-loading="<?php _e( 'Loading', CHILD__TEXTDOMAIN ); ?>" data-select="<?php _e( 'Select Model', CHILD__TEXTDOMAIN ); ?>">
                    <option value=""><?php _e( 'Model' , CHILD__TEXTDOMAIN ); ?></option>
                </select>
            </li>
            <li>
                <select name="aamp-sbv-year" disabled="disabled" data-blank="<?php _e( 'Year', CHILD__TEXTDOMAIN ); ?>" data-loading="<?php _e( 'Loading', CHILD__TEXTDOMAIN ); ?>" data-select="<?php _e( 'Select Year', CHILD__TEXTDOMAIN ); ?>">
                    <option value=""><?php _e( 'Year', CHILD__TEXTDOMAIN ); ?></option>
                </select>
            </li>
            <li>
                <button type="submit" class="btn btn-inverse"><?php _e( 'Search', CHILD__TEXTDOMAIN ); ?></button>
            </li>
        </ul>
    </form>
</div>
