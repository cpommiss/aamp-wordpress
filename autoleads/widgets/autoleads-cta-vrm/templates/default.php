<div class="cta-vrm">
    <h3 class="text-center"><?php _e( 'Vehicle Registration Lookup', CHILD__TEXTDOMAIN ); ?></h3>

    <p><?php echo $instance[ 'caption' ]; ?></p>

    <form class="cta-vrm__search" action="<?php echo sow_esc_url( $instance[ 'page' ] ); ?>" method="post">
        <input type="hidden" name="region" value="<?php echo intval( $instance[ 'region' ] ); ?>">

        <ul>
            <li class="cta-vrm__search-input">
                <input name="vrm" type="text" placeholder="<?php _e( 'Registration Number', CHILD__TEXTDOMAIN ); ?>">
                <span class="cta-vrm__search-input-note"><?php _e( '* UK Vehicle Registrations Only', CHILD__TEXTDOMAIN ); ?></span>
            </li>
            <li class="cta-vrm__search-button">
                <button type="submit" class="btn btn-inverse"><?php _e( 'Search', CHILD__TEXTDOMAIN ); ?></button>
            </li>
        </ul>
    </form>
</div>
