<?php

    /*
    Widget Name: Autoleads - Call-to-Action (Search by VRM)
    Description: Creates a search-by-VRM call to action.
    Author: Autoleads
    */

    class Autoleads_CTA_VRM extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-cta-vrm',

                __( CHILD__NAME . ' - Call-to-Action (Search by VRM)', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a search-by-VRM call to action.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(
                    'caption' => array(
                        'type'  => 'textarea',
                        'label' => __( 'Caption/Instructions', CHILD__TEXTDOMAIN ),
                        'rows'  => 4
                    ),
                    'region' => array(
                        'type'    => 'select',
                        'label'   => __( 'Region', CHILD__TEXTDOMAIN ),
                        'description' => __( 'Select the region of the Vehicle Configuration database to match the vehicle against.', CHILD__TEXTDOMAIN ),
                        'options' => array(
                            '1'   => __( 'North America', CHILD__TEXTDOMAIN ),
                            '2'     => __( 'Europe', CHILD__TEXTDOMAIN )
                        )
                    ),
                    'page' => array(
                        'type'  => 'link',
                        'label' => __( 'VRM Lookup Page', CHILD__TEXTDOMAIN ),
                        'description' => __( 'Select the page that contains the VRM Lookup widget which can display the results of the VRM lookup.', CHILD__TEXTDOMAIN )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-cta-vrm', __FILE__, 'Autoleads_CTA_VRM' );
