<?php

    /**
     * Asset-related Methods
     *
     * @package WordPress
     * @subpackage Autoleads
     * @since 1.0
     */
    namespace Autoleads\Assets;

    class Core {
        function __construct() {
            add_action( 'wp_enqueue_scripts', array( '\Autoleads\Assets\Core', 'enqueue_public' ) );
            add_action( 'admin_enqueue_scripts', array( '\Autoleads\Assets\Core', 'enqueue_admin' ) );
        }

        function __destruct() {
        }

        /**
         * Enqueue scripts and styles.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               x
         *
         * @since 1.0
         */
        public static function enqueue_public() {
            wp_enqueue_style( CHILD__TEXTDOMAIN . '-theme', get_stylesheet_directory_uri() . '/assets/css/theme.css', array( THEME__TEXTDOMAIN . '-bootstrap', THEME__TEXTDOMAIN . '-theme' ), CHILD__VERSION );

            \Autoleads\JSCore\Core::enqueue();
        }

        /**
         * Enqueue administration/dashboard scripts and styles.
         *
         * @since 1.0
         */
        public static function enqueue_admin( $hook ) {
            \Autoleads\JSCore\Core::enqueue( false );
        }
    }

