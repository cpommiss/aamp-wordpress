<?php

    /**
     * SiteOrigin Panels-related methods
     *
     * @package WordPress
     * @subpackage Autoleads
     * @since 1.0
     */
    namespace Autoleads\SiteOrigin;

    class Core {
        function __construct() {
            add_filter( 'siteorigin_panels_css_object', array( '\Autoleads\SiteOrigin\Core', 'filter_css_object' ), 20, 4 );
        }

        function __destruct() {
        }

        public static function filter_css_object( $css, $panels_data, $post_id, $layout ) {
            if ( empty( $layout ) ) {
                return $css;
            }

            foreach ( $layout as $ri => $row ) {
                if ( empty( $row[ 'cells' ] ) ) continue;

                foreach ( $row[ 'cells' ] as $ci => $cell ) {
                    if ( empty( $cell[ 'widgets' ] ) ) continue;

                    foreach ( $cell['widgets'] as $wi => $widget ) {
                        if ( ! empty( $widget[ 'panels_info' ][ 'style' ][ 'font_color' ] ) ) {
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h1:after', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ],
                                'background-color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h2:after', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ],
                                'background-color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h3:after', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ],
                                'background-color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h4:after', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ],
                                'background-color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h5:after', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ],
                                'background-color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h6:after', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ],
                                'background-color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                        }
                    }
                }
            }

            return $css;
        }
    }

