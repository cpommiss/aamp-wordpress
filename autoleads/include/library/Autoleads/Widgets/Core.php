<?php

    /**
     * Widget Area-related Methods
     *
     * @package WordPress
     * @subpackage Autoleads
     * @since 1.0
     */
    namespace Autoleads\Widgets;

    class Core {
        function __construct() {
            add_action( 'widgets_init', array( '\Autoleads\Widgets\Core', 'init' ) );
        }

        function __destruct() {
        }

        /**
         * Register widget area.
         *
         * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
         *
         * @since 1.0
         */
        public static function init() {
            register_sidebar( array(
                'name'          => __( 'Blog (Above)', THEME__TEXTDOMAIN ),
                'id'            => 'sidebar-blog-before',
                'description'   => __( 'Add widgets here to appear before blog/news articles on the News/Blog page.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );

            register_sidebar( array(
                'name'          => __( 'Blog (Below)', THEME__TEXTDOMAIN ),
                'id'            => 'sidebar-blog-after',
                'description'   => __( 'Add widgets here to appear after blog/news articles on the News/Blog page.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );

            register_sidebar( array(
                'name'          => __( 'Header', THEME__TEXTDOMAIN ),
                'id'            => 'sidebar-header',
                'description'   => __( 'Add widgets here to appear in the header area of all pages.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );
        }
    }