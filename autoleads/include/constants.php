<?php
    /**
     * Theme Constants
     *
     * @package WordPress
     * @subpackage Autoleads
     * @since 1.0
     */

    define( 'CHILD__NAME', 'Autoleads' );
    define( 'CHILD__TEXTDOMAIN', 'autoleads' );
    define( 'CHILD__VERSION', '1.0.0' );

