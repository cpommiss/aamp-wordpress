if ( typeof Autoleads !== 'object' ) var Autoleads = {};

Autoleads.Core = ( function() {
    // queue initialization routines
    jQuery( document ).ready( function() {
        for ( var Module in Autoleads ) {
            if ( ( Autoleads[ Module ].hasOwnProperty( 'init' ) ) && ( typeof Autoleads[ Module ].init === 'function' ) ) {
                Autoleads[ Module ].init();
            }
        }
    } );

    return {
    }
} )();
