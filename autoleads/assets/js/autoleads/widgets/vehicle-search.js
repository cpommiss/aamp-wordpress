if ( typeof Autoleads !== 'object' ) var Autoleads = {};

Autoleads.VehicleSearch = ( function() {
    var container;

    var api_url;
    var api_region;

    var make;
    var model;
    var year;
    var trim;
    var submit;

    var set_field_state = function( field, message, disabled ) {
        field.empty().append( '<option value="" selected="selected">' + message + '</option>' );

        if ( disabled ) {
            field.attr( 'disabled', true );
        } else {
            field.attr( 'disabled', false );
        }
    };

    var populate_makes = function( data ) {
        if ( ( data ) && ( data.length ) ) {
            set_field_state( make, make.data( 'select' ), false );

            for ( var counter = 0; counter < data.length; counter++ ) {
                make.append( '<option value="' + data[ counter ].id_make + '">' + data[ counter ].name + '</option>' );
            }
        } else {
            set_field_state( make, make.data( 'blank' ), true );
        }
    };

    var populate_models = function( data ) {
        if ( ( data ) && ( data.length ) ) {
            set_field_state( model, model.data( 'select' ), false );

            for ( var counter = 0; counter < data.length; counter++ ) {
                model.append( '<option value="' + data[ counter ].name + '">' + data[ counter ].name + '</option>' );
            }
        } else {
            set_field_state( model, model.data( 'blank' ), true );
        }
    };

    var populate_years = function( data ) {
        if ( ( data ) && ( data.length ) ) {
            set_field_state( year, year.data( 'select' ), false );

            for ( var counter = 0; counter < data.length; counter++ ) {
                year.append( '<option value="' + data[ counter ].id_model + '">' + data[ counter ].year + '</option>' );
            }
        } else {
            set_field_state( year, year.data( 'blank' ), true );
        }
    };

    var populate_trim = function( data ) {
        if ( ( data ) && ( data.trims ) && ( data.trims.length ) ) {
            submit.attr( 'disabled', false );

            console.log( data );

            for ( var counter = 0; counter < data.trims.length; counter++ ) {
                if ( ( data.trims[ counter ].name.toString().toLowerCase() === 'all trim levels' ) || ( data.trims[ counter ].default === 1 ) ) {
                    console.log( 'found', data.trims[ counter ] );

                    trim.val( data.trims[ counter ].id_trim );
                    break;
                }
            }
        }
    };

    var fetch_models = function() {
        var id_make     = parseInt( make.val() );

        submit.attr( 'disabled', true );
        trim.val( '' );

        set_field_state( model, model.data( 'loading' ), true );
        set_field_state( year, year.data( 'blank' ), true );

        if ( !isNaN( id_make ) ) {
            jQuery.get( api_url + 'compatible/vehicles/models/' + id_make + '/region/' + api_region,
                        populate_models,
                        'json' );
        } else {
            set_field_state( model, model.data( 'blank' ), true );
        }
    };

    var fetch_years = function() {
        var id_make     = parseInt( make.val() );
        var model_name  = model.val();

        submit.attr( 'disabled', true );
        trim.val( '' );

        set_field_state( year, year.data( 'loading' ), true );

        if ( ( !isNaN( id_make ) ) && ( model_name.length ) ) {
            jQuery.post(    api_url + 'compatible/vehicles/models/' + id_make + '/years/region/' + api_region,
                            {
                                model: model_name
                            },
                            populate_years,
                            'json' );
        } else {
            set_field_state( year, model.data( 'blank' ), true );
        }
    };

    var fetch_trims = function() {
        var id_model    = parseInt( year.val() );

        submit.attr( 'disabled', true );
        trim.val( '' );

        if ( !isNaN( id_model ) ) {
            jQuery.get( api_url + 'compatible/vehicles/trims/' + id_model,
                        populate_trim,
                        'json' );
        }
    };

    var init = function() {
        container       = jQuery( '.cta-sbv__search' );

        if ( container.length ) {
            api_url         = container.data( 'api-url' );
            api_region      = parseInt( container.data( 'api-region' ) );

            if ( ( api_url ) && ( api_region ) && ( api_url.length ) && ( !isNaN( api_region ) ) ) {
                make            = jQuery( '[name="aamp-sbv-make"]', container );
                model           = jQuery( '[name="aamp-sbv-model"]', container );
                year            = jQuery( '[name="aamp-sbv-year"]', container );
                trim            = jQuery( '[name="aamp-sbv-trim"]', container );
                submit          = jQuery( 'button[type="submit"]', container );

                submit.attr( 'disabled', true );

                set_field_state( make, make.data( 'loading' ), true );
                set_field_state( model, model.data( 'blank' ), true );
                set_field_state( year, year.data( 'blank' ), true );

                jQuery.get( api_url + 'compatible/vehicles/makes/0/region/' + api_region,
                            populate_makes,
                            'json' );

                make.on( 'change', fetch_models );
                model.on( 'change', fetch_years );
                year.on( 'change', fetch_trims );
            }
        }
    };

    return {
        init: init
    };
} )();