if ( typeof Autoleads !== 'object' ) var Autoleads = {};

Autoleads.WidgetNewsletter = ( function() {
    var content;

    var handle_click = function( event ) {
        event.preventDefault();

        if ( content.hasClass( 'expanded' ) ) {
            content.removeClass( 'expanded' );

            content.slideUp( 500, 'easeOutExpo' );
        } else {
            content.addClass( 'expanded' );

            content.slideDown( 500, 'easeOutExpo' );
        }
    };

    var init = function() {
        if ( jQuery( '.widget_autoleads-newsletter' ).length ) {
            content         = jQuery( '.widget_autoleads-newsletter .widget__content' );

            jQuery( '.widget_autoleads-newsletter .widget__title a' ).on( 'click', handle_click );
        }
    };

    return {
        init: init
    };
} )();