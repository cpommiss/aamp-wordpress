if ( typeof Autoleads !== 'object' ) var Autoleads = {};

Autoleads.QuickLinks = ( function() {
    var handle_change = function( event ) {
        event.preventDefault();

        var url     = jQuery( this ).val();

        if ( ( url ) && ( url.length ) ) {
            window.location = url;
        }
    };

    var init = function() {
        if ( jQuery( '.widget_autoleads-quick-links' ).length ) {
            jQuery( '.widget_autoleads-quick-links select' ).on( 'change', handle_change );
        }
    };

    return {
        init: init
    };
} )();