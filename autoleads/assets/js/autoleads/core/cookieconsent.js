if ( typeof Autoleads !== 'object' ) var Autoleads = {};

Autoleads.CookieConsent = ( function() {
    var setup_consent = function() {
        window.cookieconsent.initialise(
            {
                'layout': 'autoleads',
                'position': 'bottom',
                'layouts': {
                    'autoleads': '<div class="cc-window__layout"><div class="cc-window__layout--text">{{header}}{{messagelink}}</div><div class="cc-window__layout--compliance">{{compliance}}</div></div>'
                },
                'content': {
                    'header': 'Autoleads utilizes cookies',
                    'message': 'Our website uses anonymous cookies to deliver vehicle, product and map search functionality. We do not share or sell this information with anybody.',
                    'dismiss': 'Got it!',
                    'allow': 'Allow cookies',
                    'deny': 'Decline',
                    'link': 'Learn more',
                    'href': 'https://cookiesandyou.com/',
                    'close': '&#x274c;',
                },
                'palette': {
                    'popup': {
                        'background': '#1f355e',
                        'text': '#fff',
                        'link': '#ddd'
                    }
                },
                'elements': {
                    'header': '<span class="cc-window__header">{{header}}</span>',
                    'message': '<p id="cookieconsent:desc" class="cc-window__message">{{message}}</p>',
                    'messagelink': '<p id="cookieconsent:desc" class="cc-window__message">{{message}} <a href="{{href}}" target="_blank" tabindex="0" aria-label="Learn more about cookies">{{link}}</a></p>',
                    'dismiss': '<a href="javascript:;" class="btn btn-secondary cc-btn cc-dismiss" tabindex="0" aria-label="Dismiss cookie message">{{dismiss}}</a>',
                    'allow': '<a href="javascript:;" class="btn btn-secondary cc-btn cc-deny" tabindex="0" aria-label="Allow cookies">{{allow}}</a>',
                    'deny': '<a href="javascript:;" class="btn btn-secondary btn-danger cc-btn cc-deny" tabindex="0" aria-label="Disallow cookies">{{deny}}</a>',
                    'link': '<a href="{{href}}" target="_blank" tabindex="0" aria-label="Learn more about cookies">{{link}}</a>',
                    'close': '<span class="cc-window__close" tabindex="0" aria-label="Dismiss cookie message">{{close}}</span>'
                },
                'compliance': {
                    'info': '<div class="cc-window__compliance">{{dismiss}}</div>',
                    'opt-in': '<div class="cc-window__compliance cc-window__compliance--highlight">{{dismiss}}{{allow}}</div>',
                    'opt-out': '<div class="cc-window__compliance cc-window__compliance--highlight">{{deny}}{{dismiss}}</div>',
                }, 
                'cookie': {
                    'domain': '.autoleads.co.uk'
                },
            }
        );
    };

    var init = function() {
        setup_consent();
    };

    return {
        init: init
    };
} )();