<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'parts/page/page', 'content' ); ?>
    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'parts/none/none' ); ?>
<?php endif; ?>

<?php get_footer(); ?>
