<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <section data-load="post" data-load-per-page="10" data-load-current-page="1" class="section post-content post-blocks">
        <div data-load-results="default" class="post-content__results">
        </div>
    </section>
<?php else : ?>
    <?php get_template_part( 'parts/none/none', 'post' ); ?>
<?php endif; ?>

<?php get_footer(); ?>
