<?php get_header(); ?>

<?php get_template_part( 'parts/header/header', 'featured_image' ); ?>

<?php if ( have_posts() ) : ?>
    <h2><?php _e( 'Search Results', THEME__TEXTDOMAIN ); ?></h2>

    <div class="search-results">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'parts/search/search', 'content' ); ?>
        <?php endwhile; ?>
    </div>

    <?php
        the_posts_pagination(
            array(
                'mid_size'  => 2,
                'prev_text' => __( 'Previous', THEME__TEXTDOMAIN ),
                'next_text' => __( 'Next', THEME__TEXTDOMAIN ),
            )
        );
    ?>
<?php endif; ?>

<?php get_footer(); ?>