<?php
    /**
     * Theme Initialization
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */

    /**
     * Initialize theme.
     *
     * @since 1.0
     */
    function __theme_setup() {
        load_theme_textdomain( THEME__TEXTDOMAIN );

        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );

        $sizes      = array(
            'square' => array( 'width' => 600, 'height' => 600, 'crop' => true, '2x' => true ),
            'featured' => array( 'width' => 1920, 'height' => 1080, 'crop' => false, '2x' => false ),
            'featured-short' => array( 'width' => 1920, 'height' => 592, 'crop' => false, '2x' => false )
        );

        foreach ( $sizes as $size => $details ) {
            add_image_size( THEME__TEXTDOMAIN . '-' . $size, $details[ 'width' ], $details[ 'height' ], $details[ 'crop' ] );

            if ( $details[ '2x' ] ) {
                add_image_size( THEME__TEXTDOMAIN . '-' . $size . '-2x', $details[ 'width' ] * 2, $details[ 'height' ] * 2, $details[ 'crop' ] );
            }
        }

        register_nav_menus( array(
                                'primary'       => __( 'Primary Navigation Menu', THEME__TEXTDOMAIN ),
                                'epilogue'      => __( 'Footer Navigation Menu', THEME__TEXTDOMAIN ),
                                'social'        => __( 'Social Media Menu', THEME__TEXTDOMAIN ),
                                'brands'        => __( 'AAMP Global Brands Menu', THEME__TEXTDOMAIN )
                            ) );

        add_theme_support( 'html5', array(
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'search-form'
        ) );

        add_theme_support( 'customize-selective-refresh-widgets' );

        add_editor_style( array( 'assets/css/editor-style.css' ) );
    }
    add_action( 'after_setup_theme', '__theme_setup', 10 );

    require get_template_directory() . '/include/setup/shortcodes.php';
    require get_template_directory() . '/include/override/navigation-walker.php';
    require get_template_directory() . '/include/library/AAMP/Core.php';

    new AAMP\Core;
