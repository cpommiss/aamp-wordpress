<?php
    /**
     * Shortcodes
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */

    function __shortcode_current_year() {
        return date( 'Y' );
    }

    add_shortcode( 'current-year', '__shortcode_current_year' );