<?php
    /**
     * Custom Navigation Walker
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    class AAMP_Nav_Walker_Default extends Walker_Nav_Menu {
        function start_lvl( &$output, $depth = 0, $args = array() ) {
            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            $indent = str_repeat( $t, $depth );

            // Default class.
            $slug = $args->menu->slug;

            $classes = array( $slug . '__sub-menu' );
            $classes[] = 'sub-menu';

            $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
            $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

            $output .= "{$n}{$indent}<ul $class_names>{$n}";
        }

        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            global $wp_query;

            $has_children   = false;
            $slug           = strtolower( $args->menu->slug );

            $indent         = ( $depth ) ? str_repeat( "\t", $depth ) : '';

            $class_names    =
            $value          = '';

            $classes        = empty( $item->classes ) ? array() : (array) $item->classes;
            $classes[]      = 'nav-item';
            $classes[]      = $slug . '__item';

            if ( in_array( 'menu-item-has-children', $item->classes ) !== false ) {
                $has_children   = true;

                $classes[]      = $slug . '__item--has-children';
            }

            $class_names    = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
            $class_names    = ' class="' . esc_attr( $class_names ) . '"';

            $attributes     =   ( !empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '' ) .
                                ( !empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '' ) .
                                ( !empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '' ) .
                                ( !empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '' ) .
                                ( $has_children ? ' aria-haspopup="true"' : '' ) .
                                ( ' class="nav-link ' . $slug . '__link"' ) .
                                ( ' role="menuitem"' ) .
                                ( ( get_field( 'menu_toggle', $item ) ) ? ( ' data-toggle="' . get_field( 'menu_toggle_content', $item ) . '"' ) : '' );

            $description    = ( !empty( $item->description ) ? '<span>' . esc_attr( $item->description ) . '</span>' : '' );

            if ( $depth != 0 ) {
                $description    = $append = $prepend = '';
            }

            $item_output    =   $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . ' role="presentation">' .
                                    $args->before .
                                        '<a' . $attributes . '>' .
                                            $args->link_before .
                                                //$prepend .
                                                trim( apply_filters( 'the_title', $item->title, $item->ID ) ) .
                                                //$append .
                                                $description .
                                            $args->link_after .
                                        '</a>' .
                                        ( ( $has_children ) ? '<span class="sub-menu-toggle"><i class="fa fa-caret-down"></i></span>' : '' ) .
                                    $args->after;

            $output         .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
    }