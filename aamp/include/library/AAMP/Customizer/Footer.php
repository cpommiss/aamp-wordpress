<?php

    /**
     * Customizer Footer-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Customizer;

    class Footer {
        public static $sections     = array(
            array(
                'key'       => 'footer_imagery',
                'title'     => 'Footer Imagery',
                'priority'  => 30
            )
        );

        public static $settings     = array(
            'footer_imagery'    => array(
                array(
                    'key'           => 'footer_logo',
                    'label'         => 'Footer Logo Image',
                    'description'   => 'This logo will be displayed on the bottom left of every page.',
                    'type'          => 'image',
                    'default'       => '',
                    'transport'     => 'refresh'
                )
            )
        );

        function __construct() {
            add_action( 'customize_register', array( '\AAMP\Customizer\Footer', 'setup' ), 10, 3 );
        }

        function __destruct() {
        }

        public static function setup( $customizer ) {
            Core::setup( $customizer, self::$sections, self::$settings );
        }
    }
