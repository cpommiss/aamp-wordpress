<?php

    /**
     * Customizer Brand Color-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Customizer;

    class BrandColors {
        public static $sections     = array(
            array(
                'key'       => 'brand_colors',
                'title'     => 'Brand Colors',
                'priority'  => 1
            )
        );

        public static $settings     = array(
            'brand_colors'    => array(
                array(
                    'label'         => 'Primary Brand Color',
                    'key'           => 'brandcolor_primary',
                    'type'          => 'color',
                    'default'       => '',
                    'transport'     => 'refresh'
                ),
                array(
                    'label'         => 'Secondary Brand Color',
                    'key'           => 'brandcolor_secondary',
                    'type'          => 'color',
                    'default'       => '',
                    'transport'     => 'refresh'
                ),
                array(
                    'label'         => 'Other Brand Color #1',
                    'key'           => 'brandcolor_alt1',
                    'type'          => 'color',
                    'default'       => '',
                    'transport'     => 'refresh'
                ),
                array(
                    'label'         => 'Other Brand Color #2',
                    'key'           => 'brandcolor_alt2',
                    'type'          => 'color',
                    'default'       => '',
                    'transport'     => 'refresh'
                ),
                array(
                    'label'         => 'Other Brand Color #3',
                    'key'           => 'brandcolor_alt3',
                    'type'          => 'color',
                    'default'       => '',
                    'transport'     => 'refresh'
                )
            )
        );

        function __construct() {
            add_action( 'customize_register', array( '\AAMP\Customizer\BrandColors', 'setup' ), 10, 4 );
            add_action( 'admin_print_footer_scripts', array( '\AAMP\Customizer\BrandColors', 'set_palette' ) );
            add_action( 'admin_print_footer_scripts', array( '\AAMP\Customizer\BrandColors', 'set_palette' ) );
        }

        function __destruct() {
        }

        public static function setup( $customizer ) {
            Core::setup( $customizer, self::$sections, self::$settings );
        }

        /**
         * Updates the Iris colorpicker to utilize the brand colors specified within the Customizer
         *
         * @since 1.0
         */
        public static function set_palette() {
            $colors     = array( '\'#ffffff\'', '\'#000000\'' );

            foreach ( self::$settings[ 'brand_colors' ] as $color ) {
                $value      = get_theme_mod( THEME__TEXTDOMAIN . '_' . $color[ 'key' ] );

                if ( strlen( $value ) ) {
                    array_push( $colors, ( '\'' . $value . '\'' ) );
                }
            }

            echo '
                <script type="text/javascript">
                    jQuery( document ).ready( function() { 
                        if ( ( jQuery.wp ) && ( jQuery.wp.wpColorPicker ) ) {
                            jQuery.wp.wpColorPicker.prototype.options = {
                                palettes: [ ' . implode( ',', $colors ) . ' ]
                            };
                        }
                    } );
                </script>';
        }
    }
