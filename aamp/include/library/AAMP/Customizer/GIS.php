<?php

    /**
     * Customizer GIS-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Customizer;

    class GIS {
        public static $sections     = array(
            array(
                'key'       => 'gis',
                'title'     => 'GIS (Mapping and Location)',
                'priority'  => 30
            )
        );

        public static $settings     = array(
            'gis'    => array(
                array(
                    'key'           => 'gis_gmaps_api_key',
                    'label'         => 'Google Maps API Key',
                    'description'   => 'Visit the <a href="https://console.developers.google.com/" target="_blank">Google Developer Console</a> to obtain an API key for Google Maps.',
                    'type'          => 'text',
                    'default'       => '',
                    'transport'     => 'refresh'
                ),
                array(
                    'key'           => 'gis_default_latitude',
                    'label'         => 'Default Latitude',
                    'description'   => 'Used as the default center of maps when they first load (unless specified otherwise by a widget or page settings). Use an online tool, such as <a href="http://www.latlong.net/convert-address-to-lat-long.html" target="_blank">LatLong</a>, to obtain these coordinates from a physical street address.',
                    'type'          => 'text',
                    'default'       => '0.000',
                    'transport'     => 'refresh'
                ),
                array(
                    'key'           => 'gis_default_longitude',
                    'label'         => 'Default Longitude',
                    'description'   => 'Used as the default center of maps when they first load (unless specified otherwise by a widget or page settings).',
                    'type'          => 'text',
                    'default'       => '0.000',
                    'transport'     => 'refresh'
                ),
                array(
                    'key'           => 'gis_default_zoom',
                    'label'         => 'Default Zoom Level',
                    'description'   => 'Used as the default zoom level of maps when they first load (unless specified otherwise by a widget or page settings). As this number gets larger, the map zooms in further.',
                    'type'          => 'number',
                    'default'       => 13,
                    'min'           => 1,
                    'max'           => 20,
                    'transport'     => 'refresh'
                ),
            )
        );

        function __construct() {
            add_action( 'customize_register', array( '\AAMP\Customizer\GIS', 'setup' ), 10, 4 );
        }

        function __destruct() {
        }

        public static function setup( $customizer ) {
            Core::setup( $customizer, self::$sections, self::$settings );
        }
    }
