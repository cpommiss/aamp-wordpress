<?php

    /**
     * Customizer Contact Information-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Customizer;

    class Contact {
        public static $sections     = array(
            array(
                'key'       => 'contact_information',
                'title'     => 'Contact Information',
                'priority'  => 30
            )
        );

        public static $settings     = array(
            'contact_information'    => array(
                array(
                    'key'           => 'contact_phone',
                    'label'         => 'Phone #',
                    'description'   => 'Primary contact phone number',
                    'type'          => 'text',
                    'default'       => '',
                    'transport'     => 'refresh'
                ),
                array(
                    'key'           => 'contact_fax',
                    'label'         => 'Fax #',
                    'description'   => 'Fax number',
                    'type'          => 'text',
                    'default'       => '',
                    'transport'     => 'refresh'
                )
            )
        );

        function __construct() {
            add_action( 'customize_register', array( '\AAMP\Customizer\Contact', 'setup' ), 10, 3 );
        }

        function __destruct() {
        }

        public static function setup( $customizer ) {
            Core::setup( $customizer, self::$sections, self::$settings );
        }
    }
