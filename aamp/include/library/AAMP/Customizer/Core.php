<?php

    /**
     * Customizer-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Customizer;

    class Core {
        public static $queue    = array();
        
        function __construct() {
            foreach ( new \DirectoryIterator( dirname( __FILE__ ) ) as $file ) {
                if ( ( $file->isDot() ) || ( strtolower( $file->getBasename( '.php' ) ) === 'core' ) ) continue;

                array_push( self::$queue, $file->getBasename( '.php' ) );
            }

            foreach ( self::$queue as $item ) {
                require_once $item . '.php';

                $classname      = '\\AAMP\\Customizer\\' . $item;

                new $classname;
            }
        }

        function __destruct() {
        }

        /**
         * Sets up additional Customizer field groups and fields
         *
         * @since 1.0
         */
        public static function setup( $customizer, $sections, $settings ) {
            foreach ( $sections as $section ) {
                $customizer->add_section(
                    THEME__TEXTDOMAIN . '_' . $section[ 'key' ],
                    array(
                        'title'      => __( $section[ 'title' ], THEME__TEXTDOMAIN ),
                        'priority'   => $section[ 'priority' ]
                    )
                );
            }

            foreach ( $settings as $section => $items ) {
                foreach ( $items as $item ) {
                    $customizer->add_setting(
                        THEME__TEXTDOMAIN . '_' . $item[ 'key' ],
                        array(
                            'default'       => $item[ 'default' ],
                            'transport'     => $item[ 'transport' ]
                        )
                    );

                    $options_common     = array(
                        'label'          => $item[ 'label' ],
                        'description'    => ( ( isset( $item[ 'description' ] ) ) ? $item[ 'description' ] : '' ),
                        'section'        => THEME__TEXTDOMAIN . '_' . $section,
                        'settings'       => THEME__TEXTDOMAIN . '_' . $item[ 'key' ],
                        'type'           => $item[ 'type' ]
                    );

                    switch ( $item[ 'type' ] ) {
                        case 'color' :
                            $customizer->add_control(
                                new \WP_Customize_Color_Control(
                                    $customizer,
                                    THEME__TEXTDOMAIN . '_' . $item[ 'key' ],
                                    array_merge(
                                        $options_common,
                                        ( ( ( isset( $item[ 'control' ] ) ) && ( is_array( $item[ 'control' ] ) ) ) ? $item[ 'control' ] : array() )
                                    )
                                )
                            );
                            break;

                        case 'image' :
                            $customizer->add_control(
                                new \WP_Customize_Image_Control(
                                    $customizer,
                                    THEME__TEXTDOMAIN . '_' . $item[ 'key' ],
                                    array_merge(
                                        $options_common,
                                        ( ( ( isset( $item[ 'control' ] ) ) && ( is_array( $item[ 'control' ] ) ) ) ? $item[ 'control' ] : array() )
                                    )
                                )
                            );
                            break;

                        default :
                            $customizer->add_control(
                                new \WP_Customize_Control(
                                    $customizer,
                                    THEME__TEXTDOMAIN . '_' . $item[ 'key' ],
                                    array_merge(
                                        $options_common,
                                        ( ( ( isset( $item[ 'control' ] ) ) && ( is_array( $item[ 'control' ] ) ) ) ? $item[ 'control' ] : array() )
                                    )
                                )
                            );
                            break;
                    }
                }
            }
        }

        /**
         * Returns data from a custom customizer field
         *
         * @since 1.0
         *
         * @return string 
         */
        public static function get( $key, $default = '' ) {
            $value      = get_theme_mod( THEME__TEXTDOMAIN . '_' . $key );

            return ( ( strlen( $value ) ) ? $value : $default );
        }
    }