<?php

    /**
     * Miscellaneous Blog/Post-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Blog;

    class Misc {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Add a pingback url auto-discovery header for singularly identifiable articles.
         *
         * @since 1.0
         */
        public static function pingback_header() {
            if ( is_singular() && pings_open() ) {
                printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
            }
        }
    }