<?php

    /**
     * Blog/Post-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Blog;

    class Core {
        function __construct() {
            add_action( 'wp_head', array( '\AAMP\Blog\Misc', 'pingback_header' ) );
            add_filter( 'excerpt_more', array( '\AAMP\Blog\Core', 'excerpt' ) );
            add_filter( 'excerpt_length', array( '\AAMP\Blog\Core', 'excerpt_length' ) );
        }

        function __destruct() {
        }

        /**
         * Replaces "[...]" (appended to automatically generated excerpts) with ... and
         * a 'Continue reading' link.
         *
         * @since 1.0
         *
         * @return string 'Continue reading' link prepended with an ellipsis.
         */
        public static function excerpt( $link ) {
            if ( is_admin() ) {
                return $link;
            }

            return '';
        }


        /**
         * Modifies the word count for excerpt lengths based on the page being viewed.
         *
         * @since 1.0
         *
         * @return integer
         */
        public static function excerpt_length( $words ) {
            switch ( true ) {
                case is_post_type_archive( 'activities' ) :
                case get_post_type() === 'activities' :
                    return 20;
                    break;

                default :
                    return 15;
            }
        }


        /**
         * Prints HTML category list of categories assigned to current post.
         *
         * @since 1.0
         */
        public static function categories() {
            return get_the_category_list();
        }

        /**
         * Prints HTML with meta information for the current post-date/time and author.
         *
         * @since 1.0
         */
        public static function posted_on() {
            // Get the author name; wrap it in a link.
            $byline = sprintf(
            /* translators: %s: post author */
                __( 'by %s', THEME__TEXTDOMAIN ),
                '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
            );

            // Finally, let's write all of this to the page.
            return '<span class="posted-on">' . \AAMP\Blog\Core::time_link() . '</span><span class="byline"> ' . $byline . '</span>';
        }

        /**
         * Gets a nicely formatted string for the published date.
         *
         * @since 1.0
         */
        public static function time_link() {
            $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

            if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
                $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
            }

            $time_string = sprintf(
                $time_string,
                get_the_date( DATE_W3C ),
                get_the_date(),
                get_the_modified_date( DATE_W3C ),
                get_the_modified_date()
            );

            return sprintf(
            /* translators: %s: post date */
                __( '<span class="screen-reader-text">Posted on</span> %s', THEME__TEXTDOMAIN ),
                $time_string
            );
        }
    }