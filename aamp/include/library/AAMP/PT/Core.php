<?php

    /**
     * PnT (Post and Taxonomies)-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\PT;

    class Core {
        public static $queue    = array();

        function __construct() {
            foreach ( new \DirectoryIterator( dirname( __FILE__ ) ) as $file ) {
                if ( ( $file->isDot() ) || ( strtolower( $file->getBasename( '.php' ) ) === 'core' ) ) continue;

                array_push( self::$queue, array( 'class_name' => 'AAMP', 'post_type' => $file->getBasename( '.php' ) ) );
            }

            foreach ( self::$queue as $item ) {
                require_once $item[ 'post_type' ] . '.php';
            }

            add_action( 'init', array( '\AAMP\PT\Core', 'register_post_types' ) );
            add_action( 'init', array( '\AAMP\PT\Core', 'register_taxonomies' ) );
            add_action( 'init', array( '\AAMP\PT\Core', 'add_image_sizes' ) );

            add_action( 'wp_ajax_' . THEME__TEXTDOMAIN . '_fetch_posts', array( '\AAMP\PT\Core', 'fetch_posts' ) );
            add_action( 'wp_ajax_nopriv_' . THEME__TEXTDOMAIN . '_fetch_posts', array( '\AAMP\PT\Core', 'fetch_posts' ) );

            add_filter( 'template_include', array( '\AAMP\PT\Core', 'set_search_template' ), 10, 1 );
        }

        function __destruct() {
        }

        public static function register_post_types() {
            foreach ( self::$queue as $item ) {
                $namespace  = '\\' . $item[ 'class_name' ] . '\\PT\\' . $item[ 'post_type' ];

                foreach ( $namespace::$post_types as $slug => $type ) {
                    $data               = $type[ 'options' ];
                    $data[ 'labels' ]   = $type[ 'labels' ];

                    register_post_type( $slug, $data );
                }
            }
        }

        public static function register_taxonomies() {
            foreach ( self::$queue as $item ) {
                $namespace  = '\\' . $item[ 'class_name' ] . '\\PT\\' . $item[ 'post_type' ];

                foreach ( $namespace::$taxonomies as $slug => $taxonomy ) {
                    $data               = $taxonomy[ 'options' ];
                    $data[ 'labels' ]   = $taxonomy[ 'labels' ];

                    register_taxonomy( $slug, $taxonomy[ 'post_types' ], $data );
                }
            }
        }

        public static function add_image_sizes() {
            foreach ( self::$queue as $item ) {
                $namespace  = '\\' . $item[ 'class_name' ] . '\\PT\\' . $item[ 'post_type' ];

                foreach ( $namespace::$image_sizes as $key => $data ) {
                    if ( !empty( $data ) ) {
                        add_image_size( THEME__TEXTDOMAIN . '-' . $key, $data[ 'width' ], $data[ 'height' ], $data[ 'crop' ] );

                        if ( $data[ '2x' ] === true ) {
                            add_image_size( THEME__TEXTDOMAIN . '-' . $key . '-2x', $data[ 'width' ] * 2, $data[ 'height' ] * 2, $data[ 'crop' ] );
                        }
                    }
                }
            }
        }

        public static function set_search_template( $template ) {
            global $wp_query;

            $post_type  = get_query_var( 'post_type' );

            if ( ( $wp_query->is_search ) && ( in_array( $post_type, array( 'types-of-cancer' ) ) !== false ) ) {
                return locate_template( 'search-' . $post_type . '.php' );
            } else {
                return $template;
            }
        }

        public static function lookup_post_type( $post_type, $return_all = false ) {
            foreach ( self::$queue as $item ) {
                $namespace  = '\\' . $item[ 'class_name' ] . '\\PT\\' . $item[ 'post_type' ];

                foreach ( $namespace::$post_types as $slug => $type ) {
                    if ( $slug === $post_type ) {
                        return ( ( $return_all ) ? array( 'namespace' => $namespace, 'settings' => $type ) : $type );
                    }
                }
            }

            return false;
        }

        public static function fetch_posts() {
            global $wp_query;

            $response       = array(
                'success'       => false,
                'total'         => 0,
                'current'       => 0,
                'pages'         => 0,
                'dataset'       => array(),
                'query'         => array()
            );

            $per_page       = intval( $_GET[ 'per_page' ] );
            $page           = intval( $_GET[ 'page' ] );
            $post_type      = strtolower( $_GET[ 'post_type' ] );

            if ( $page === 0 ) {
                $page           = 1;
            }

            if ( $per_page === 0 ) {
                $per_page       = 10;
            }

            if ( empty( $post_type ) ) {
                $post_type      = 'post';
            }

            $post_type_data = self::lookup_post_type( $post_type, true );

            if ( strlen( $post_type ) ) {
                $response[ 'query' ]    = array(
                    'post_type'         => $post_type,
                    'post_status'       => 'publish',
                    'posts_per_page'    => $per_page,
                    'paged'             => $page
                );

                if ( !empty( $_GET[ 'taxonomy' ] ) ) {
                    $response[ 'query' ][ 'tax_query' ] = array();

                    foreach ( $_GET[ 'taxonomy' ] as $taxonomy ) {
                        if ( strlen( trim( $taxonomy[ 'value' ] ) ) ) {
                            $query      = array(
                                'taxonomy'  => $taxonomy[ 'term' ],
                                'field'     => 'term_id',
                                'terms'     => explode( ',', $taxonomy[ 'value' ] ),
                                'operator'  => 'IN'
                            );

                            array_push( $response[ 'query' ][ 'tax_query' ], $query );
                        }
                    }

                    if ( empty( $response[ 'query' ][ 'tax_query' ] ) ) {
                        unset( $response[ 'query' ][ 'tax_query' ] );
                    }
                }

                if ( !empty( $_GET[ 'meta' ] ) ) {
                    $response[ 'query' ][ 'meta_query' ] = array();

                    foreach ( $_GET[ 'meta' ] AS $meta ) {
                        $query      = array();

                        if ( ( $post_type_data ) && ( isset( $post_type_data[ 'namespace' ] ) ) && ( method_exists( $post_type_data[ 'namespace' ], 'handle_meta_query' ) ) ) {
                            $query      = $post_type_data[ 'namespace' ]::handle_meta_query( $meta[ 'key' ], $meta[ 'value' ] );
                        }

                        if ( ( !$query ) || ( empty( $query ) ) ) {
                            $query      = array(
                                'key'       => $meta[ 'key' ],
                                'value'     => $meta[ 'value' ]
                            );
                        }

                        if ( !empty( $query[ 'value' ] ) ) {
                            array_push( $response[ 'query' ][ 'meta_query' ], $query );
                        }
                    }

                    if ( empty( $response[ 'query' ][ 'meta_query' ] ) ) {
                        unset( $response[ 'query' ][ 'meta_query' ] );
                    }
                }

                // set ordering parameters
                if ( ( $post_type_data ) && ( isset( $post_type_data[ 'settings' ] ) ) && ( isset( $post_type_data[ 'settings' ][ 'ordering' ] ) ) ) {
                    $response[ 'query' ]    = array_merge(  $response[ 'query' ],
                                                            $post_type_data[ 'settings' ][ 'ordering' ] );
                } else {
                    $response[ 'query' ]    = array_merge(  $response[ 'query' ],
                                                            array(
                                                                'orderby'       => 'title',
                                                                'order'         => 'ASC'
                                                            ) );
                }

                $dataset                = new \WP_Query( $response[ 'query' ] );
                $wp_query               = $dataset;

                if ( is_object( $dataset ) ) {
                    $response[ 'success' ]  = true;
                    $response[ 'total' ]    = $dataset->found_posts;
                    $response[ 'current' ]  = $page;
                    $response[ 'pages' ]    = ( ( ( $per_page > 0 ) && ( $per_page < $dataset->found_posts ) ) ? ceil( $dataset->found_posts / $per_page ) : 1 );

                    if ( $dataset->have_posts() ) {
                        while ( $dataset->have_posts() ) {
                            ob_clean();
                            ob_start();

                            $dataset->the_post();

                            setup_postdata( get_the_ID() );

                            get_template_part( 'parts/' . $post_type . '/ajax/block' );

                            array_push( $response[ 'dataset' ], ob_get_clean() );
                        }
                    }
                }

                wp_reset_postdata();
                wp_reset_query();
            }

            print json_encode( $response );
            exit;
        }
    }
