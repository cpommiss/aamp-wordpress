<?php

    /**
     * Archive-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Archives;

    class Core {
        function __construct() {
            add_filter( 'pre_get_posts', array( '\AAMP\Archives\Core', 'set_display_restrictions' ) );
            add_filter( 'pre_get_posts', array( '\AAMP\Archives\Core', 'set_sort_order' ) );
            add_filter( 'pre_get_posts', array( '\AAMP\Archives\Core', 'set_search_parameters' ) );
        }

        function __destruct() {
        }
        
        public static function set_sort_order( $query ) {
            if ( !is_admin() ) {
                switch ( true ) {
                    case is_post_type_archive( 'to-be-determined' ) :
                        $query->set( 'orderby', 'menu_order' );
                        $query->set( 'order', 'ASC' );
                        break;
                }
            }
        }

        public static function set_display_restrictions( $query ) {
            if ( !is_admin() ) {
                switch ( true ) {
                    case is_post_type_archive( 'to-be-determined' ) :
                        $query->set( 'post_parent', '0' );
                        break;
                }
            }
        }

        public static function set_search_parameters( $query ) {
            if ( !is_admin() ) {
                if ( ( ( isset( $query->query_vars[ 'post_type' ] ) ) && ( in_array( $query->query_vars[ 'post_type' ], array( 'to-be-determined' ) ) !== false ) ) ) {
                    $query->query_vars[ 'posts_per_page' ] = -1;
                } else if ( $query->is_search ) {
                    $query->query_vars[ 'posts_per_page' ] = 12;
                }
            }

            return $query;
        }
    }