<?php

    /**
     * ACF-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\ACF;

    class Options {
        public static $subpages     = array(
            'Archives'
        );

        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Sets up Options pages within ACF.
         *
         * @since 1.0
         *
         * @return array
         */
        public static function setup() {
            if ( function_exists( 'acf_add_options_page' ) ) {
                $subpages   = array();

                $parent     = acf_add_options_page(
                    array(
                        'page_title'    => 'Theme Options',
                        'menu_title'    => 'Theme Options',
                        'redirect'      => false
                    )
                );

                foreach ( self::$subpages as $subpage ) {
                    array_push( $subpages, $subpage );
                }

                if ( have_rows( 'additional_options_pages', 'option' ) ) {
                    while ( have_rows( 'additional_options_pages', 'option' ) ) {
                        the_row();

                        array_push( $subpages, get_sub_field( 'name', 'option' ) );
                    }
                }

                sort( $subpages );

                foreach ( $subpages as $subpage ) {
                    acf_add_options_sub_page(
                        array(
                            'page_title'    => $subpage,
                            'menu_title'    => $subpage,
                            'parent_slug'   => $parent[ 'menu_slug' ]
                        )
                    );
                }
            }
        }
    }
