<?php

    /**
     * ACF-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\ACF;

    class Core {
        function __construct() {
            add_filter( 'acf/init', array( '\AAMP\ACF\Core', 'set_google_maps_api_key' ) );
            add_filter( 'acf/settings/load_json', array( '\AAMP\ACF\Core', 'set_json_load_points' ) );
            add_filter( 'acf/settings/save_json', array( '\AAMP\ACF\Core', 'set_json_save_point' ) );
            add_filter( 'acf/get_image_sizes', array( '\AAMP\ACF\Core', 'get_image_sizes' ) );

            add_action( 'after_setup_theme', array( '\AAMP\ACF\Options', 'setup' ) );
        }

        function __destruct() {
        }

        /**
         * Sets the Google Maps API key for ACF Pro.
         *
         * @since 1.0
         *
         * @return array
         */
        public static function set_google_maps_api_key( $path ) {
            acf_update_setting( 'google_api_key', \AAMP\Customizer\Core::get( 'gis_gmaps_api_key' ) );
        }

        /**
         * Changes the path that ACF saves .json field group definitions to.
         *
         * @since 1.0
         *
         * @return array
         */
        public static function set_json_save_point( $path ) {
            if ( is_child_theme() ) {
                $path   = get_stylesheet_directory() . '/include/acf';
            } else {
                $path   = get_template_directory() . '/include/acf';
            }

            return $path;
        }

        /**
         * Changes the path that ACF saerches for .json field group definitions to
         * the /include/acf folder within both the parent and child theme (when applicable).
         *
         * @since 1.0
         *
         * @return array
         */
        public static function set_json_load_points( $paths ) {
            // remove original path (optional)
            unset($paths[0]);

            $paths[] = get_template_directory() . '/include/acf';

            if ( is_child_theme() ) {
                $paths[] = get_stylesheet_directory() . '/include/acf';
            }

            return $paths;
        }

        /**
         * Filters the name of the image sizes for display within ACF.
         *
         * @since 1.0
         *
         * @return array
         */
        public static function get_image_sizes( $sizes ) {
            foreach ( $sizes as $key => $value ) {
                if ( stripos( $key, ( THEME__TEXTDOMAIN . '-' ) ) !== false ) {
                    $sizes[ $key ]  = str_replace( ucwords( THEME__TEXTDOMAIN ), ( '[' . THEME__NAME . '] ' ), $value );
                }
            }

            return $sizes;
        }
    }
