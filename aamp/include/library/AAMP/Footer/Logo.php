<?php

    /**
     * Footer logo-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Footer;

    class Logo {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Retreives the footer logo specified within the Customizer. This does NOT use the 'custom-logo' feature of WordPress,
         * as that feature does not support SVG images due to the requirement to crop the uploaded logo image before placement.
         *
         * @since 1.0
         *
         * @return string
         */
        public static function get( $classes = '' ) {
            $output     = '';
            $logo       = \AAMP\Customizer\Core::get( 'footer_logo' );
            $classes    = 'logo' . ( ( strlen( $classes ) ) ? ( ' ' . $classes ) : '' );

            if ( strlen( $logo ) ) {
                $output     = sprintf(  '<img src="%s" alt="%s" class="logo">',
                                        $logo,
                                        get_bloginfo( 'name' ),
                                        $classes );
            }

            return $output;
        }
    }

