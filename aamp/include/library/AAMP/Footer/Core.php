<?php

    /**
     * Footer-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Footer;

    class Core {
        function __construct() {
            add_action( 'wp_ajax_' . THEME__TEXTDOMAIN . '_get_core_footer', array( '\AAMP\Footer\Core', 'get_core_footer' ) );
            add_action( 'wp_ajax_nopriv_' . THEME__TEXTDOMAIN . '_get_core_footer', array( '\AAMP\Footer\Core', 'get_core_footer' ) );
        }

        function __destruct() {
        }

        /**
         * Returns the footer of the current theme
         *
         * @since 1.0
         *
         * @return void
         */
        public static function get_core_footer() {
            if ( ob_get_length() !== false ) {
                ob_clean();
            }

            ob_start();

            // trigger header (then erase output buffer so we don't send any header data) so we can get footer scripts
            wp_head();

            ob_clean();

            get_template_part( 'parts/footer/footer', 'core' );

            wp_footer();

            $buffer     = ob_get_clean();

            print json_encode( array( 'output' => $buffer ) );
            exit;
        }
    }

