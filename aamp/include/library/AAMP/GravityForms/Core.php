<?php

    /**
     * Gravity Forms-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\GravityForms;

    class Core {
        public static $fields = array(
            'layout_class'  => array(
                'priority'  => 0,
                'section'   => 'appearance',
                'type'      => 'select',
                'key'       => 'layoutClass',
                'title'         => 'Layout Style Options',
                'tooltip'       => '<h6>Layout</h6>Options to control how this field is visually positioned.',
                'options'   => array(
                    'multiple'  => false,
                    'choices'   => array(
                        'gf_full'   => 'Full-width (default)',

                        'Halves (2-Column Layouts)' => array(
                            'gf_left_half'      => 'On the left (left column)',
                            'gf_right_half'     => 'On the right (right column)'
                        ),
                        'Thirds (3-Column Layouts)' => array(
                            'gf_left_third'     => 'On the left (left column)',
                            'gf_middle_third'   => 'In the middle (middle column)',
                            'gf_right_third'    => 'On the right (right column)'
                        )
                    )
                ),
                'usage'     => 'css_class'
            ),

            'layout_extras' => array(
                'priority'      => 0,
                'section'       => 'appearance',
                'type'          => 'select',
                'key'           => 'layoutExtras',
                'title'         => 'Layout Extras',
                'tooltip'       => '<h6>Layout Extras</h6>Special layouts that can be used on lists, text and more. <strong>You may select more than one option from this list.</strong>',
                'style'         => 'width: 100%;',
                'attributes'    => array(
                    'size'          => 6
                ),
                'options'       => array(
                    'multiple'      => true,
                    'choices'       => array(
                        'Checkbox/radio column styles'  => array(
                            'gf_list_2col'      => 'Equally-spaced 2-column layout',
                            'gf_list_3col'      => 'Equally-spaced 3-column layout',
                            'gf_list_4col'      => 'Equally-spaced 4-column layout',
                            'gf_list_5col'      => 'Equally-spaced 5-column layout',
                            'gf_list_inline'    => 'Inline (horizontal) layout'
                        ),
                        'Checkbox/radio height styles'  => array(
                            'gf_list_height_25'     => 'Forces checkbox/radio to be 25 pixels tall',
                            'gf_list_height_50'     => 'Forces checkbox/radio to be 25 pixels tall',
                            'gf_list_height_75'     => 'Forces checkbox/radio to be 25 pixels tall',
                            'gf_list_height_100'    => 'Forces checkbox/radio to be 25 pixels tall',
                            'gf_list_height_125'    => 'Forces checkbox/radio to be 25 pixels tall',
                            'gf_list_height_150'    => 'Forces checkbox/radio to be 25 pixels tall'
                        ),
                        'Section break styles'  => array(
                            'gf_scroll_text'        => 'Forces a section break to scroll if the text within it is too long.'
                        ),
                        'Field modifiers'       => array(
                            'gf_hide_ampm'          => 'Hides the am/pm selector on a time field.',
                            'gf_hide_charleft'      => 'Hides the characters remaining counter on a paragraph field.'
                        )
                    )
                ),
                'usage'     => 'css_class'
            )
        );

        function __construct() {
            add_filter( 'gform_tooltips', array( '\AAMP\GravityForms\Core', 'insert_field_tooltips' ) );
            add_action( 'gform_editor_js', array( '\AAMP\GravityForms\Core', 'render_editor_scripting' ) );
            add_filter( 'gform_field_container', array( '\AAMP\GravityForms\Core', 'render_field_value' ), 10, 6 );
            add_filter( 'gform_submit_button', array( '\AAMP\GravityForms\Core', 'modify_submit_button' ), 10, 2 );
            add_action( 'gfiframe_head', array( '\AAMP\GravityForms\Core', 'enqueue_embed_assets' ) );

            $sections       = array();

            foreach ( self::$fields as $field ) {
                if ( in_array( $field[ 'section' ], $sections ) === false ) {
                    array_push( $sections, $field[ 'section' ] );
                }
            }

            if ( count( $sections ) ) {
                foreach ( $sections as $section ) {
                    add_action( 'gform_field_' . $section . '_settings', array( '\AAMP\GravityForms\Core', 'render_fields' ), 10, 2 );
                }
            }
        }

        function __destruct() {
        }

        public static function enqueue_assets() {

        }

        public static function enqueue_embed_assets( $id_form ) {
            echo '<link rel="stylesheet" type="text/css" media="screen" href="' . get_stylesheet_directory_uri() . '/assets/css/vendor/gravityforms-embed.css">' . PHP_EOL;
        }

        public static function render_fields( $position, $form_id ) {
            foreach ( self::$fields as $field_key => $field ) {
                // make sure we add the field to the proper section and in the proper position within that section, based on the priority and section attributes of the field definition
                if ( ( $field[ 'priority' ] == $position ) && ( current_action() == ( 'gform_field_' . $field[ 'section' ] . '_settings' ) ) ) {
                    $rendered_field     = '';

                    switch ( $field[ 'type' ] ) {
                        case 'radio' :
                        case 'text' :
                            // TODO
                            break;

                        case 'checkbox' :
                        case 'select' :
                            if ( ( isset( $field[ 'options' ] ) ) && ( isset( $field[ 'options' ][ 'choices' ] ) ) && ( is_array( $field[ 'options' ][ 'choices' ] ) ) ) {
                                $rendered_field     .= '<select id="field_' . $field_key . '" name="field_' . $field_key . '"' . self::render_field_attributes( $field ) . '>';

                                foreach ( $field[ 'options' ][ 'choices' ] as $choice_key => $choice_value ) {
                                    // nested optgroups are not supported in native HTML, so this doesn't need to be recursive.
                                    if ( is_array( $choice_value ) ) {
                                        $rendered_field     .= '<optgroup label="' . $choice_key . '">' . self::render_select_field_choices( $choice_value ) . '</optgroup>';
                                    } else {
                                        $rendered_field     .= self::render_select_field_choices( array( $choice_key => $choice_value ) );
                                    }
                                }

                                $rendered_field     .= '</select>';
                            }
                            break;
                    }

                    echo '<li class="' . $field_key . ' field_setting">';
                    echo    '<label for="field_admin_label">' . $field[ 'title' ] . gform_tooltip( 'form_field_' . $field_key ) . '</label>';
                    echo    $rendered_field;
                    echo '</li>';
                }
            }
        }

        public static function render_field_attributes( $field ) {
            $attributes     = ' data-internal-key="' . $field[ 'key' ] . '" data-field-type="' . $field[ 'type' ] . '"';

            if ( ( isset( $field[ 'style' ] ) ) && ( $field[ 'style' ] ) ) {
                $attributes     .=  ' style="' . $field[ 'style' ] . '"';
            }

            if ( ( $field[ 'type' ] == 'select' ) && ( isset( $field[ 'options' ][ 'multiple' ] ) ) && ( $field[ 'options' ][ 'multiple' ] ) ) {
                $attributes     .=  ' multiple="multiple"';
            }
            if ( ( isset( $field[ 'attributes' ] ) ) && ( $field[ 'attributes' ] ) && ( is_array( $field[ 'attributes' ] ) ) ) {
                foreach ( $field[ 'attributes' ] as $attribute_key => $attribute_value ) {
                    $attributes     .= ' ' . $attribute_key . '="' . $attribute_value . '"';
                }
            }

            return $attributes;
        }

        public static function render_select_field_choices( $choices ) {
            $options        = '';

            if ( is_array( $choices ) ) {
                foreach ( $choices as $choice_value => $choice_label ) {
                    $options        .=  '<option value="' . $choice_value . '">' . $choice_label . '</option>';
                }
            }

            return $options;
        }

        public static function render_field_value( $field_container, $field_data, $form, $field_css_class, $field_style, $field_content ) {
            $field_attrs    = '';
            $field_prepend  = '';
            $field_append   = '';

            // lifted from: gravityforms/form_display.php:2740
            $field_id       = ( ( ( \GFCommon::is_form_editor() || \GFCommon::is_entry_detail() ) || empty( $form ) ) ? ( 'field_' . $field_data[ 'id' ] ) : ( 'field_' . $form[ 'id' ] . '_' . $field_data[ 'id' ] ) );

            foreach ( self::$fields as $field_key => $field ) {
                if ( isset( $field_data[ $field[ 'key' ] ] ) ) {
                    $field_value        = trim( $field_data[ $field[ 'key' ] ] );

                    switch ( $field[ 'type' ] ) {
                        case 'checkbox' :
                        case 'select' :
                            if ( ( isset( $field[ 'options' ] ) ) && ( is_array( $field[ 'options' ] ) ) && ( isset( $field[ 'options' ][ 'multiple' ] ) ) && ( $field[ 'options' ][ 'multiple' ] ) ) {
                                $field_value        = join( ' ', explode( ',', $field_value ) );
                            }
                            break;
                    }

                    switch ( strtolower( $field[ 'usage' ] ) ) {
                        case 'css_class' :
                            $field_css_class            .= ' ' . esc_attr( $field_value );
                            break;

                        case 'attribute' :
                            $field_attrs                .=  ' ' . $field_key . '="' . esc_attr( $field_value ) . '"';
                            break;

                        case 'element' :
                            // prepend or append an HTML element somewhere
                            // TODO
                            break;
                    }
                }
            }

            return '<li id="' . $field_id . '" class="' . $field_css_class . '"' . ( ( strlen( $field_style ) ) ? ( ' ' . $field_style ) : '' ) . ( ( strlen( $field_attrs ) ) ? ( ' ' . $field_attrs ) : '' ) . '>' . $field_prepend . $field_content . $field_append . '</li>';
        }

        public static function render_editor_scripting() {
            $field_list     = array();

            foreach ( self::$fields as $field_key => $field ) {
                array_push( $field_list, '.' . $field[ 'key' ] );
            }

            echo '<script type="text/javascript">';
            echo    'fieldSettings.text += ",' . implode( ',', $field_list ) . '";';
            echo    'jQuery( document ).bind( "gform_load_field_settings", aamp_admin.fn.extend.gravityforms.fields.restore );';

            foreach ( self::$fields as $field_key => $field ) {
                switch ( $field[ 'type' ] ) {
                    case 'checkbox' :
                    case 'radio' :
                        break;

                    default :
                        echo    'jQuery( "#field_' . $field_key . '" ).on( "change", aamp_admin.fn.extend.gravityforms.fields.set );';
                        break;
                }
            }

            echo '</script>';
        }

        public static function insert_field_tooltips( $tooltips ) {
            foreach ( self::$fields as $field_key => $field ) {
                if ( $field[ 'tooltip' ] ) {
                    $tooltips[ $field_key ] = $field[ 'tooltip' ];
                }
            }

            return $tooltips;
        }

        public static function modify_submit_button( $button, $form ) {
            return '<button class="btn btn-default pull-right" id="gform_submit_button_' . $form[ 'id' ] . '"><span>' . __( 'Submit', THEME__TEXTDOMAIN ) . '</span></button>';
        }
    }

