<?php

    /**
     * SiteOrigin Panels General Utilities
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\SiteOrigin;

    class Utils {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Formats a hexadecimal color string in rgba format
         *
         * @since 1.0
         *
         * @return string
         */
        public static function format_color( $color, $opacity = 100, $as_string = true ) {
            $color      = preg_replace( "/[^0-9A-Fa-f]/", '', $color );
            $opacity    = number_format( ( intval( $opacity ) / 100 ), 2 );
            $rgb        = array();

            if ( strlen( $color ) == 6 ) {
                $color_dec  = hexdec( $color );

                $rgb[ 'red' ]   = 0xFF & ( $color_dec >> 0x10 );
                $rgb[ 'green' ] = 0xFF & ( $color_dec >> 0x8 );
                $rgb[ 'blue' ]  = 0xFF & $color_dec;
            } elseif ( strlen( $color ) == 3 ) {
                $rgb[ 'red' ]   = hexdec( str_repeat( substr( $color, 0, 1 ), 2 ) );
                $rgb[ 'green' ] = hexdec( str_repeat( substr( $color, 1, 1 ), 2 ) );
                $rgb[ 'blue' ]  = hexdec( str_repeat( substr( $color, 2, 1 ), 2 ) );
            }

            if ( $as_string ) {
                return 'rgba(' . join( ',', $rgb ) . ',' . $opacity . ')';
            } else {
                return array( 'color' => $rgb, 'opacity' => $opacity );
            }
        }
    }

