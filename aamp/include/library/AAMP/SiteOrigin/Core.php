<?php

    /**
     * SiteOrigin Panels-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\SiteOrigin;

    class Core {
        function __construct() {
            add_filter( 'siteorigin_panels_widget_style_fields', array( '\AAMP\SiteOrigin\WidgetSettings', 'add_layout_settings' ) );
            add_filter( 'siteorigin_panels_widget_style_attributes', array( '\AAMP\SiteOrigin\WidgetSettings', 'widget_style_attributes' ), 10, 2 );

            add_filter( 'siteorigin_widgets_form_options_sow-simple-masonry', array( '\AAMP\SiteOrigin\Widgets\SimpleMasonry', 'extend_admin_form' ), 10, 2 );
            add_filter( 'siteorigin_widgets_template_file_sow-simple-masonry', array( '\AAMP\SiteOrigin\Widgets\SimpleMasonry', 'replace_template_file' ), 10, 3 );

            //add_filter( 'siteorigin_panels_widget_dialog_tabs', array( '\AAMP\SiteOrigin\Core', 'add_widget_tabs', 20 ) );
            //add_filter( 'siteorigin_panels_widgets', array( '\AAMP\SiteOrigin\Core', 'add_widget_icons' ) );
            add_filter( 'siteorigin_widgets_widget_folders', array( '\AAMP\SiteOrigin\Core', 'widgets_collection' ) );

            add_filter( 'siteorigin_panels_css_object', array( '\AAMP\SiteOrigin\Core', 'filter_css_object' ), 20, 4 );
        }

        function __destruct() {
        }

        /**
         * Register custom widget category within SiteOrigin Panels.
         *
         * @since 1.0
         */
        public static function add_widget_tabs( $tabs ) {
            $tabs[] = array(
                'title'  => __( sprintf( '%s Widgets', THEME__NAME ), THEME__TEXTDOMAIN ),
                'filter' => array(
                    'groups' => array( THEME__TEXTDOMAIN )
                )
            );

            return $tabs;
        }

        /**
         * Register custom widget category option within SiteOrigin Panels.
         *
         * @since 1.0
         */
        public static function add_widget_icons( $widgets ) {
            foreach ( $widgets as &$widget_data ) {
                if ( stripos( $widget_data[ 'title' ], THEME__NAME ) === 0 ) {
                    $widget_data[ 'groups' ] = array( THEME__TEXTDOMAIN );
                }
            }

            return $widgets;
        }

        /**
         * Register custom widget folder within theme directory.
         *
         * @since 1.0
         */
        public static function widgets_collection( $folders ) {
            $folders[] = get_template_directory() . '/widgets/';
            $folders[] = get_stylesheet_directory() . '/widgets/';

            return $folders;
        }


        public static function filter_css_object( $css, $panels_data, $post_id, $layout ) {
            if ( empty( $layout ) ) {
                return $css;
            }

            foreach ( $layout as $ri => $row ) {
                if ( empty( $row[ 'cells' ] ) ) continue;

                foreach ( $row[ 'cells' ] as $ci => $cell ) {
                    if ( empty( $cell[ 'widgets' ] ) ) continue;

                    foreach ( $cell['widgets'] as $wi => $widget ) {
                        if ( ! empty( $widget[ 'panels_info' ][ 'style' ][ 'font_color' ] ) ) {
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h1', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h2', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h3', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h4', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h5', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                            $css->add_widget_css( $post_id, $ri, $ci, $wi, ' h6', array(
                                'color' => $widget[ 'panels_info' ][ 'style' ][ 'font_color' ]
                            ) );
                        }
                    }
                }
            }

            return $css;
        }
    }

