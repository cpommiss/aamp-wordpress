<?php

    /**
     * SiteOrigin Panels 'Simple Masonry' built-in widget extensions
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\SiteOrigin\Widgets;

    class SimpleMasonry {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Adds settings to Simple Masonry administration area form
         *
         * @since 1.0
         *
         * @return array
         */
        public static function extend_admin_form( $form_options, $widget ){
            $form_options[ 'desktop_layout' ][ 'gutter' ][ 'default' ]  = 30;
            $form_options[ 'tablet_layout' ][ 'gutter' ][ 'default' ]   = 30;

            $form_options[ 'is_gallery' ]    = array(
                'type'      => 'checkbox',
                'label'     => __( 'Should this masonry grid behave like a gallery, with image links opening in a modal window?', THEME__TEXTDOMAIN ),
                'default'   => true
            );

            if ( ( isset( $form_options[ 'items' ] ) ) && ( isset( $form_options[ 'items' ][ 'fields' ] ) ) ) {
                $form_options[ 'items' ][ 'fields' ][ 'link_to_image' ] = array(
                    'type'      => 'checkbox',
                    'label'     => __( 'Disregard URL field above and link directly to the large version of the chosen image.', THEME__TEXTDOMAIN ),
                    'default'   => true
                );
            }

            return $form_options;
        }

        /**
         * Replaces the template file for the Simple Masonry widget
         *
         * @since 1.0
         *
         * @return string
         */
        public static function replace_template_file( $filename, $instance, $widget ) {
            return get_template_directory() . '/widgets/siteorigin/simple-masonry/default.php';
        }
    }
