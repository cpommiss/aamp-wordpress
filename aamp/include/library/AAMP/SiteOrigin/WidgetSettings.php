<?php

    /**
     * SiteOrigin Panels widget-related Settings Additions
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\SiteOrigin;

    class WidgetSettings {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Adds widget settings to SiteOrigin Panels plug-in
         *
         * @since 1.0
         *
         * @return array
         */
        public static function add_layout_settings( $fields, $post_id = 0, $args = array() ) {
            $fields[ 'header_alignment' ] = array(
                'name'          => __( 'Header Text Alignment', THEME__TEXTDOMAIN ),
                'description'   => __( 'Controls how the header for this widget is aligned.', THEME__TEXTDOMAIN ),
                'type'          => 'select',
                'group'         => 'layout',
                'options'       => array(
                    'left'          => __( 'Left-justified (Default)', THEME__TEXTDOMAIN ),
                    'center'        => __( 'Centered', THEME__TEXTDOMAIN ),
                    'right'         => __( 'Right-justified', THEME__TEXTDOMAIN ),
                    'justify'       => __( 'Full justification', THEME__TEXTDOMAIN )
                ),
                'priority'      => 11
            );
            $fields[ 'text_alignment' ] = array(
                'name'          => __( 'Text Alignment', THEME__TEXTDOMAIN ),
                'description'   => __( 'Controls how the text for the content of this widget is aligned.', THEME__TEXTDOMAIN ),
                'type'          => 'select',
                'group'         => 'layout',
                'options'       => array(
                    'left'          => __( 'Left-justified (Default)', THEME__TEXTDOMAIN ),
                    'center'        => __( 'Centered', THEME__TEXTDOMAIN ),
                    'right'         => __( 'Right-justified', THEME__TEXTDOMAIN ),
                    'justify'       => __( 'Full justification', THEME__TEXTDOMAIN )
                ),
                'priority'      => 12
            );

            return $fields;
        }

        /**
         * Adds CSS style attributes to the widget being built and output by SiteOrigin Panels
         *
         * @since 1.0
         *
         * @return array
         */
        public static function widget_style_attributes( $attributes, $args ) {
            if ( isset( $args[ 'header_alignment' ] ) ) {
                $attributes[ 'class' ][]    = 'header-alignment-' . $args[ 'header_alignment' ];
            }
            if ( isset( $args[ 'text_alignment' ] ) ) {
                $attributes[ 'class' ][]    = 'text-alignment-' . $args[ 'text_alignment' ];
            }

            return $attributes;
        }
    }

