<?php

    /**
     * Content-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Content;

    class Core {
        function __construct() {
            add_filter( 'post_class', array( '\AAMP\Content\Core', 'filter_post_class' ), 10, 1 );
            add_filter( 'body_class', array( '\AAMP\Content\Core', 'filter_body_class' ), 10, 1 );

            add_filter( 'pre_post_title', array( '\AAMP\Content\Core', 'mask_empty' ) );
            add_filter( 'pre_post_content', array( '\AAMP\Content\Core', 'mask_empty' ) );
            add_filter( 'wp_insert_post_data', array( '\AAMP\Content\Core', 'unmask_empty' ) );

            add_filter( 'mce_buttons_2', array( '\AAMP\Content\TinyMCE', 'enable_styles' ) );
            add_filter( 'tiny_mce_before_init', array( '\AAMP\Content\TinyMCE', 'add_formats' ) );

            add_action( 'wp_nav_menu_objects', array( '\AAMP\Content\Core', 'filter_nav_class' ), 10, 2 );
        }

        function __destruct() {
        }

        public static function filter_nav_class( $sorted_menu_items, $args ) {
            if ( ( defined( 'DOING_AJAX' ) ) && ( DOING_AJAX ) ) {
                if ( isset( $_POST[ 'current_uri' ] ) ) {
                    $current_uri    = $_POST[ 'current_uri' ];
                    $section        = ( ( isset( $_POST[ 'section' ] ) ) ? $_POST[ 'section' ] : '' );

                    foreach ( $sorted_menu_items as &$menu_item ) {
                        $url_parts      = parse_url( $menu_item->url );

                        if ( $url_parts[ 'path' ] == $section ) {
                            array_push( $menu_item->classes, 'current-menu-ancestor' );
                        } else if ( ( $url_parts[ 'path' ] != '/' ) && ( stripos( $current_uri, $url_parts[ 'path' ] ) !== false ) ) {
                            array_push( $menu_item->classes, 'current-menu-item' );
                        } else {
                            foreach ( array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor', 'current_page_item', 'current_page_parent', 'current_menu_ancestor' ) as $key ) {
                                if ( in_array( $key, $menu_item->classes ) !== false ) {
                                    array_splice( $menu_item->classes, array_search( $key, $menu_item->classes ), 1 );

                                    sort( $menu_item->classes );
                                }
                            }
                        }
                    }
                }
            }

            return $sorted_menu_items;
        }

        public static function filter_post_class( $classes ) {
            return $classes;
        }
        
        public static function filter_body_class( $classes ) {
            if ( is_home() || is_front_page() ) {
                $classes[] = 'front-page';
            }
    
            // Add class if we're viewing the Customizer for easier styling of theme options.
            if ( is_customize_preview() ) {
                $classes[] = THEME__TEXTDOMAIN . '-customizer';
            }
        
            // Add class if sidebar is used.
            if ( is_active_sidebar( 'sidebar-primary' ) && ! is_page() ) {
                $classes[] = 'has-sidebar';
            }

            if ( function_exists( 'get_field' ) ) {
                if ( $background = get_field( 'bg_type', get_the_ID() ) ) {
                    $classes[] = 'background-' . $background;
                }

                if ( $background_fade = get_field( 'bg_fade_height', get_the_ID() ) ) {
                    $classes[] = 'background-fade-' . $background_fade;
                }

                if ( $margin_style = get_field( 'page_margins', get_the_ID() ) ) {
                    $classes[] = 'margin-style-' . $margin_style;
                }
            }

            return $classes;
        }

        /* Allow empty post content */
        public static function mask_empty( $value ) {
            if ( empty($value) ) {
                return ' ';
            }

            return $value;
        }

        public static function unmask_empty( $data ) {
            if ( $data[ 'post_title' ] == ' ' ) {
                $data[ 'post_title' ] = '';
            }

            if ( $data[ 'post_content' ] == ' ' ) {
                $data[ 'post_content' ] = '';
            }

            return $data;
        }

        public static function get_page_style() {
            $styles     = array();

            if ( is_page() ) {
                if ( $bgcolor = get_field( 'page_bgcolor', get_the_ID() ) ) {
                    array_push( $styles, sprintf( 'background-color: %s;', $bgcolor ) );
                }
            }

            return ( ( !empty( $styles ) ) ? ( ' style="' . implode( ' ', $styles ) . '"' ) : '' );
        }
    }