<?php

    /**
     * Content-related URL Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Content;

    class URL {
        function __construct() {
        }

        function __destruct() {
        }

        public static function get_shop_url() {
            $url_source     = get_bloginfo( 'url' );

            // TODO: allow overrides of URL prefix through ACF options

            $url_target     = str_replace( '://', '://catalog.', $url_source );

            return $url_target;
        }
    }