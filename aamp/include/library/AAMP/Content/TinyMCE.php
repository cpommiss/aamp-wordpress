<?php

    /**
     * Content-related Methods
     * TinyMCE Style Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Content;

    class TinyMCE {
        public static $styles = array(
        );

        function __construct() {
        }

        function __destruct() {
        }

        public static function enable_styles( $buttons ) {
            if ( !empty( self::$styles ) ) {
                array_unshift( $buttons, 'styleselect' );
            }

            return $buttons;
        }

        public static function add_formats( $settings ) {
            if ( !empty( self::$styles ) ) {
                $settings[ 'style_formats' ] = json_encode( self::$styles );
            }

            return $settings;
        }
    }

