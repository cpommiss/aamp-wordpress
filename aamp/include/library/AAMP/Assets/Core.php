<?php

    /**
     * Asset-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Assets;

    class Core {
        function __construct() {
            add_action( 'wp_enqueue_scripts', array( '\AAMP\Assets\Core', 'enqueue_public' ) );
            add_action( 'admin_enqueue_scripts', array( '\AAMP\Assets\Core', 'enqueue_admin' ) );
        }

        function __destruct() {
        }

        /**
         * Enqueue scripts and styles.
         *
         * @since 1.0
         */
        public static function enqueue_public() {
            wp_enqueue_style( THEME__TEXTDOMAIN . '-bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/dist/css/bootstrap.min.css', false, '4.0.0' );
            wp_enqueue_style( THEME__TEXTDOMAIN . '-theme', get_template_directory_uri() . '/assets/css/theme.css', array( THEME__TEXTDOMAIN . '-bootstrap' ), THEME__VERSION );

            wp_enqueue_script( 'jquery' );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-popper', get_theme_file_uri( '/assets/vendor/popper.js/dist/umd/popper.min.js' ), false, '1.12.5', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-fresco', get_theme_file_uri( '/assets/vendor/fresco-2.2.2/js/fresco/fresco.js' ), false, '2.2.2', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-slick', get_theme_file_uri( '/assets/vendor/slick-carousel/slick/slick.min.js' ), false, '1.6.0', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-sps', get_theme_file_uri( '/assets/vendor/scrollpos-styler/scrollPosStyler.js' ), false, '0.7.0', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-bootstrap', get_theme_file_uri( '/assets/vendor/bootstrap/dist/js/bootstrap.min.js' ), array( 'jquery', THEME__TEXTDOMAIN . '-popper' ), '4.0.0', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-bootstrap-slider', get_theme_file_uri( '/assets/vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.js' ), array( THEME__TEXTDOMAIN . '-bootstrap' ), '9.8.1', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-vuejs', get_theme_file_uri( '/assets/vendor/vue/dist/vue.min.js' ), false, '2.5.2', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-afterglow', get_theme_file_uri( '/assets/vendor/afterglow/dist/afterglow.min.js' ), false, '1.1.0', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-easing', get_theme_file_uri( '/assets/vendor/jquery-easing/jquery.easing.min.js' ), array( 'jquery' ), '1.3', true );
            wp_enqueue_script( THEME__TEXTDOMAIN . '-cookieconsent', get_theme_file_uri( '/assets/vendor/cookieconsent/build/cookieconsent.min.js' ), array( 'jquery' ), '3.0.6', true );

            \AAMP\JSCore\Core::enqueue();

            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }
        }

        /**
         * Enqueue administration/dashboard scripts and styles.
         *
         * @since 1.0
         */
        public static function enqueue_admin( $hook ) {
            if ( is_admin() ) {
                \AAMP\JSCore\Core::enqueue( false );
            }
        }
    }

