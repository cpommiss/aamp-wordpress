<?php

    /**
     * AAMP API-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\API;

    class VRM {
        const API_USERNAME      = 'ArmCar6512';
        const API_PASSWORD      = '5aA0777A';
        const API_APPID         = 67;

        function __construct() {
        }

        function __destruct() {
        }

        public static function lookup( $vrm = '' ) {
            if ( ( defined( 'DOING_AJAX' ) ) && ( DOING_AJAX ) ) {
                $vrm            = $_REQUEST[ 'vrm' ];
            }

            $vehicle_map    = array(
                'Make'                  => array( 'source' => 'VDS', 'key' => 'make' ),
                'Model'                 => array( 'source' => 'VDS', 'key' => 'model' ),
                'VehicleYear'           => array( 'source' => 'DVLA', 'key' => 'year' ),
                'DateOfRegistration'    => array( 'source' => 'DVLA', 'key' => 'registered_on' ),
                'Colour'                => array( 'source' => 'DVLA', 'key' => 'color' ),
                'Doors'                 => array( 'source' => 'VDS', 'key' => 'doors' ),
                'BodyStyle'             => array( 'source' => 'VDS', 'key' => 'body_style' ),
            );

            $headers        = array(
                'Content-Type: application/json; charset=utf-8',
                'Expect: 100-continue',
                'Accept: application/json'
            );

            $dataset        = array(
                'UserToken'     => array(
                    'Username'      => self::API_USERNAME,
                    'Password'      => self::API_PASSWORD,
                    'ApplicationId' => self::API_APPID,
                    'Language'      => 'en',
                    'LoggedInUser'  => ''
                ),
                'Attributes'    => array(
                    array(
                        'Name'          => 'VRM',
                        'Value'         => $vrm,
                        'DisplayName'   => null,
                        'DisplayValue'  => null,
                        'Source'        => 'DVLA'
                    )
                ),
                'AttributesToReturn' => array(
                )
            );

            $payload        = json_encode( $dataset );

            $request        = curl_init();

            curl_setopt( $request, CURLOPT_URL, 'http://live.vdslookup.co.uk/vehiclesearch.v4.0/api/GB/details' );
            curl_setopt( $request, CURLOPT_HEADER, false );
            curl_setopt( $request, CURLOPT_VERBOSE, false );
            curl_setopt( $request, CURLOPT_HTTPHEADER, array_merge( $headers, array( 'Content-Length: ' . strlen( $payload ) ) ) );
            curl_setopt( $request, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $request, CURLOPT_POST, true );
            curl_setopt( $request, CURLOPT_POSTFIELDS, $payload );

            $response   = curl_exec( $request );

            curl_close( $request );

            $vehicle    = (object)[];
            $output     = json_decode( $response );

            if ( is_array( $output ) ) {
                foreach ( $vehicle_map as $vehicle_source => $vehicle_data ) {
                    foreach ( $output as $row ) {
                        if ( ( $row->Name === $vehicle_source ) && ( $row->Source === $vehicle_data[ 'source' ] ) ) {
                            $vehicle->$vehicle_data[ 'key' ]    = $row->Value;
                            break;
                        }
                    }
                }
            }

            if ( ( defined( 'DOING_AJAX' ) ) && ( DOING_AJAX ) ) {
                $vehicle        = json_encode( $vehicle );

                header( 'Content-Type: application/json' );
                header( 'Content-Length: ' . strlen( $vehicle ) );

                print $vehicle;
                exit;
            } else {
                return $vehicle;
            }
        }
    }
