<?php

    /**
     * AAMP API-related methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\API;

    class Core {
        function __construct() {
            add_action( 'wp_ajax_' . THEME__TEXTDOMAIN . '_api', array( '\AAMP\API\Core', 'consume' ) );
            add_action( 'wp_ajax_nopriv_' . THEME__TEXTDOMAIN . '_api', array( '\AAMP\API\Core', 'consume' ) );

            add_action( 'wp_ajax_' . THEME__TEXTDOMAIN . '_vrm_lookup', array( '\AAMP\API\VRM', 'lookup' ) );
            add_action( 'wp_ajax_nopriv_' . THEME__TEXTDOMAIN . '_vrm_lookup', array( '\AAMP\API\VRM', 'lookup' ) );
        }

        function __destruct() {
        }

        public static function consume( $path = '', $method = 'GET', $parameters = array() ) {
            if ( ( defined( 'DOING_AJAX' ) ) && ( DOING_AJAX ) ) {
                $path           = $_REQUEST[ 'path' ];
                $method         = $_REQUEST[ 'method' ];
                $parameters     = $_REQUEST[ 'parameters' ];
            }

            $request        = curl_init();

            curl_setopt( $request, CURLOPT_URL, get_bloginfo( 'url' ) . '/rest/' . $path );
            curl_setopt( $request, CURLOPT_HEADER, false );
            curl_setopt( $request, CURLOPT_VERBOSE, false );
            curl_setopt( $request, CURLOPT_RETURNTRANSFER, true );

            switch ( strtoupper( $method ) ) {
                case 'GET' :
                    curl_setopt( $request, CURLOPT_HTTPGET, true );
                    break;

                case 'POST' :
                    curl_setopt( $request, CURLOPT_POST, true );
                    curl_setopt( $request, CURLOPT_POSTFIELDS, $parameters );
                    break;
            }

            $response   = curl_exec( $request );

            curl_close( $request );

            if ( ( defined( 'DOING_AJAX' ) ) && ( DOING_AJAX ) ) {
                header( 'Content-Type: application/json' );
                header( 'Content-Length: ' . strlen( $response ) );

                print $response;
                exit;
            } else {
                return json_decode( $response );
            }
        }
    }
