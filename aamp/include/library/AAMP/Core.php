<?php

    /**
     * Initialization
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP;

    class Core {
        function __construct() {
            // include composer autoload
            $vendor_autoload    = get_template_directory() . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

            if ( file_exists( $vendor_autoload ) ) {
                require $vendor_autoload;
            }

            // register autoloader
            spl_autoload_register(
                function( $class ) {
                    foreach ( array( get_template_directory(), get_stylesheet_directory() ) as $location ) {
                        $path       = $location . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . str_replace( '\\', '/', $class ) . '.php';

                        if ( file_exists( $path ) ) {
                            require_once $path;
                        }
                    }
                }
            );

            // assets
            new Assets\Core;

            // widgets
            new Widgets\Core;

            // post types
            new PT\Core;

            // header and navigation enhancements
            new Header\Core;

            // footer enhancements
            new Footer\Core;

            // content improvements
            new Content\Core;
            
            // blog/posts actions and filters
            new Blog\Core;

            // customizer initialization
            new Customizer\Core;

            // archives actions and filters
            new Archives\Core;

            // siteorigin enhancements
            new SiteOrigin\Core;

            // gravityforms enhancements
            new GravityForms\Core;

            // load advanced custom fields presets
            new ACF\Core;

            // load AAMP API methods
            new API\Core;
        }

        function __destruct() {
        }
    }
