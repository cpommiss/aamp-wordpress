<?php

    /**
     * Widget Area-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Widgets;

    class Core {
        function __construct() {
            add_action( 'widgets_init', array( '\AAMP\Widgets\Core', 'init' ) );

            add_action( 'wp_ajax_' . THEME__TEXTDOMAIN . '_get_specific_sidebar', array( '\AAMP\Widgets\Core', 'get_specific_sidebar' ) );
            add_action( 'wp_ajax_nopriv_' . THEME__TEXTDOMAIN . '_get_specific_sidebar', array( '\AAMP\Widgets\Core', 'get_specific_sidebar' ) );
        }

        function __destruct() {
        }

        /**
         * Returns the requested sidebar of the current theme
         *
         * @since 1.0
         *
         * @return void
         */
        public static function get_specific_sidebar() {
            $sidebar    = ( isset( $_GET[ 'sidebar' ] ) ? sanitize_title( $_GET[ 'sidebar' ] ) : 'sidebar-primary' );

            if ( ob_get_length() !== false ) {
                ob_clean();
            }

            ob_start();

            dynamic_sidebar( $sidebar );

            $buffer     = ob_get_clean();

            print json_encode( array( 'output' => $buffer ) );
            exit;
        }

        /**
         * Register widget area.
         *
         * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
         *
         * @since 1.0
         */
        public static function init() {
            register_sidebar( array(
                'name'          => __( 'Sidebar (Blog)', THEME__TEXTDOMAIN ),
                'id'            => 'sidebar-primary',
                'description'   => __( 'Add widgets here to appear on News pages.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );
            
            register_sidebar( array(
                'name'          => __( 'Sidebar (Pages)', THEME__TEXTDOMAIN ),
                'id'            => 'sidebar-page',
                'description'   => __( 'Add widgets here to appear on CMS pages.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );

            register_sidebar( array(
                'name'          => __( 'Sidebar (Products)', THEME__TEXTDOMAIN ),
                'id'            => 'sidebar-products',
                'description'   => __( 'Add widgets here to appear on product pages.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );

            register_sidebar( array(
                'name'          => __( '"404/Not Found" Page', THEME__TEXTDOMAIN ),
                'id'            => 'widgets-404',
                'description'   => __( 'Add widgets here to appear on the "404 Not Found" page.', THEME__TEXTDOMAIN ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ) );
        }
    }