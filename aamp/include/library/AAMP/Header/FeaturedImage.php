<?php

    /**
     * Featured Image-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Header;

    class FeaturedImage {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Retreives the featured image based on the specified post ID
         *
         * @since 1.0
         *
         * @return string
         */
        public static function get_featured_image( $id_post = 0, $size = 'featured-short' ) {
            $id_image   = 0;
            $image      = '';

            switch ( true ) {
                case is_search() :
                    $post_type      = get_query_var( 'post_type' );

                    if ( $post_type ) {
                        $id_image   = self::find_archive_featured_image( $post_type );
                    }
                    break;

                case is_archive() :
                    $id_image       = self::find_archive_featured_image( get_post_type() );
                    break;

                case is_404() :
                    $id_image       = intval( \AAMP\Customizer\Core::get( 'cta_default_image_404' ) );
                    break;

                case is_front_page() :
                    $id_image       = get_post_thumbnail_id( get_the_ID() );
                    break;

                case is_home() :
                    $id_image       = self::find_archive_featured_image( 'post' );
                    break;

                default :
                    if ( $id_post === 0 ) {
                        $id_post    = intval( get_the_ID() );
                    }

                    if ( has_post_thumbnail( $id_post ) ) {
                        $id_image       = get_post_thumbnail_id( $id_post );
                    }
                    break;
            }

            if ( $id_image !== 0 ) {
                $image      = wp_get_attachment_image_url( $id_image, THEME__TEXTDOMAIN . '-' . $size );
            }

            if ( strlen( $image ) === 0 ) {
                if ( ( is_single() ) && ( $id_image = self::find_single_post_featured_image( get_post_type() ) ) ) {
                    $image      = wp_get_attachment_image_url( $id_image, THEME__TEXTDOMAIN . '-' . $size );
                } else {
                    $image      = wp_get_attachment_image_url( intval( attachment_url_to_postid( \AAMP\Customizer\Core::get( 'cta_default_image' ) ) ), THEME__TEXTDOMAIN . '-' . $size );
                }
            }

            return $image;
        }


        /**
         * Searches the ACF "Archive Call to Action" options page setting
         * for a featured image to use for a given post type.
         *
         * @since 1.0
         *
         * @return integer
         */
        public static function find_archive_featured_image( $post_type = 'post' ) {
            $id_image       = 0;

            if ( have_rows( 'archive_cta', 'option' ) ) {
                while ( have_rows( 'archive_cta', 'option' ) ) {
                    the_row();

                    if ( get_sub_field( 'post_type' ) == $post_type ) {
                        $id_image   = intval( get_sub_field( 'image' ) );
                        break;
                    }
                }
            }

            return $id_image;
        }


        /**
         * Searches the ACF "Single Post Call to Action Images" options page setting
         * for a featured image to use for a single post of a given post type.
         *
         * @since 1.0
         *
         * @return integer
         */
        public static function find_single_post_featured_image( $post_type = 'post' ) {
            $id_image       = 0;

            if ( have_rows( 'single_cta', 'option' ) ) {
                while ( have_rows( 'single_cta', 'option' ) ) {
                    the_row();

                    if ( get_sub_field( 'post_type' ) == $post_type ) {
                        $id_image   = intval( get_sub_field( 'image' ) );
                        break;
                    }
                }
            }

            return $id_image;
        }
    }

