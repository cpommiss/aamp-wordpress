<?php

    /**
     * Header-related Methods
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */
    namespace AAMP\Header;

    class Core {
        function __construct() {
            add_action( 'wp_ajax_' . THEME__TEXTDOMAIN . '_get_core_header', array( '\AAMP\Header\Core', 'get_core_header' ) );
            add_action( 'wp_ajax_nopriv_' . THEME__TEXTDOMAIN . '_get_core_header', array( '\AAMP\Header\Core', 'get_core_header' ) );
        }

        function __destruct() {
        }

        /**
         * Returns the header of the current theme
         *
         * @since 1.0
         *
         * @return void
         */
        public static function get_core_header() {
            if ( ob_get_length() !== false ) {
                ob_clean();
            }

            ob_start();

            get_template_part( 'parts/header/header', 'core' );

            $buffer     = ob_get_clean();

            print json_encode( array( 'output' => $buffer ) );
            exit;
        }

        /**
         * Returns whether or not the title is visible
         *
         * @since 1.0
         *
         * @return boolean
         */
        public static function is_title_visible() {
            switch ( true ) {
                case is_page() :
                    if ( get_field( 'title_show' ) ) {
                        return true;
                    } else {
                        return false;
                    }
                    break;

                case is_home() :
                case is_archive() :
                case is_category() :
                case is_tag() :
                case is_404() :
                case is_search() :
                    // TODO
                    return true;
                    break;

                default :
                    return true;
                    break;
            }
        }

        /**
         * Builds the title based on custom field toggles and/or
         * type of post or page being viewed.
         *
         * @since 1.0
         *
         * @return string
         */
        public static function build_title( $post_id = 0, $title_tag = 'h1', $subtitle_tag = 'p', $title_class = 'page-title', $subtitle_class = 'page-subtitle' ) {
            $title              = '';
            $title_alt          = get_field( 'title_alt', $post_id );
            $title_subtitle     = get_field( 'title_sub', $post_id );

            $title_before       = '<' . $title_tag . ' class="' . $title_class . '">';
            $title_after        = '</' . $title_tag . '>';
            $subtitle_before    = '<' . $subtitle_tag . ' class="' . $subtitle_class . '">';
            $subtitle_after     = '</' . $subtitle_tag . '>';

            switch ( true ) {
                case is_single() :
                    $overrides      = self::find_archive_title_override( get_post_type() );

                    if ( ( is_array( $overrides ) ) && ( !empty( $overrides ) ) ) {
                        $title_alt      = $overrides[ 'title' ];
                        $title_subtitle = $overrides[ 'subtitle' ];
                    }

                    $title      = $title_before . ( ( strlen( $title_alt ) ) ? $title_alt : get_the_title() ) . $title_after . ( ( strlen( $title_subtitle ) ) ? ( $subtitle_before . $title_subtitle . $subtitle_after ) : '' );
                    break;

                case is_home() :
                case is_search() :
                case is_category() :
                case is_tag() :
                case is_404() :
                case is_archive() :
                    if ( $post_id !== 0 ) {
                        $title      = $title_before . get_the_title( $post_id ) . $title_after;
                    } else {
                        switch ( true ) {
                            case is_home() :
                                $overrides  = self::find_archive_title_override( 'post' );

                                if ( ( is_array( $overrides ) ) && ( !empty( $overrides ) ) ) {
                                    $title_alt      = $overrides[ 'title' ];
                                    $title_subtitle = $overrides[ 'subtitle' ];
                                } else {
                                    $title      = $title_before . get_the_title( get_option( 'page_for_posts' ) ) . $title_after;
                                }

                                $title      = $title_before . ( ( strlen( $title_alt ) ) ? $title_alt : $title ) . $title_after . ( ( strlen( $title_subtitle ) ) ? ( $subtitle_before . $title_subtitle . $subtitle_after ) : '' );
                                break;

                            case is_search() :
                                $post_type      = ( ( strlen( get_query_var( 'post_type' ) ) ) ? strtolower( get_query_var( 'post_type' ) ) : get_post_type() );
                                $title          = 'Search';
                                $title_subtitle = sprintf( __( 'Results for "<strong>%s</strong>"', THEME__TEXTDOMAIN ), get_search_query() );

                                $overrides  = self::find_archive_title_override( $post_type );

                                if ( ( is_array( $overrides ) ) && ( !empty( $overrides ) ) ) {
                                    $title_alt      = $overrides[ 'title' ];
                                    $title_subtitle = $title;
                                }

                                $title      = $title_before . ( ( strlen( $title_alt ) ) ? $title_alt : $title ) . $title_after . ( ( strlen( $title_subtitle ) ) ? ( $subtitle_before . $title_subtitle . $subtitle_after ) : '' );
                                break;

                            case is_tag() :
                            case is_category() :
                                $title      = $title_before . get_the_archive_title() . $title_after;
                                break;

                            case is_archive() :
                                $overrides  = self::find_archive_title_override( get_post_type() );

                                if ( ( is_array( $overrides ) ) && ( !empty( $overrides ) ) ) {
                                    $title_alt      = $overrides[ 'title' ];
                                    $title_subtitle = $overrides[ 'subtitle' ];
                                }

                                $title      = $title_before . ( ( strlen( $title_alt ) ) ? $title_alt : get_the_archive_title() ) . $title_after . ( ( strlen( $title_subtitle ) ) ? ( $subtitle_before . $title_subtitle . $subtitle_after ) : '' );
                                break;

                            case is_404() :
                                $title      = $title_before . __( 'Page Not Found', THEME__TEXTDOMAIN ) . $title_after;
                                break;
                        }
                    }
                    break;

                default :
                    $title      = $title_before . ( ( strlen( $title_alt ) ) ? $title_alt : get_the_title() ) . $title_after . ( ( strlen( $title_subtitle ) ) ? ( $subtitle_before . $title_subtitle . $subtitle_after ) : '' );
                    break;
            }

            return $title;
        }

        /**
         * Searches the ACF "Archive Title Overrides" options page setting
         * for a title/subtitle override for a given post type.
         *
         * @since 1.0
         *
         * @return array
         */
        public static function find_archive_title_override( $post_type = 'post' ) {
            if ( strlen( $post_type ) ) {
                if ( have_rows( 'archive_titles', 'option' ) ) {
                    while ( have_rows( 'archive_titles', 'option' ) ) {
                        the_row();

                        $override_post_type     = get_sub_field( 'post_type' );

                        if ( $override_post_type == $post_type ) {
                            return array( 'title' => get_sub_field( 'title' ), 'subtitle' => get_sub_field( 'subtitle' ) );
                        }
                    }
                }
            }

            return false;
        }
    }

