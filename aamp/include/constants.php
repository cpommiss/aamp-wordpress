<?php
    /**
     * Theme Constants
     *
     * @package WordPress
     * @subpackage AAMP
     * @since 1.0
     */

    define( 'THEME__NAME', 'AAMP Global' );
    define( 'THEME__TEXTDOMAIN', 'aamp' );
    define( 'THEME__VERSION', '1.0.0' );

