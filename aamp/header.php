<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <link rel="profile" href="http://gmpg.org/xfn/11">

        <?php get_template_part( 'parts/header/header', 'icons' ); ?>
        <?php get_template_part( 'parts/header/header', 'extra' ); ?>

        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <?php get_template_part( 'parts/header/header', 'core' ); ?>

        <main class="site-content"<?php echo \AAMP\Content\Core::get_page_style(); ?>>
            <?php get_template_part( 'parts/header/header', 'title' ); ?>
