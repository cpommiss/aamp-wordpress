<?php

    the_posts_pagination( array(
        'prev_text' => '<i class="fa fa-chevron-left"></i><span class="screen-reader-text">' . __( 'Previous page', THEME__TEXTDOMAIN ) . '</span>',
        'next_text' => '<span class="screen-reader-text">' . __( 'Next page', THEME__TEXTDOMAIN ) . '</span><i class="fa fa-chevron-right"></i>',
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', THEME__TEXTDOMAIN ) . ' </span>',
    ) );
    