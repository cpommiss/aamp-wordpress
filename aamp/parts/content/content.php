<header class="single-post__meta">
    <h2 class="page-sub-title"><?php echo ( ( $title = get_field( 'title_alt', get_the_ID() ) ) ? $title : get_the_title( get_the_ID() ) ); ?></h2>

    <span class="single-post__meta--posted"><?php echo get_the_date( 'F jS, Y', get_the_ID() ); ?></span>
</header>

<?php the_content(); ?>