<?php if ( \AAMP\Header\Core::is_title_visible() ) : ?>
<header class="page-heading">
    <?php echo \AAMP\Header\Core::build_title(); ?>
</header>
<?php endif; ?>
