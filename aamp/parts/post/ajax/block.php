<?php global $wp_query; ?>
<?php $image = ( ( has_post_thumbnail( get_the_ID() ) ) ? wp_get_attachment_image_url( get_post_thumbnail_id( get_the_ID() ), THEME__TEXTDOMAIN . '-square' ) : 'https://via.placeholder.com/600x600/1F355E/FFFFFF?text=no+image+available' ); ?>
<div class="post-block result">
    <div class="post-block__inner">
        <div class="post-block__background" style="background-image: url(<?php echo $image; ?>);"></div>
        <div class="post-block__details">
            <h3><a href="<?php echo get_the_permalink( get_the_ID() ); ?>"><?php echo ( ( $title = get_field( 'title_alt', get_the_ID() ) ) ? $title : get_the_title( get_the_ID() ) ); ?></a></h3>
            <span class="post-block__details--posted"><?php echo get_the_date( 'F jS, Y', get_the_ID() ); ?></span>

            <div class="post-block__excerpt">
                <p><?php echo get_the_excerpt( get_the_ID() ); ?></p>

                <!--<a href="<?php echo get_the_permalink( get_the_ID() ); ?>" class="btn btn-primary"><?php _e( 'Read More', THEME__TEXTDOMAIN ); ?></a>-->
            </div>
        </div>
    </div>
</div>