<article class="container">
    <div class="row">
        <div class="content col-lg-8 col-md-12">
            <header class="single-post__meta">
                <small class="single-post__meta--posted"><?php echo get_the_date( 'F jS, Y', get_the_ID() ); ?></small>
            </header>

            <hr />

            <?php the_content(); ?>
        </div>
        <div class="sidebar col-lg-4 col-md-12">
            <?php dynamic_sidebar( 'sidebar-primary' ); ?>
        </div>
    </div>
</article>


