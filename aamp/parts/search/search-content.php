<div class="search-content">
    <a href="<?php echo get_the_permalink( get_the_ID() ); ?>" class="search-content__link">
        <h3><?php echo get_the_title( get_the_ID() ); ?></h3>
        <p><?php echo get_the_excerpt( get_the_ID() ); ?></p>
        <?php if ( $post_type = get_post_type_object( get_post_type( get_the_ID() ) ) ) : ?><span class="search-content__label"><?php echo $post_type->labels->singular_name; ?></span><?php endif; ?>
    </a>
</div>