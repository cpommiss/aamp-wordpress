<?php
    wp_nav_menu(
        array(
            'theme_location'    => 'primary',
            'container'         => false,
            'menu_class'        => 'navbar-nav',
            'walker'            => new \AAMP_Nav_Walker_Default
        )
    )
?>

