
<?php
    wp_nav_menu(
        array(
            'theme_location'    => 'social',
            'container'         => false,
            'menu_class'        => 'navbar-social'
        )
    );
?>
