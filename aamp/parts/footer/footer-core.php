
        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <?php get_template_part( 'parts/navigation/navigation', 'epilogue' ); ?>
                    </div>
                    <div class="col-lg-6">
                        <?php get_template_part( 'parts/footer/footer', 'copyright' ); ?>
                    </div>
                </div>

                <hr />

                <div class="row">
                    <div class="col-lg-2">
                        <?php get_template_part( 'parts/navigation/navigation', 'social' ); ?>
                    </div>
                    <div class="col-lg-10">
                        <?php get_template_part( 'parts/navigation/navigation', 'brands' ); ?>
                    </div>
                </div>
            </div>
        </footer>

        <div id="site-modal" class="modal fade zoom-out site-modal" tabindex="-1" role="dialog" aria-labelledby="site-modal" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button type="button" data-dismiss="modal"></button>

                    <div class="modal-content__inner"></div>
                </div>
            </div>
        </div>

        <?php get_template_part( 'parts/footer/footer', 'extra' ); ?>

