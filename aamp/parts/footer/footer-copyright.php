
<span class="copyright"><?php echo sprintf( __( 'Copyright &copy; %s. All rights reserved. %s is a Power Brand of AAMP Global.', THEME__TEXTDOMAIN ), date( 'Y' ), get_bloginfo( 'name' ) ); ?></span>