<?php

    /*
    Widget Name: Search by Vehicle
    Description: Creates a Search by Vehicle widget.
    */

    class Theme_SBV extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-sbv',

                __( 'Search by Vehicle', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a Search by Vehicle widget.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'style' => array(
                        'type'    => 'select',
                        'label'   => __( 'Style', THEME__TEXTDOMAIN ),
                        'options' => array(
                            'mmy'   => __( 'Make/Model/Year', THEME__TEXTDOMAIN ),
                            'mmyt'  => __( 'Make/Model/Year/Trim', THEME__TEXTDOMAIN )
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-sbv', __FILE__, 'Theme_SBV' );
