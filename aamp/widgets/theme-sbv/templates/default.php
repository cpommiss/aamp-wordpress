<?php $id = uniqid(); ?>
<div class="sbv sbv--<?php echo $instance[ 'style' ]; ?>" data-style="<?php echo $instance[ 'style' ]; ?>" data-region="2" data-api-url="/rest/" data-store-url="<?php echo \AAMP\Content\URL::get_shop_url(); ?>/">
    <div class="sbv__photo" v-bind:class="photo.status">
        <div class="sbv__photo-inner" v-bind:style="{'background-image': 'url(' + photo.url + ')'}"></div>
    </div>

    <form action="<?php echo \AAMP\Content\URL::get_shop_url(); ?>/" method="GET" class="sbv__form" id="sbv-<?php echo $id; ?>">
        <input type="hidden" name="fc" value="module">
        <input type="hidden" name="module" value="aampsearchbyvehicle">
        <input type="hidden" name="controller" value="results">

        <input type="hidden" name="region" value="2">

        <h3 class="widget__title"><?php _e( 'Search by Vehicle', THEME__TEXTDOMAIN ); ?></h3>

        <div class="sbv__fields" v-bind:class="loading">
            <div class="sbv__field" data-field-type="make">
                <label for="sbv_<?php echo $id; ?>_make"><?php _e( 'Make', THEME__TEXTDOMAIN ); ?></label>
                <select v-model="make.current" v-on:change="select" name="make" id="sbv_<?php echo $id; ?>_make">
                    <option value="" disabled><?php _e( 'Select a Make', THEME__TEXTDOMAIN ); ?></option>

                    <option v-for="option in make.makes" v-bind:value="option.id_make">{{ option.name }}</option>
                </select>
            </div>

            <div class="sbv__field" data-field-type="model">
                <label for="sbv_<?php echo $id; ?>_model"><?php _e( 'Model', THEME__TEXTDOMAIN ); ?></label>
                <select v-model="model.current" v-on:change="select" name="model" id="sbv_<?php echo $id; ?>_model">
                    <option value="" disabled><?php _e( 'Select a Model', THEME__TEXTDOMAIN ); ?></option>

                    <option v-for="option in model.models" v-bind:value="option.name">{{ option.name }}</option>
                </select>
            </div>

            <div class="sbv__field" data-field-type="year">
                <label for="sbv_<?php echo $id; ?>_year"><?php _e( 'Year', THEME__TEXTDOMAIN ); ?></label>
                <select v-model="year.current" v-on:change="select" name="year" id="sbv_<?php echo $id; ?>_year">
                    <option value="" disabled><?php _e( 'Select a Model Year', THEME__TEXTDOMAIN ); ?></option>

                    <option v-for="option in year.years" v-bind:value="option.id_model">{{ option.year }}</option>
                </select>
            </div>

            <div class="sbv__field" data-field-type="trim">
                <label for="sbv_<?php echo $id; ?>_trim"><?php _e( 'Trim', THEME__TEXTDOMAIN ); ?></label>
                <select v-model="trim.current" v-on:change="select" name="trim" id="sbv_<?php echo $id; ?>_trim">
                    <option value="" disabled><?php _e( 'Select a Trim Level', THEME__TEXTDOMAIN ); ?></option>

                    <option v-for="option in trim.trims" v-bind:value="option.id_trim">{{ option.name }}</option>
                </select>
            </div>

            <div class="sbv__field" data-field-type="button">
                <button name="submit" type="submit" class="btn btn-secondary" v-bind:disabled="isNaN(parseInt(trim.current))"><?php _e( 'Search', THEME__TEXTDOMAIN ); ?></button>
            </div>
        </div>
    </form>
</div>