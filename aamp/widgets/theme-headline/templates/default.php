<?php
    $tag        = $instance[ 'headline' ][ 'tag' ];
    $classes    =   array(
                        'section-title',
                        'text-' . $instance[ 'headline' ][ 'alignment' ]
                    );

    if ( strlen( $instance[ 'headline' ][ 'text' ] ) ) :
        $classes[]      = 'has-headline';
    endif;

    if ( strlen( $instance[ 'subheadline' ][ 'text' ] ) ) :
        $classes[]      = 'has-subheadline';
    endif;
?>
<<?php echo $tag; ?> class="<?php echo implode( ' ', $classes ); ?>"<?php echo ( ( ( isset( $instance[ 'headline' ][ 'color' ] ) ) && ( strlen( $instance[ 'headline' ][ 'color' ] ) ) ) ? ( ' style="color: ' . $instance[ 'headline' ][ 'color' ] . ';"' ) : '' ); ?>>
    <?php if ( strlen( $instance[ 'headline' ][ 'text' ] ) ) : ?><?php echo $instance[ 'headline' ][ 'text' ]; ?><?php endif; ?>

    <?php if ( strlen( $instance[ 'subheadline' ][ 'text' ] ) ) : ?>
    <span class="section-title__subtitle text-<?php echo $instance[ 'subheadline' ][ 'alignment' ]; ?>"<?php echo ( ( ( isset( $instance[ 'subheadline' ][ 'color' ] ) ) && ( strlen( $instance[ 'subheadline' ][ 'color' ] ) ) ) ? ( ' style="color: ' . $instance[ 'subheadline' ][ 'color' ] . ';"' ) : '' ); ?>>
        <?php echo str_replace( "\n", '<br />', $instance[ 'subheadline' ][ 'text' ] ); ?>
    </span>
    <?php endif; ?>
</<?php echo $tag; ?>>