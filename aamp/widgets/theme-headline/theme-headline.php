<?php

    /*
    Widget Name: Custom Headline
    Description: Creates a headline (and optional sub-headline).
    */

    class Theme_Custom_Headline extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-custom-headline',

                __( 'Custom Headline', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a headline (and optional sub-headline).', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'headline' => array(
                        'type'   => 'section',
                        'label'  => __( 'Headline', THEME__TEXTDOMAIN ),
                        'hide'   => false,
                        'fields' => array(
                            'text'      => array(
                                'type'  => 'text',
                                'label' => __( 'Text', THEME__TEXTDOMAIN )
                            ),
                            'color'     => array(
                                'type'    => 'color',
                                'label'   => __( 'Color', THEME__TEXTDOMAIN ),
                                'default' => '#000000'
                            ),
                            'alignment' => array(
                                'type'    => 'select',
                                'label'   => __( 'Alignment', THEME__TEXTDOMAIN ),
                                'options' => array(
                                    'left'   => __( 'Left-aligned', THEME__TEXTDOMAIN ),
                                    'center' => __( 'Centered', THEME__TEXTDOMAIN ),
                                    'right'  => __( 'Right-aligned', THEME__TEXTDOMAIN )
                                )
                            ),
                            'tag'       => array(
                                'type'    => 'select',
                                'label'   => __( 'Tag/Level', THEME__TEXTDOMAIN ),
                                'default' => 'h3',
                                'options' => array(
                                    'h1' => __( 'H1', THEME__TEXTDOMAIN ),
                                    'h2' => __( 'H2', THEME__TEXTDOMAIN ),
                                    'h3' => __( 'H3 (Default)', THEME__TEXTDOMAIN ),
                                    'h4' => __( 'H4', THEME__TEXTDOMAIN ),
                                    'h5' => __( 'H5', THEME__TEXTDOMAIN ),
                                    'h6' => __( 'H6', THEME__TEXTDOMAIN )
                                )
                            )
                        )
                    ),
                    'subheadline' => array(
                        'type'   => 'section',
                        'label'  => __( 'Sub-headline', THEME__TEXTDOMAIN ),
                        'hide'   => false,
                        'fields' => array(
                            'text'      => array(
                                'type'  => 'textarea',
                                'label' => __( 'Text', THEME__TEXTDOMAIN ),
                                'rows'  => 4
                            ),
                            'color'     => array(
                                'type'    => 'color',
                                'label'   => __( 'Color', THEME__TEXTDOMAIN ),
                                'default' => '#000000'
                            ),
                            'alignment' => array(
                                'type'    => 'select',
                                'label'   => __( 'Alignment', THEME__TEXTDOMAIN ),
                                'options' => array(
                                    'left'   => __( 'Left-aligned', THEME__TEXTDOMAIN ),
                                    'center' => __( 'Centered', THEME__TEXTDOMAIN ),
                                    'right'  => __( 'Right-aligned', THEME__TEXTDOMAIN )
                                )
                            )
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-custom-headline', __FILE__, 'Theme_Custom_Headline' );
