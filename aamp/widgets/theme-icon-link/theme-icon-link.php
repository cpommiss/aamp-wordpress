<?php

    /*
    Widget Name: Icon with Link
    Description: Creates a block with an icon and link.
    */

    class Theme_Icon_Link extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-icon-link',

                __( 'Icon with Link', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a block with an icon and link.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'icon' => array(
                        'type' => 'media',
                        'label' => __( 'Choose an icon image', THEME__TEXTDOMAIN ),
                        'choose' => __( 'Choose an icon image', THEME__TEXTDOMAIN ),
                        'update' => __( 'Set icon image', THEME__TEXTDOMAIN ),
                        'library' => 'image'
                    ),
                    'title' => array(
                        'type'    => 'text',
                        'label'   => __( 'Title', THEME__TEXTDOMAIN )
                    ),
                    'subtitle' => array(
                        'type'    => 'text',
                        'label'   => __( 'Subtitle', THEME__TEXTDOMAIN )
                    ),
                    'link' => array(
                        'type'    => 'link',
                        'label'   => __( 'Link/URL', THEME__TEXTDOMAIN )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-icon-link', __FILE__, 'Theme_Icon_Link' );
