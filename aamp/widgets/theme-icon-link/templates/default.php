<h3 class="widget__title">
    <a href="<?php echo sow_esc_url( $instance[ 'link' ] ); ?>" class="widget__link">
        <span class="widget__icon" style="background-image: url(<?php echo wp_get_attachment_image_url( $instance[ 'icon' ], 'full' ); ?>);"><img src="<?php echo wp_get_attachment_image_url( $instance[ 'icon' ], 'full' ); ?>"></span>

        <?php echo $instance[ 'title' ]; ?><?php if ( strlen( $instance[ 'subtitle' ] ) ) : ?> <span class="widget__subtitle"><?php echo $instance[ 'subtitle' ]; ?></span><?php endif; ?>
    </a>
</h3>