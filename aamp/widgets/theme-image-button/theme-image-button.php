<?php

    /*
    Widget Name: Image Button
    Description: Creates a block containing an image and an optional title.
    */

    class Theme_Image_Button extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-image-button',

                __( 'Image Button', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a block containing an image and an optional title.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'icon' => array(
                        'type' => 'media',
                        'label' => __( 'Choose an icon image', THEME__TEXTDOMAIN ),
                        'choose' => __( 'Choose an icon image', THEME__TEXTDOMAIN ),
                        'update' => __( 'Set icon image', THEME__TEXTDOMAIN ),
                        'library' => 'image'
                    ),
                    'title' => array(
                        'type'    => 'text',
                        'label'   => __( 'Title', THEME__TEXTDOMAIN )
                    ),
                    'link' => array(
                        'type'    => 'link',
                        'label'   => __( 'Link/URL', THEME__TEXTDOMAIN )
                    ),
                    'link_target' => array(
                        'type'    => 'select',
                        'label'   => __( 'Link/URL Behavior', THEME__TEXTDOMAIN ),
                        'options' => array(
                            '_self'   => __( 'Open link in the Same window', THEME__TEXTDOMAIN ),
                            '_blank'  => __( 'Open link in a new window', THEME__TEXTDOMAIN ),
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-image-button', __FILE__, 'Theme_Image_Button' );
