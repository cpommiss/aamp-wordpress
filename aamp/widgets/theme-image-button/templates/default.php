<a href="<?php echo sow_esc_url( $instance[ 'link' ] ); ?>" target="<?php echo $instance[ 'link_target' ]; ?>" class="widget__link">
    <span class="widget__image"><img src="<?php echo wp_get_attachment_image_url( $instance[ 'icon' ], 'full' ); ?>"></span>
    <?php if ( strlen( $instance[ 'title' ] ) ) : ?><span class="widget__caption"><?php echo $instance[ 'title' ]; ?></span><?php endif; ?>
</a>