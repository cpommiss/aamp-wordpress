<div class="icon">
    <div class="icon__inner" style="background-color: <?php echo $instance[ 'imagery' ][ 'bgcolor' ]; ?>; color: <?php echo $instance[ 'imagery' ][ 'color' ]; ?>;">
        <?php if ( !empty( $instance[ 'imagery' ][ 'icon' ] ) ) : ?>
        <i class="<?php echo $instance[ 'imagery' ][ 'icon' ]; ?>"></i>
        <?php else : ?>
        <div class="icon__image" style="background-image: url(<?php echo wp_get_attachment_image_url( $instance[ 'imagery' ][ 'image' ], 'full' ); ?>);"></div>
        <?php endif; ?>
    </div>
</div>
<div class="content">
    <?php echo wpautop( $instance[ 'content' ][ 'content' ] ); ?>
</div>