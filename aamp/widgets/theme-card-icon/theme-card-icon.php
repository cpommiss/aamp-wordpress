<?php

    /*
    Widget Name: Card with Icon
    Description: Creates a card with an icon and link.
    */

    class Theme_Card_Icon extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-card-icon',

                __( 'Card with Icon', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a card with an icon and link.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'imagery' => array(
                        'type' => 'section',
                        'label' => __( 'Icon/Image', THEME__TEXTDOMAIN ),
                        'hide' => false,
                        'fields' => array(
                            'image' => array(
                                'type' => 'media',
                                'label' => __( 'Icon Image', THEME__TEXTDOMAIN ),
                                'choose' => __( 'Choose an icon image', THEME__TEXTDOMAIN ),
                                'update' => __( 'Set icon image', THEME__TEXTDOMAIN ),
                                'library' => 'image'
                            ),
                            'icon' => array(
                                'type' => 'icon',
                                'label'   => __( 'Icon', THEME__TEXTDOMAIN )
                            ),
                            'color' => array(
                                'type'  => 'color',
                                'label'     => __( 'Color (Foreground)', THEME__TEXTDOMAIN )
                            ),
                            'bgcolor' => array(
                                'type'  => 'color',
                                'label'     => __( 'Color (Background)', THEME__TEXTDOMAIN )
                            )
                        )
                    ),
                    'content' => array(
                        'type' => 'section',
                        'label' => __( 'Title/Content', THEME__TEXTDOMAIN ),
                        'hide' => false,
                        'fields' => array(
                            'title' => array(
                                'type'    => 'text',
                                'label'   => __( 'Title', THEME__TEXTDOMAIN )
                            ),
                            'content' => array(
                                'type'    => 'tinymce',
                                'label'   => __( 'Content', THEME__TEXTDOMAIN )
                            ),
                            'link' => array(
                                'type'    => 'link',
                                'label'   => __( 'Link/URL', THEME__TEXTDOMAIN )
                            )
                        )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-card-icon', __FILE__, 'Theme_Card_Icon' );
