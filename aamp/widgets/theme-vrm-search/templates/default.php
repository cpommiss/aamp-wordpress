<div class="vrm-search">
    <h3 class="widget__title"><?php echo $instance[ 'title' ]; ?></h3>

    <form class="vrm-search__search" action="<?php echo sow_esc_url( $instance[ 'page' ] ); ?>" method="post">
        <input type="hidden" name="region" value="<?php echo intval( $instance[ 'region' ] ); ?>">

        <ul class="vrm-search__fields">
            <li class="vrm-search__input">
                <input name="vrm" type="text" placeholder="<?php _e( 'Search', THEME__TEXTDOMAIN ); ?>">
            </li>
            <li class="vrm-search__button">
                <button type="submit" class="btn btn-inverse"><i class="fa fa-search"></i></button>
            </li>
        </ul>
    </form>
</div>
