<?php

    /*
    Widget Name: VRM Search
    Description: Creates a VRM search widget.
    */

    class Theme_VRM_Search extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-vrm-search',

                __( 'VRM Search', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a VRM search widget.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'title' => array(
                        'type'  => 'text',
                        'label' => __( 'Title', THEME__TEXTDOMAIN ),
                        'default' => __( 'Search by VRM', THEME__TEXTDOMAIN )
                    ),
                    'region' => array(
                        'type'    => 'select',
                        'label'   => __( 'Region', THEME__TEXTDOMAIN ),
                        'description' => __( 'Select the region of the Vehicle Configuration database to match the vehicle against.', THEME__TEXTDOMAIN ),
                        'options' => array(
                            '1'   => __( 'North America', THEME__TEXTDOMAIN ),
                            '2'     => __( 'Europe', THEME__TEXTDOMAIN )
                        )
                    ),
                    'page' => array(
                        'type'  => 'link',
                        'label' => __( 'VRM Lookup Page', THEME__TEXTDOMAIN ),
                        'description' => __( 'Select the page that contains the VRM Lookup widget which can display the results of the VRM lookup.', THEME__TEXTDOMAIN )
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-vrm-search', __FILE__, 'Theme_VRM_Search' );
