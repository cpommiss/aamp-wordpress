<?php if ( !empty( $instance[ 'title' ] ) ) : ?><h3 class="widget__title"><?php echo $instance[ 'title' ]; ?></h3><?php endif; ?>
<?php if ( !empty( $instance[ 'caption' ] ) ) : ?><div class="widget__caption"><p><?php echo $instance[ 'caption' ]; ?></p></div><?php endif; ?>

<form action="<?php echo \AAMP\Content\URL::get_shop_url(); ?>/search" class="widget__form form-inline">
    <input type="hidden" name="controller" value="search">
    <input type="hidden" name="orderby" value="position">
    <input type="hidden" name="orderway" value="desc">
    <input type="hidden" name="submit_search" value="Search">

    <input class="form-control" name="search_query" type="text" placeholder="Search">
    <button class="btn btn-lg" type="submit"><?php _e( 'Search', THEME__TEXTDOMAIN ); ?></button>
</form>
