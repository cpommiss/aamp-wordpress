<?php

    /*
    Widget Name: Product Search
    Description: Creates a product search form widget.
    */

    class Theme_Product_Search extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-product-search',

                __( 'Product Search', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a product search form widget.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'title'     => array(
                        'type'  => 'text',
                        'label' => __( 'Title', THEME__TEXTDOMAIN ),
                        'default' => 'Search'
                    ),
                    'caption'   => array(
                        'type'  => 'textarea',
                        'label' => __( 'Caption/Description', THEME__TEXTDOMAIN ),
                        'rows'  => 3
                    )
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-product-search', __FILE__, 'Theme_Product_Search' );
