<?php if ( ( !empty( \Theme_VRM_Lookup::$data ) ) && ( isset( \Theme_VRM_Lookup::$data->categories ) ) ) : ?>
<h3 class="widget__title"><?php echo $instance[ 'title' ]; ?></h3>
<div class="widget__content">
    <ul class="category-filter category-filter--root">
        <li class="category-filter__item category-filter__item--all category-filter__item--active"><a href="javascript:;" data-category-id="" class="category-filter__link"><?php _e( 'View All', THEME__TEXTDOMAIN ); ?></a></li>
        <?php echo \Theme_VRM_Category_Filtering::traverse_category_tree( \Theme_VRM_Lookup::$data->categories ); ?>
    </ul>
</div>
<?php endif; ?>