<?php

    /*
    Widget Name: VRM Category Filtering
    Description: Creates a category filtering widget for use on VRM results pages.
    */

    class Theme_VRM_Category_Filtering extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-vrm-category-filtering',

                __( 'VRM Category Filtering', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a category filtering widget for use on VRM results pages.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(
                    'title' => array(
                        'type'  => 'text',
                        'label' => __( 'Title', THEME__TEXTDOMAIN ),
                        'default' => __( 'Product Categories', THEME__TEXTDOMAIN )
                    ),
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }

        public static function traverse_category_tree( $categories, $content = '' ) {
            foreach ( $categories as $category ) {
                if ( $category->enabled ) {
                    $content        .=  '<li class="category-filter__item">';

                    $content        .=  sprintf(    '<a href="javascript:;" data-category-id="%d" class="category-filter__link">%s</a>',
                                                    $category->id_category,
                                                    $category->language->name );

                    if ( !empty( $category->children ) ) {
                        $content        .=  '<ul class="category-filter category-filter--nested">' . self::traverse_category_tree( $category->children ) . '</ul>';
                    }

                    $content        .=  '</li>';
                }
            }

            return $content;
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-vrm-category-filtering', __FILE__, 'Theme_VRM_Category_Filtering' );
