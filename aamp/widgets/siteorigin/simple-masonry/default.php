<?php
/**
 * @var $args
 * @var $items
 * @var $layouts
 */
?>
<?php
	$is_gallery = intval( $instance['is_gallery'] );

	if ( $is_gallery ) $id_gallery = 'sow-layout-slider-gallery-' . uniqid();

	if( !empty( $instance['widget_title'] ) ) echo $args['before_title'] . esc_html( $instance['widget_title'] ) . $args['after_title']
?>
<div class="sow-masonry-grid<?php echo ( ( $is_gallery ) ? ' sow-masonry-grid-gallery' : '' ); ?>"
	 data-layouts="<?php echo esc_attr( json_encode( $layouts ) ) ?>" data-gallery-mode="; ?>">
	<?php
	if( ! empty( $items ) ) {
		foreach ( $items as $item ) {
			$src        = wp_get_attachment_image_src( $item['image'], 'full' );
			$src        = empty( $src ) ? '' : $src[0];
			$title      = empty( $item['title'] ) ? '' : $item['title'];
			$new_window = ! empty( $item['new_window'] );
			$link_self	= ! empty( $item['link_to_image'] );
			$url        = empty( $item['url'] ) ? ( ( !$link_self ) ? '' : $src ) : $item['url'];
			$url_parts	= pathinfo( $url );
			$is_modal	= ( ( ( $is_gallery ) && ( in_array( $url_parts[ 'extension' ], array( 'jpg', 'jpeg', 'gif', 'png' ) ) ) !== false ) ? true : false );
			?>
			<div class="sow-masonry-grid-item" data-col-span="<?php echo esc_attr( $item['column_span'] ) ?>" data-row-span="<?php echo esc_attr( $item['row_span'] ) ?>">
				<?php if ( ! empty( $url ) ) : ?>
				<a href="<?php echo sow_esc_url( $url ) ?>" <?php if ( $new_window ) { ?>target="_blank" <?php } ?><?php echo ( ( $is_modal ) ? ( ' class="fresco" data-fresco-caption="' . $title . '" data-fresco-group="' . $id_gallery . '"' ) : '' ); ?>>
					<?php endif; ?>

					<?php echo wp_get_attachment_image( $item['image'], 'large', false, array( 'title' => esc_attr( $title ) ) ); ?>

					<?php if ( ! empty( $url ) ) : ?>
				</a>
			<?php endif; ?>
			</div>
			<?php
		}
	}
	?>

</div>
