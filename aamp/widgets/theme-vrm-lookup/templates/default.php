<?php $results = \Theme_VRM_Lookup::fetch_results(); ?>
<?php if ( ( is_array( $results ) ) && ( get_object_vars( $results[ 'response' ] ) ) ) : ?>
    <div class="vrm-vehicle">
        <div class="row">
            <div class="col-lg-12 col-md-8 vrm-vehicle__details">
                <h3><?php _e( 'Vehicle Details', THEME__TEXTDOMAIN ); ?></h3>
                <p><?php _e( 'We\'ve identified this vehicle using the VRM you provided:', THEME__TEXTDOMAIN ); ?></p>

                <h4 class="vrm-vehicle__name"><?php echo $results[ 'response' ]->year . ' ' . $results[ 'response' ]->make . ' ' . $results[ 'response' ]->model; ?></h4>

                <dl class="vrm-vehicle__specs">
                    <dt><?php _e( 'Registration Number', THEME__TEXTDOMAIN ); ?></dt>
                    <dd><?php echo strtoupper( $results[ 'vrm' ] ); ?></dd>

                    <dt><?php _e( 'Body Type', THEME__TEXTDOMAIN ); ?></dt>
                    <dd><?php echo $results[ 'response' ]->doors . ' ' . $results[ 'response' ]->body_style; ?></dd>

                    <dt><?php _e( 'Colour', THEME__TEXTDOMAIN ); ?></dt>
                    <dd><?php echo $results[ 'response' ]->color; ?></dd>

                    <?php if ( strlen( $results[ 'response' ]->registered_on ) === 8 ) : ?>
                    <dt><?php _e( 'Registration Date', THEME__TEXTDOMAIN ); ?></dt>
                    <dd><?php echo date( 'm/Y', mktime( 0, 0, 0, intval( substr( $results[ 'response' ]->registered_on, 2, 2 ) ), intval( substr( $results[ 'response' ]->registered_on, 0, 2 ) ), intval( substr( $results[ 'response' ]->registered_on, 4 ) ) ) ); ?></dd>
                    <?php endif; ?>
                </dl>
            </div>
            <!--
            <div class="col-lg-5 col-md-4 vrm-vehicle__photo">
                <?php if ( ( isset( $results[ 'photo' ] ) ) && ( !empty( $results[ 'photo' ]->url ) ) ) : ?>
                <img src="<?php echo $results[ 'photo' ]->url; ?>">
                <?php else : ?>
                <div class="vrm-vehicle__photo--empty"><?php _e( 'No photo available', THEME__TEXTDOMAIN ); ?></div>
                <?php endif; ?>
            </div>
            -->
        </div>
    </div>

    <?php if ( ( get_object_vars( $results[ 'vehicle' ] ) ) && ( ( $results[ 'vehicle' ]->mode !== 1 ) || ( $results[ 'vehicle' ]->year != $results[ 'response' ]->year ) ) ) : ?>
    <div class="vrm-vehicle-actual">
        <div class="row">
            <div class="col-lg-12">
                <i class="fa fa-info-circle" aria-hidden="true"></i> <?php _e( 'Showing products compatible with:', THEME__TEXTDOMAIN ); ?>

                <ul class="vrm-vehicle-actual__fields">
                    <li>
                        <span class="vrm-vehicle-actual__fields--label"><?php _e( 'Year', THEME__TEXTDOMAIN ); ?></span>
                        <?php echo $results[ 'vehicle' ]->year; ?>
                    </li>
                    <li>
                        <span class="vrm-vehicle-actual__fields--label"><?php _e( 'Make', THEME__TEXTDOMAIN ); ?></span>
                        <?php echo $results[ 'vehicle' ]->make; ?>
                    </li>
                    <li>
                        <span class="vrm-vehicle-actual__fields--label"><?php _e( 'Model', THEME__TEXTDOMAIN ); ?></span>
                        <?php echo $results[ 'vehicle' ]->model; ?>
                    </li>
                    <li>
                        <span class="vrm-vehicle-actual__fields--label"><?php _e( 'Additional Information', THEME__TEXTDOMAIN ); ?></span>
                        <?php echo ( ( strlen( $results[ 'vehicle' ]->trim ) ) ? $results[ 'vehicle' ]->trim : '&mdash;' ); ?>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p><em><?php _e( 'We couldn\'t find your exact vehicle in our database, but we did find a similar vehicle, shown above. You can try using our Vehicle Search to find your exact vehicle.' ); ?></em></p>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="vrm-products">
        <?php if ( ( isset( $results[ 'dataset' ]->products ) ) && ( !empty( $results[ 'dataset' ]->products ) ) ) : ?>
            <div class="products-wrap products-wrap--grid">
                <div class="row products">
                    <?php foreach ( $results[ 'dataset' ]->products as $product ) : ?>
                        <?php $product_link = \Theme_VRM_Lookup::format_product_url( $product->categories, $product->id_category_default, $product->reference ); ?>
                        <?php $product_image = \Theme_VRM_Lookup::format_image_url( $product->images, $product->reference ); ?>

                        <div class="products__tile products__tile--active col-lg-4 col-md-6 col-sm-6 col-xs-12 <?php echo \Theme_VRM_Lookup::product_category_classes( $product->categories ); ?>" data-href="<?php echo $product_link; ?>">
                            <figure>
                                <div class="products__tile-image">
                                    <a href="<?php echo $product_link; ?>">
                                        <?php if ( !empty( $product_image ) ) : ?>
                                            <img src="<?php echo $product_image; ?>" alt="<?php echo $product->name; ?>">
                                        <?php else : ?>
                                            <img src="https://via.placeholder.com/300x300/1F355E/FFFFFF?text=%20" alt="<?php echo $product->name; ?>">
                                        <?php endif; ?>
                                    </a>
                                </div>

                                <figcaption>
                                    <h3 class="products__tile-title"><a href="<?php echo $product_link; ?>"><?php echo $product->name; ?></a></h3>

                                    <div class="products__tile-meta">
                                        <span class="products__tile-meta--reference"><?php echo strtoupper( $product->reference ); ?></span>
                                    </div>

                                    <div class="products__tile-desc">
                                        <?php if ( strlen( $product->description_short ) ) : ?>
                                            <p><?php echo substr( strip_tags( $product->description_short ), 0, 360 ); ?>...</p>
                                        <?php else : ?>
                                            <p><?php echo substr( $product->name, 0, 360 ); ?>...</p>
                                        <?php endif; ?>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php else : ?>
            <div class="alert alert-info">
                <p><?php _e( 'Sorry, but no products were found to be specifically marked as compatible with your vehicle. You may have better results using our <strong>Vehicle Search</strong> feature at the top of the page.', THEME__TEXTDOMAIN ); ?></p>
            </div>
        <?php endif; ?>
    </div>
<?php else : ?>
    <div class="alert alert-info">
        <p><?php _e( 'Sorry, but we could not identify the vehicle associated with the VRM you entered.', THEME__TEXTDOMAIN ); ?></p>
    </div>
<?php endif; ?>