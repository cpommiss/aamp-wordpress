<?php

    /*
    Widget Name: VRM Lookup
    Description: Creates a VRM lookup widget.
    */

    class Theme_VRM_Lookup extends SiteOrigin_Widget {
        public static $data = array();

        function __construct() {
            parent::__construct(
                THEME__TEXTDOMAIN . '-vrm-lookup',

                __( 'VRM Lookup', THEME__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a VRM lookup widget.', THEME__TEXTDOMAIN )
                ),

                array(),

                array(),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }

        public static function fetch_results() {
            $results    = array();

            if ( isset( $_POST[ 'vrm' ] ) ) {
                $vrm        = sanitize_text_field( $_POST[ 'vrm' ] );
                $region     = ( ( isset( $_POST[ 'region' ] ) ) ? intval( $_POST[ 'region' ] ) : 1 );

                if ( strlen( $vrm ) ) {
                    $results[ 'vrm' ]       = $vrm;
                    $results[ 'response' ]  = \AAMP\API\VRM::lookup( $vrm );

                    if ( is_object( $results[ 'response' ] ) ) {
                        $results[ 'vehicle' ]   = \AAMP\API\Core::consume( 'v2/vehicle/lookup/config/from-text', 'POST', array( 'region' => $region, 'make' => $results[ 'response' ]->make, 'model' => $results[ 'response' ]->model, 'year' => $results[ 'response' ]->year, 'trim' => '' ) );

                        if ( is_object( $results[ 'vehicle' ] ) ) {
                            $results[ 'photo' ]     = \AAMP\API\Core::consume( 'v2/photo/vehicle/' . $results[ 'vehicle' ]->id_model, 'GET', false );
                            $results[ 'dataset' ]   = \AAMP\API\Core::consume( 'v2/product/compatible/trim/' . $results[ 'vehicle' ]->id_trim . '/' . get_field( 'prestashop_store_id', 'option' ) . '/1', 'GET', false );

                            self::$data             = $results[ 'dataset' ];
                        }
                    }
                }
            }

            return $results;
        }

        public static function format_product_url( $categories, $id_category_default, $reference, $base = 'catalog/' ) {
            if ( ( count( $categories ) ) && ( strlen( $reference ) ) ) {
                $link_rewrite       = '';

                if ( $id_category_default > 0 ) {
                    foreach ( $categories as $category ) {
                        if ( empty( $link_rewrite ) ) {
                            $link_rewrite       = $category->link_rewrite;
                        }

                        if ( $category->id_category === $id_category_default ) {
                            $link_rewrite       = $category->link_rewrite;
                            break;
                        }
                    }
                }

                if ( !empty( $link_rewrite ) ) {
                    return \AAMP\Content\URL::get_shop_url() . '/' . $base . $link_rewrite . '/' . $reference;
                }
            }

            return '';
        }

        public static function format_image_url( $images, $reference, $size = 'large_default' ) {
            if ( ( count( $images ) ) && ( strlen( $reference ) ) ) {
                $id_image       = 0;

                foreach ( $images as $image ) {
                    $id_image       = $image->id_image;

                    if ( $image->cover ) {
                        break;
                    }
                }

                if ( $id_image > 0 ) {
                    return \AAMP\Content\URL::get_shop_url() . '/' . $id_image . '-' . $size . '/' . strtolower( $reference ) . '.jpg';
                }
            }

            return false;
        }

        public static function product_category_classes( $categories ) {
            $classes        = array();

            foreach ( $categories as $category ) {
                array_push( $classes, 'category-' . $category->id_category );
            }

            return implode( ' ', $classes );
        }
    }

    siteorigin_widget_register( THEME__TEXTDOMAIN . '-vrm-lookup', __FILE__, 'Theme_VRM_Lookup' );
