aamp_admin.fn.extend.gravityforms = {
    fields: {
        init: function() {
        },

        restore: function( event, field_data, form ) {
            var fields      = jQuery( '[data-internal-key]' );
            var element, internal_key, values, counter, value_counter;

            if ( ( fields ) && ( fields.length ) ) {
                for ( counter = 0; counter < fields.length; counter++ ) {
                    element     = jQuery( fields[ counter ] );

                    if ( ( element ) && ( element.length ) ) {
                        internal_key    = element.data( 'internal-key' );

                        if ( field_data[ internal_key ] ) {
                            switch ( element.data( 'field-type' ).toLowerCase() ) {
                                case 'checkbox' :
                                case 'radio' :
                                case 'text' :
                                    // TODO
                                    break;

                                case 'select' :
                                    if ( element.attr( 'multiple' ) ) {
                                        values      = field_data[ internal_key ].split( ',' );

                                        for ( value_counter = 0; value_counter < values.length; value_counter++ ) {
                                            element.find( 'option[value="' + values[ value_counter ] + '"]' ).attr( 'selected', true );
                                        }
                                    } else {
                                        element.val( field_data[ internal_key ] );
                                    }
                                    break;
                            }
                        }

                        // for some reason, some versions of gravity forms hide custom fields with a "display: none;" style. we'll undo that here.
                        element.parent( 'li' ).show();
                    }
                }
            }
        },

        set: function( event ) {
            var element     = jQuery( event.target );

            if ( ( element ) && ( element.length ) ) {
                var internal_key    = element.data( 'internal-key' );

                switch ( element.data( 'field-type' ).toLowerCase() ) {
                    case 'checkbox' :
                    case 'radio' :
                    case 'text' :
                        // TODO
                        break;

                    case 'select' :
                        if ( element.attr( 'multiple' ) ) {
                            var values      = [];

                            element.find( 'option:selected' ).each( function() {
                                values.push( jQuery( this ).val() );
                            } );

                            SetFieldProperty( internal_key, values.join( ',' ) );
                        } else {
                            SetFieldProperty( internal_key, element.val() );
                        }
                        break;
                }
            }
        }
    }
};