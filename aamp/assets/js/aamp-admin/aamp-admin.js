var aamp_admin = {
    data: {
    },
    
    fn: {
        init: {
            start: function() {
                for ( obj in aamp_admin.fn.extend ) {
                    aamp_admin.fn.init.run( aamp_admin.fn.extend[ obj ] );
                }
            },

            run: function( tree ) {
                for ( obj in tree ) {
                    if ( typeof tree[ obj ] === 'object' ) {
                        aamp_admin.fn.init.run( tree[ obj ] );
                    } else if ( ( typeof tree[ obj ] === 'function' ) && ( obj == 'init' ) ) {
                        tree[ obj ]();
                    }
                }
            }
        },

        extend: {

        }
    }
};

jQuery( document ).ready( function() {
    aamp_admin.fn.init.start();
} );