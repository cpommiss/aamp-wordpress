if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.Core = ( function() {
    var debounce = function( func, wait, immediate ) {
        /**
         * Returns a function, that, as long as it continues to be invoked, will not
         * be triggered. The function will be called after it stops being called for
         * N milliseconds. If `immediate` is passed, trigger the function on the
         * leading edge, instead of the trailing.
         *
         * Courtesy of David Walsh: https://davidwalsh.name/javascript-debounce-function
          */

        var timeout;

        return function() {
            var context = this, args = arguments;

            var later = function() {
                timeout = null;

                if ( !immediate ) {
                    func.apply( context, args );
                }
            };

            var call_now = immediate && !timeout;

            clearTimeout( timeout );

            timeout = setTimeout( later, wait );

            if ( call_now ) {
                func.apply( context, args );
            }
        };
    };

    // queue initialization routines
    jQuery( document ).ready( function() {
        for ( var Module in AAMP ) {
            if ( ( AAMP[ Module ].hasOwnProperty( 'init' ) ) && ( typeof AAMP[ Module ].init === 'function' ) ) {
                AAMP[ Module ].init();
            }
        }
    } );

    return {
        debounce: debounce
    }
} )();
