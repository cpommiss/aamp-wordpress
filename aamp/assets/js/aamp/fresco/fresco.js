if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.Fresco = ( function() {
    var handler = function( event ) {
        event.preventDefault();

        Fresco.show(
            {
                url: jQuery( this ).attr( 'href' )
            }
        );
    };

    var init = function() {
        jQuery( 'a.fresco, a[href*="youtube.com"], a[href*="youtu.be"]' ).on( 'click', handler );
    };

    return {
        handler: handler,
        init: init
    }
} )();