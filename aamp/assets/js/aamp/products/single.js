if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.ProductsSingle = ( function() {
    var handle_quantity_click = function( event ) {
        var action      = jQuery( this ).data( 'quantity-action' );
        var quantity    = jQuery( 'input[type="number"]' );

        if ( ( quantity.length ) && ( action ) ) {
            var amount      = parseInt( quantity.val() );

            switch ( action.toLowerCase() ) {
                case 'add' :
                    if ( amount < 10 ) {
                        amount++;
                    }
                    break;

                case 'subtract' :
                    if ( amount > 1 ) {
                        amount--;
                    }
                    break;
            }

            quantity.val( amount );
        }
    };

    var init = function() {
        jQuery( '[data-quantity-action]' ).on( 'click', handle_quantity_click );
    };

    return {
        init: init
    }
} )();