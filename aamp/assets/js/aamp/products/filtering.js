if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.ProductsFiltering = ( function() {
    var products_wrapper;

    var price_range;
    var price_layered_min;
    var price_layered_max;
    var price_ranges        = {
        min: 0,
        max: 0
    };
    var reload_timeout;

    var handle_price_change = function( event ) {
        jQuery( '#layered_price_range_min' ).val( event.value.newValue[ 0 ] );
        jQuery( '#layered_price_range_max' ).val( event.value.newValue[ 1 ] );

        clearTimeout( reload_timeout );
        reload_timeout      = setTimeout( reloadContent, 125 );
    };

    var handle_price_format = function( value ) {
        if ( typeof value !== 'number' ) {
            return 'Range: $' + value[ 0 ].format( 2 ) + ' - $' + value[ 1 ].format( 2 );
        } else {
            return '$' + value.format( 2 );
        }
    };

    var handle_layout_change = function( event ) {
        event.preventDefault();

        var type                = jQuery( this ).data( 'layout-type' ).toLowerCase();

        jQuery( 'a[data-layout-type]' ).removeClass( 'active' );
        jQuery( this ).addClass( 'active' );

        products_wrapper.removeClass( function ( index, class_name ) {
            return ( class_name.match( /(^|\s)products-wrap--\S+/g) || [] ).join( ' ' );
        } );

        products_wrapper.addClass( 'products-wrap--' + type );
    };

    var set_price_starting_range = function() {
        var cur_min             = parseInt( jQuery( '#layered_price_range_min' ).val() );
        var cur_max             = parseInt( jQuery( '#layered_price_range_max' ).val() );

        price_range.bootstrapSlider( 'setValue', [ ( ( !isNaN( cur_min ) ) ? cur_min : price_ranges.min ), ( ( !isNaN( cur_max ) ) ? cur_max : price_ranges.max ) ] );
    };

    var init = function() {
        products_wrapper        = jQuery( '.products-wrap' );

        price_range             = jQuery( '#filtering__range-price' );
        price_layered_min       = jQuery( '#layered_price_range_min' );
        price_layered_max       = jQuery( '#layered_price_range_max' );

        if ( ( price_range.length ) && ( price_layered_min.length ) && ( price_layered_max.length ) && ( typeof jQuery.fn.bootstrapSlider === 'function' ) ) {
            price_ranges.min        = parseInt( price_layered_min.attr( 'limitvalue' ) );
            price_ranges.max        = parseInt( price_layered_max.attr( 'limitvalue' ) );

            price_range.bootstrapSlider( 'setAttribute', 'min', price_ranges.min );
            price_range.bootstrapSlider( 'setAttribute', 'max', price_ranges.max );
            price_range.bootstrapSlider( 'setAttribute', 'formatter', handle_price_format );

            setTimeout( set_price_starting_range, 1000 );

            price_range.on( 'change', handle_price_change );
        }

        jQuery( 'a[data-layout-type]' ).on( 'click', handle_layout_change );
    };

    return {
        init: init
    }
} )();