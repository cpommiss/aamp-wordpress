if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.ProductsCategory = ( function() {
    var toggle_category_view = function( event ) {
        event.preventDefault();

        var content     = jQuery( this ).siblings( '.widget__content' );

        if ( content.length ) {
            if ( jQuery( this ).hasClass( 'expanded' ) ) {
                jQuery( this ).removeClass( 'expanded' );

                content.slideUp( 500, 'easeOutExpo' );
            } else {
                jQuery( this ).addClass( 'expanded' );

                content.slideDown( 500, 'easeOutExpo' );
            }
        }
    };

    var init = function() {
        var products    = jQuery( '#product_list' );

        if ( ( products.length ) && ( typeof reloadContent === 'function' ) ) {
            reloadContent();

            AAMP.ProductsFiltering.init();
        }

        var categories  = jQuery( '.widget-categories' );

        if ( categories.length ) {
            jQuery( '.widget__title', categories ).on( 'click', toggle_category_view );
        }
    };

    return {
        init: init
    }
} )();