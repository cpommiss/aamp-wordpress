if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.Slick = ( function() {
    var init = function() {
        jQuery( '.image-carousel, [data-slick]' ).each(
            function() {
                if ( !jQuery( this ).hasClass( 'slick-initialized' ) ) {
                    jQuery( this ).slick();
                }
            } );
    };

    return {
        init: init
    }
} )();