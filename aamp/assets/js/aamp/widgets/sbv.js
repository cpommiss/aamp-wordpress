if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.SBV = ( function() {
    var container;
    var instance;
    var api_url;
    var store_url;
    var region;
    var mode;

    var init = function() {
        container       = jQuery( '.sbv' );

        if ( container.length ) {
            api_url         = container.data( 'api-url' );
            store_url       = container.data( 'store-url' );
            region          = parseInt( container.data( 'region' ) );

            instance        = new Vue(
                {
                    el: '.sbv',
                    data: {
                        loading: 'make',
                        make: {
                            current: null,
                            makes: []
                        },
                        model: {
                            current: null,
                            models: []
                        },
                        year: {
                            current: null,
                            years: []
                        },
                        trim: {
                            current: null,
                            trims: []
                        },
                        photo: {
                            status: 'select',
                            url: ''
                        }
                    },
                    methods: {
                        select: AAMP.SBV.select
                    }
                }
            );

            select( null );
        }
    };

    var select = function( event ) {
        var url;
        var method      = 'GET';
        var source      = '';
        var parameters  = {};

        if ( ( event ) && ( event.target ) ) {
            source          = jQuery( event.target ).attr( 'name' ).toLowerCase();
        }

        switch ( source ) {
            case 'make' :
                mode        = 'model';
                url         = 'compatible/vehicles/models/' + instance.make.current + '/region/' + region;
                break;

            case 'model' :
                mode        = 'year';
                method      = 'POST';
                url         = 'compatible/vehicles/models/' + instance.make.current + '/years/region/' + region;

                parameters.model    = instance.model.current;
                break;

            case 'year' :
                mode        = 'trim';
                url         = 'compatible/vehicles/trims/' + instance.year.current;
                break;

            default :
                mode        = 'make';
                url         = 'compatible/vehicles/makes/0/region/' + region;
                break;
        }

        instance.loading    = 'sbv__fields--loading-' + mode;

        set_state();

        if ( url ) {
            jQuery.ajax(
                api_url + url,
                {
                    method: method,
                    dataType: 'json',
                    data: parameters,
                    success: parse
                }
            );
        }
    };

    var parse = function( data ) {
        instance.loading    = '';

        switch ( mode ) {
            case 'make' :
                instance.make.makes     = data;
                instance.make.current   = '';
                break;

            case 'model' :
                instance.model.models   = data;
                instance.model.current  = '';
                break;

            case 'year' :
                instance.year.years     = data;
                instance.year.current   = '';
                break;

            case 'trim' :
                instance.trim.trims     = ( ( data.trims ) ? data.trims : [] );
                instance.trim.current   = ( ( data.trims ) ? data.trims[ 0 ].id_trim : '' );
                break;
        }
    };

    var get_photo = function() {
        jQuery.ajax(
            api_url + 'v2/photo/vehicle/' + instance.year.current,
            {
                method: 'GET',
                dataType: 'json',
                success: set_photo
            }
        );
    };

    var set_photo = function( data ) {
        if ( ( data ) && ( data.url ) ) {
            instance.photo.url      = data.url;
        } else {
            instance.photo.url      = '';
        }

        set_photo_status();
    };

    var set_photo_status = function() {
        instance.photo.status   = 'sbv__photo--' + ( ( instance.photo.url.length ) ? 'display' : ( ( mode === 'trim' ) ? 'no-image' : 'select' ) );
    };

    var set_state = function() {
        switch ( mode ) {
            case 'make' :
                instance.make.makes     = [];
                instance.model.models   = [];
                instance.year.years     = [];
                instance.trim.trims     = [];

                instance.make.current   = '';
                instance.model.current  = '';
                instance.year.current   = '';
                instance.trim.current   = '';
                break;

            case 'model' :
                instance.model.models   = [];
                instance.year.years     = [];
                instance.trim.trims     = [];

                instance.model.current  = '';
                instance.year.current   = '';
                instance.trim.current   = '';
                break;

            case 'year' :
                instance.year.years     = [];
                instance.trim.trims     = [];

                instance.year.current   = '';
                instance.trim.current   = '';
                break;

            case 'trim' :
                instance.trim.trims     = [];

                instance.trim.current   = '';
                break;
        }

        // post processing
        switch ( mode ) {
            case 'make' :
            case 'model' :
            case 'year' :
                instance.photo.url  = '';

                set_photo_status();
                break;

            case 'trim' :
                get_photo();
                break;
        }
    };

    var get_instance = function() {
        return instance;
    };

    return {
        init: init,
        select: select,
        get_instance: get_instance
    };
} )();