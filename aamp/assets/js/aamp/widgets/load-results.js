if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.LoadResults = ( function() {
    var containers      = jQuery( '[data-load]' );

    var clear_results   = false;
    var clear_timeout   = 0;

    var fetch_results = function( container ) {
        var counter;
        var dataset         = {
            action: 'aamp_fetch_posts',
            post_type: container.data( 'load' ),
            page: parseInt( container.data( 'load-current-page' ) ),
            per_page: parseInt( container.data( 'load-per-page' ) ),
            taxonomy: [],
            meta: []
        };

        var taxonomy        = jQuery( 'input[data-load-taxonomy]', container );
        var meta            = jQuery( 'input[data-load-meta]', container );

        if ( taxonomy.length ) {
            for ( counter = 0; counter < taxonomy.length; counter++ ) {
                dataset.taxonomy.push(
                    {
                        term: jQuery( taxonomy[ counter ] ).data( 'load-taxonomy' ),
                        value: jQuery( taxonomy[ counter ] ).val()
                    }
                )
            }
        }

        if ( meta.length ) {
            for ( counter = 0; counter < meta.length; counter++ ) {
                dataset.meta.push(
                    {
                        key: jQuery( meta[ counter ] ).data( 'load-meta' ),
                        value: jQuery( meta[ counter ] ).val()
                    }
                )
            }
        }

        jQuery( '[data-static-loader]', container ).show();

        jQuery.get(
            '/wp-admin/admin-ajax.php',
            dataset,
            function( data ) {
                if ( ( data ) && ( data.success === true ) ) {
                    jQuery( '[data-static-loader]', container ).hide();

                    parse_results( container, data );
                }
            },
            'json'
        );
    };

    var parse_results = function( container, data ) {
        var counter, remaining;
        var load_more       = jQuery( '[data-load-more]', container );
        var results         = jQuery( '[data-load-results]', container );

        if ( clear_results ) {
            clear_results       = false;

            jQuery( '[data-load-results] .alert', container ).remove()
        }

        load_more.find( '.load-more__label' ).text( 'Load More' );

        if ( data.dataset.length ) {
            remaining       = ( data.total - ( data.current * data.query.posts_per_page ) );

            if ( remaining < 0 ) {
                remaining       = 0;
            }

            load_more.data( 'load-more-remaining', remaining );
            load_more.find( '.load-more__remaining' ).text( remaining + ' remaining' );

            if ( data.current >= data.pages ) {
                load_more.addClass( 'hidden' );
            } else {
                load_more.removeClass( 'hidden' );
            }

            for ( counter = 0; counter < data.dataset.length; counter++ ) {
                results.append( jQuery( data.dataset[ counter ] ).data( 'result-index', counter + 1 ).addClass( 'result--show' ).addClass( 'result--index-' + ( counter + 1 ) ).clone() );
            }
        } else {
            load_more.addClass( 'hidden' );

            results.empty().append( '<div class="alert alert-info"><p>Sorry, but there is nothing to display at this time.</p></div>' );
        }
    };

    var empty_results = function( container ) {
        clear_results   = true;

        jQuery( '[data-load-results] .result', container ).removeClass( 'result--show' ).addClass( 'result--hide' );

        clearTimeout( clear_timeout );
        clear_timeout   = setTimeout( function() { jQuery( '[data-load-results] .result.result--hide', container ).remove(); }, 1000 );
    };

    var init = function() {
        var counter, container;

        if ( containers.length ) {
            for ( counter = 0; counter < containers.length; counter++ ) {
                container       = jQuery( containers[ counter ] );

                if ( window.location.hash ) {
                    // TODO: pre-select option
                }

                // add load more link
                jQuery( '[data-load-results="modal"]', container )
                    .on( 'click', '.result a', function( event ) {
                        event.preventDefault();

                        jQuery( this ).parent( '.result' ).addClass( 'result--loading' );
                        jQuery( this ).before( '<div class="loader"></div>' )

                        AAMP.Modal.load_result.call( this, event );
                    } );

                jQuery( '[data-load-results]' )
                    .after( '<a href="javascript:;" class="load-more hidden" data-load-more="true"><span class="load-more__inner"><strong><span class="load-more__label">Load More</span> <span class="load-more__remaining"></span></strong></span></a>' )
                    .after( '<div data-static-loader="true" class="loader"></div>' );

                jQuery( '[data-load-more]', container ).on( 'click', function( event ) {
                    var container       = jQuery( this ).parents( '[data-load]' );

                    container.data( 'load-current-page', parseInt( container.data( 'load-current-page' ) ) + 1 );

                    setTimeout(
                        function() {
                            fetch_results( container );
                        },
                        250
                    );
                } );
                
                jQuery( '.dropdown-menu li[data-value]', container ).on( 'click', function( event ) {
                    var container       = jQuery( this ).parents( '[data-load]' );

                    empty_results( container );

                    // reset page counter since we are changing taxonomy terms and reloading the dataset from scratch
                    container.data( 'load-current-page', 1 );

                    setTimeout(
                        function() {
                            fetch_results( container );
                        },
                        350
                    );
                } );

                jQuery( '#site-modal' ).on( 'hide.bs.modal', function( event ) {
                    // remove site loaders
                    jQuery( '.result' ).removeClass( 'result--loading' );
                    jQuery( '.result .loader' ).remove();
                } );

                fetch_results( container );
            }
        }
    };

    return {
        init: init
    };
} )();