if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.CategoryFiltering = ( function() {
    var container;
    var categories  = [];

    var classes = {
        selected: 'category-filter__item--active'
    };

    var handler = function( event ) {
        event.preventDefault();

        // erase our category list
        categories      = [];

        var category    = parseInt( jQuery( this ).data( 'category-id' ) );
        var products    = jQuery( '.products__tile' );
        var target      = jQuery( this ).parent( 'li' );
        var root        = jQuery( this ).parents( '.category-filter--root' );
        var root_link   = jQuery( '.category-filter__item--all' );

        if ( ( target ) && ( root ) && ( root_link ) ) {
            if ( ( !isNaN( category ) ) && ( category > 0 ) ) {
                root_link.removeClass( classes.selected );

                if ( target.hasClass( classes.selected ) ) {
                    target.removeClass( classes.selected );
                    target.find( 'li' ).removeClass( classes.selected );
                } else {
                    target.addClass( classes.selected );
                    target.find( 'li' ).addClass( classes.selected );
                }

                jQuery( root ).find( '.' + classes.selected ).each( function() {
                    var id_category     = parseInt( jQuery( this ).children( 'a' ).data( 'category-id' ) );

                    if ( ( !isNaN( id_category ) ) && ( id_category > 0 ) ) {
                        categories.push( id_category );
                    }
                } );
            } else {
                jQuery( '.category-filter__item' ).removeClass( classes.selected );

                root_link.addClass( classes.selected );
            }

            products.removeClass( 'products__tile--active' ).addClass( 'products__tile--hidden' );

            if ( categories.length ) {
                for ( var counter = 0; counter < categories.length; counter++ ) {
                    jQuery( '.products__tile.category-' + categories[ counter ] ).removeClass( 'products__tile--hidden' ).addClass( 'products__tile--active' );
                }
            } else {
                products.removeClass( 'products__tile--hidden' ).addClass( 'products__tile--active' );
            }
        }
    };

    var init = function() {
        jQuery( '.category-filter__link' ).on( 'click', handler );
    };

    return {
        init: init,
        settings: {
            categories: categories
        }
    };
} )();