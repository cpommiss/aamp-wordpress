if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.Menus = ( function() {
    var init = function() {
        jQuery( '.sub-menu-toggle' ).on( 'click', function( event ) {
            var parent      = jQuery( this ).parent( '.nav-item' );

            if ( parent.hasClass( 'nav-item--expanded' ) ) {
                parent.removeClass( 'nav-item--expanded' );

                jQuery( this ).siblings( '.sub-menu' ).slideUp( 350, 'easeOutExpo' );
            } else {
                parent.addClass( 'nav-item--expanded' );

                jQuery( this ).siblings( '.sub-menu' ).slideDown( 350, 'easeOutExpo' );
            }
        } );

        jQuery( '[data-toggle]' ).on( 'click', function( event ) {
            event.preventDefault();

            var target      = jQuery( this ).data( 'toggle' );

            if ( ( target ) && ( target.length ) ) {
                var element     = jQuery( target );

                if ( element.length ) {
                    if ( element.hasClass( 'visible' ) ) {
                        element.removeClass( 'visible' );
                    } else {
                        element.addClass( 'visible' );
                    }
                }
            }
        } );
    };

    return {
        init: init
    }
} )();