if ( typeof AAMP !== 'object' ) var AAMP = {};

AAMP.Modal = ( function() {
    var load_result = function( event ) {
        event.preventDefault();

        var url     = jQuery( this ).attr( 'href' );

        if ( url.length ) {
            jQuery.get( url,
                function( content ) {
                    if ( content.length ) {
                        var section     = jQuery( '.post-content', content ).html();

                        if ( ( section ) && ( section.length ) ) {
                            jQuery( '#site-modal .modal-content__inner' ).html( section );
                            jQuery( '#site-modal' ).modal( 'show' );

                            setTimeout( function() {
                                AAMP.Slick.init();
                            }, 250 );
                        }
                    }
                },
                'html' );
        }
    };

    var init = function() {
    };

    return {
        init: init,
        load_result: load_result
    }
} )();