<?php
    /**
     * Theme Constants
     *
     * @package WordPress
     * @subpackage AAMP Global
     * @since 1.0
     */

    define( 'CHILD__NAME', 'AAMP Global' );
    define( 'CHILD__TEXTDOMAIN', 'aampglobal' );
    define( 'CHILD__VERSION', '1.0.0' );

