<?php
    /**
     * Theme Initialization
     *
     * @package WordPress
     * @subpackage AAMP Global
     * @since 1.0
     */

    /**
     * Initialize theme.
     *
     * @since 1.0
     */
    function __child_setup() {
        load_theme_textdomain( CHILD__TEXTDOMAIN );

        $sizes      = array(
        );

        foreach ( $sizes as $size => $details ) {
            add_image_size( CHILD__TEXTDOMAIN . '-' . $size, $details[ 'width' ], $details[ 'height' ], $details[ 'crop' ] );

            if ( $details[ '2x' ] ) {
                add_image_size( CHILD__TEXTDOMAIN . '-' . $size . '-2x', $details[ 'width' ] * 2, $details[ 'height' ] * 2, $details[ 'crop' ] );
            }
        }

        register_nav_menus( array(
                                'secondary'     => __( 'Secondary Navigation Menu', CHILD__TEXTDOMAIN )
                            ) );
    }
    add_action( 'after_setup_theme', '__child_setup', 10 );

    require get_stylesheet_directory() . '/include/setup/shortcodes.php';
    require get_stylesheet_directory() . '/include/library/AAMPGlobal/Core.php';

    new AAMPGlobal\Core;
