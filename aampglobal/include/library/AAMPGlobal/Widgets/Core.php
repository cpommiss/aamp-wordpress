<?php

    /**
     * Widget Area-related Methods
     *
     * @package WordPress
     * @subpackage AAMP Global
     * @since 1.0
     */
    namespace AAMPGlobal\Widgets;

    class Core {
        function __construct() {
            add_action( 'widgets_init', array( '\AAMPGlobal\Widgets\Core', 'init' ) );
        }

        function __destruct() {
        }

        /**
         * Register widget area.
         *
         * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
         *
         * @since 1.0
         */
        public static function init() {
        }
    }