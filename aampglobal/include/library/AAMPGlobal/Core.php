<?php

    /**
     * Initialization
     *
     * @package WordPress
     * @subpackage AAMP Global
     * @since 1.0
     */
    namespace AAMPGlobal;

    class Core {
        function __construct() {
            // include composer autoload
            $vendor_autoload    = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

            if ( file_exists( $vendor_autoload ) ) {
                require $vendor_autoload;
            }

            // register autoloader
            spl_autoload_register(
                function( $class ) {
                    $path       = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . str_replace( '\\', '/', $class ) . '.php';

                    if ( file_exists( $path ) ) {
                        require_once $path;
                    }
                }
            );

            // post types
            new PT\Core;

            // assets
            new Assets\Core;

            // widgets
            new Widgets\Core;

            // siteorigin
            new SiteOrigin\Core;
        }

        function __destruct() {
        }
    }
