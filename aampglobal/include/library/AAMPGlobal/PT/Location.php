<?php

    /**
     * PnT (Post and Taxonomies)-related methods
     * Locations
     *
     * @package WordPress
     * @subpackage AAMPGlobal
     * @since 1.0
     */
    namespace AAMPGlobal\PT;

    class Location {
        public static $post_types = array(
            'location' => array(
                'labels'    => array(
                    'name' => 'Locations',
                    'singular_name' => 'Location'
                ),
                'options' => array(
                    'label' => 'Locations',
                    'description' => 'Locations around the world where AAMP operates',
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'show_in_rest' => false,
                    'rest_base' => '',
                    'has_archive' => false,
                    'show_in_menu' => true,
                    'exclude_from_search' => false,
                    'capability_type' => 'post',
                    'map_meta_cap' => true,
                    'hierarchical' => false,
                    'rewrite' => array( 'slug' => 'location', 'with_front' => true ),
                    'query_var' => true,
                    'menu_icon' => 'dashicons-admin-site',
                    'supports' => array( 'title', 'editor', 'excerpt', 'revisions' )
                )
            )
        );

        public static $taxonomies   = array(
        );

        public static $image_sizes  = array(
        );
    }
