<?php

    /**
     * PnT (Post and Taxonomies)-related methods
     * Team Members
     *
     * @package WordPress
     * @subpackage AAMPGlobal
     * @since 1.0
     */
    namespace AAMPGlobal\PT;

    class Team {
        public static $post_types = array(
            'team' => array(
                'labels'    => array(
                    'name' => 'Team Members',
                    'singular_name' =>'Team Member'
                ),
                'options' => array(
                    'label' => 'Team Members',
                    'description' => 'AAMP Global Leadership Team Members',
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'show_in_rest' => false,
                    'rest_base' => '',
                    'has_archive' => false,
                    'show_in_menu' => true,
                    'exclude_from_search' => false,
                    'capability_type' => 'post',
                    'map_meta_cap' => true,
                    'hierarchical' => false,
                    'rewrite' => array( 'slug' => 'team', 'with_front' => true ),
                    'query_var' => true,
                    'menu_icon' => 'dashicons-groups',
                    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
                )
            )
        );

        public static $taxonomies   = array(
        );

        public static $image_sizes  = array(
        );
    }
