<?php

    /**
     * PnT (Post and Taxonomies)-related methods
     *
     * @package WordPress
     * @subpackage AAMPGlobal
     * @since 1.0
     */
    namespace AAMPGlobal\PT;

    class Core {
        function __construct() {
            add_action( 'init', array( '\AAMPGlobal\PT\Core', 'add_post_types' ) );
        }

        public static function add_post_types() {
            foreach ( new \DirectoryIterator( dirname( __FILE__ ) ) as $file ) {
                if ( ( $file->isDot() ) || ( strtolower( $file->getBasename( '.php' ) ) === 'core' ) ) continue;

                array_push( \AAMP\PT\Core::$queue, array( 'class_name' => 'AAMPGlobal', 'post_type' => $file->getBasename( '.php' ) ) );
            }

            foreach ( \AAMP\PT\Core::$queue as $item ) {
                require_once $item[ 'post_type' ] . '.php';
            }
        }
    }
