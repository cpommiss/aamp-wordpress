<?php

    /**
     * JS Core framework-related Methods
     *
     * @package WordPress
     * @subpackage AAMP Global
     * @since 1.0
     */
    namespace AAMPGlobal\JSCore;

    class Core {
        function __construct() {
        }

        function __destruct() {
        }

        /**
         * Enqueues core JS files from both the main theme directory
         * and the child theme directory (when applicable).
         *
         * Searches for .js files in the <theme dir>/assets/js/<theme>/ directory
         * within both the theme and the child theme and automatically
         * enqueues them.
         *
         * @since 1.0
         */
        public static function enqueue( $load_in_footer = true ) {
            $queue = array();
            $paths = array();

            array_push(
                $paths,
                array(
                    'path'              => get_stylesheet_directory(),
                    'is_base_template'  => true
                )
            );

            foreach ( $paths as $path ) {
                $assets_path    = $path[ 'path' ] . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . strtolower( CHILD__TEXTDOMAIN ) . ( ( is_admin() ) ? '-admin' : '' ) . DIRECTORY_SEPARATOR;

                if ( file_exists( $assets_path ) ) {
                    $directory      = new \RecursiveDirectoryIterator( $assets_path, \RecursiveDirectoryIterator::SKIP_DOTS );
                    $iterator       = new \RecursiveIteratorIterator( $directory, \RecursiveIteratorIterator::SELF_FIRST );
                    $matches        = new \RegexIterator( $iterator, '/^.+\.js$/i', \RecursiveRegexIterator::GET_MATCH );

                    foreach ( $matches as $asset ) {
                        if ( is_array( $asset ) ) {
                            array_push(
                                $queue,
                                array(
                                    'path'             => $asset[ 0 ],
                                    'is_base_template' => $path[ 'is_base_template' ],
                                    'is_core'          => ( ( pathinfo( $asset[ 0 ], PATHINFO_FILENAME ) === ( strtolower( CHILD__TEXTDOMAIN ) . ( ( is_admin() ) ? '-admin' : '' ) ) ) ? true : false ),
                                    'load_first'       => ( ( $path[ 'is_base_template' ] ) ? true : false )
                                )
                            );
                        }
                    }
                }
            }

            /**
             * sort assets so that files that exist
             * in the /assets/js/<theme>/ base folder are loaded BEFORE any files in subfolders.
             *
             * this is necessary to ensure the namespace has been created in the DOM
             */
            uasort(
                $queue,
                function ( $left, $right ) use ( $paths ) {
                    return ( ( ( $left[ 'is_core' ] ) || ( $left[ 'is_base_template' ] ) ) ? -1 : 1 );
                }
            );

            foreach ( $queue as $asset ) {
                $version     = date( 'ymd-Gis', filemtime( $asset[ 'path' ] ) );
                $handle      = ( ( $asset[ 'is_core' ] ) ? CHILD__TEXTDOMAIN . '-core' : CHILD__TEXTDOMAIN . '-' . pathinfo( $asset[ 'path' ], PATHINFO_BASENAME ) . '-' . filemtime( $asset[ 'path' ] ) );
                $uri_partial = str_replace( get_stylesheet_directory(), '', $asset[ 'path' ] );

                wp_enqueue_script( $handle, get_stylesheet_directory_uri() . $uri_partial, ( ( $asset[ 'is_core' ] ) ? array( 'jquery' ) : array( 'jquery', CHILD__TEXTDOMAIN . '-core' ) ), ( CHILD__TEXTDOMAIN . '-' . $version ), $load_in_footer );
            }
        }
    }

