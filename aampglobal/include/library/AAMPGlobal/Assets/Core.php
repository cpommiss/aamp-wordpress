<?php

    /**
     * Asset-related Methods
     *
     * @package WordPress
     * @subpackage AAMP Global
     * @since 1.0
     */
    namespace AAMPGlobal\Assets;

    class Core {
        function __construct() {
            add_action( 'wp_enqueue_scripts', array( '\AAMPGlobal\Assets\Core', 'enqueue_public' ) );
            add_action( 'admin_enqueue_scripts', array( '\AAMPGlobal\Assets\Core', 'enqueue_admin' ) );
        }

        function __destruct() {
        }

        /**
         * Enqueue scripts and styles.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               x
         *
         * @since 1.0
         */
        public static function enqueue_public() {
            wp_enqueue_style( CHILD__TEXTDOMAIN . '-theme', get_stylesheet_directory_uri() . '/assets/css/theme.css', array( THEME__TEXTDOMAIN . '-bootstrap', THEME__TEXTDOMAIN . '-theme' ), CHILD__VERSION );
            wp_enqueue_style( CHILD__TEXTDOMAIN . '-ammap-export', get_template_directory_uri() . '/assets/vendor/amcharts-maps/ammap/plugins/export/export.css', false, '1.4.74' );

            wp_enqueue_script( CHILD__TEXTDOMAIN . '-gmapi', 'https://maps.googleapis.com/maps/api/js?key=' . \AAMP\Customizer\Core::get( 'gis_gmaps_api_key' ), false, '3.0', true );

            wp_enqueue_script( CHILD__TEXTDOMAIN . '-ammap', get_template_directory_uri() . '/assets/vendor/amcharts-maps/ammap/ammap.js', false, '3.21.12', true );
            wp_enqueue_script( CHILD__TEXTDOMAIN . '-ammap-world', get_template_directory_uri() . '/assets/vendor/amcharts-maps/ammap/maps/js/worldLow.js', false, '3.21.12', true );
            wp_enqueue_script( CHILD__TEXTDOMAIN . '-ammap-export', get_template_directory_uri() . '/assets/vendor/amcharts-maps/ammap/plugins/export/export.min.js', false, '1.4.74', true );
            wp_enqueue_script( CHILD__TEXTDOMAIN . '-ammap-theme', get_template_directory_uri() . '/assets/vendor/amcharts-maps/ammap/themes/light.js', false, '3.21.12', true );

            \AAMPGlobal\JSCore\Core::enqueue();
        }

        /**
         * Enqueue administration/dashboard scripts and styles.
         *
         * @since 1.0
         */
        public static function enqueue_admin( $hook ) {
            \AAMPGlobal\JSCore\Core::enqueue( false );
        }
    }

