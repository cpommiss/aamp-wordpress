<nav class="navbar navbar-expand-lg">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#primary-nav" aria-controls="primary-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar"></span>
        <span class="navbar-toggler-bar"></span>
        <span class="navbar-toggler-bar"></span>
    </button>

    <?php get_template_part( 'parts/navigation/navigation', 'logo' ); ?>

    <div class="collapse navbar-collapse" id="primary-nav">
        <?php
            wp_nav_menu(
                array(
                    'theme_location'    => 'primary',
                    'container'         => false,
                    'menu_class'        => 'navbar-nav',
                    'walker'            => new \AAMP_Nav_Walker_Default()
                )
            )
        ?>

        <?php get_template_part( 'parts/navigation/navigation', 'social' ); ?>
    </div>
</nav>