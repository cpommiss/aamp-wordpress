<?php if ( \AAMP\Header\Core::is_title_visible() ) : ?>
<header class="page-heading" style="background-image: url(<?php echo \AAMP\Header\FeaturedImage::get_featured_image( get_the_ID() ); ?>);">
    <?php echo \AAMP\Header\Core::build_title(); ?>
</header>
<?php endif; ?>
