if ( typeof AAMPGlobal !== 'object' ) var AAMPGlobal = {};

AAMPGlobal.TeamMembers = ( function() {
    var modal;

    var handle = function( event ) {
        event.preventDefault();

        var card    = jQuery( this );
        var html    =   '<div class="modal-content__inner">' +
                            '<div class="team">' +
                                '<div class="team__card-imagery">' + card.find( '.team__card-imagery--candid' ).clone().wrap('<div>').parent().html() + '</div>' +
                                '<div class="team__card-details">' +
                                    card.find( '.team__card-header' ).clone().wrap('<div>').parent().html() +
                                    card.find( '.team__card-content' ).clone().wrap('<div>').parent().html() +
                                '</div>' +
                            '</div>' +
                        '</div>';

        modal.find( '.modal-content' ).html( html );
        modal.modal( 'show' );
    };

    var init = function() {
        modal       = jQuery( '.modal' );

        jQuery( '.team__card' ).on( 'click', handle );
    };

    return {
        init: init
    };
} )();