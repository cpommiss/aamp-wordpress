if ( typeof AAMPGlobal !== 'object' ) var AAMPGlobal = {};

AAMPGlobal.Locations = ( function() {
    var maps        = [];
    var settings    = {
        'type': 'map',
        'theme': 'light',
        'projection': 'miller',
        'imagesSettings': {
            'rollOverColor': '#ffffff',
            'rollOverScale': 3,
            'selectedScale': 3,
            'selectedColor': '#ffffff',
            'color': '#000'
        },
        'areasSettings': {
            'unlistedAreasColor': '#ffffff',
            'unlistedAreasAlpha': 1
        },
        'dataProvider': {
            'map': 'worldLow',
            'images': []
        }
    };

    var timers      = {
        hide_tooltip: null
    };

    var init = function() {
        var elements            = jQuery( '.location-map' );

        if ( elements.length ) {
            elements.each( function() {
                var id      = jQuery( this ).attr( 'id' );
                var config  = settings;

                jQuery( this ).data( 'direction-y', 0 );
                jQuery( this ).data( 'direction-x', 0 );

                var markers = jQuery( this ).siblings( '.locations' );

                if ( markers.length ) {
                    markers.find( '.locations__poi' ).each( function() {
                        var id          = parseInt( jQuery( this ).data( 'id' ) );
                        var title       = jQuery( this ).find( '.locations__poi-title' );
                        var purpose     = jQuery( this ).find( '.locations__poi-function' );
                        var cover       = jQuery( this ).data( 'image-cover' );
                        var latitude    = parseFloat( jQuery( this ).data( 'address-lat' ) );
                        var longitude   = parseFloat( jQuery( this ).data( 'address-lng' ) );

                        config.dataProvider.images.push(
                            {
                                'zoomLevel': 5,
                                'scale': 1,
                                'id': id,
                                'title': ( title ? title.text() : '' ),
                                'purpose': ( purpose ? purpose.text() : '' ),
                                'cover': cover,
                                'latitude': latitude,
                                'longitude': longitude
                            }
                        );
                    } );
                }

                var map     = AmCharts.makeChart( id, config );

                map.addListener( 'positionChanged', update_markers );
                map.addListener( 'init', function( event ) {
                    jQuery( event.chart.chartDiv ).addClass( 'amcharts-chart-div--initialized' );
                    setTimeout( initialize_markers, 1000, event );
                } );

                maps.push( map );

                jQuery( this ).siblings( '.locations' ).on( 'click', '.locations__poi-close', contract_display );
                jQuery( this ).on( 'click', '.map-marker__tooltip', expand_display );
            } );
        }
    };

    var initialize_markers = function( event ) {
        var map     = event.chart;

        for ( var index in map.dataProvider.images ) {
            var image   = map.dataProvider.images[ index ];

            jQuery( '.map-marker[data-id="' + image.id + '"]' ).addClass( 'map-marker--initialized' );
        }
    };

    var update_markers = function( event ) {
        var map     = event.chart;

        for ( var index in map.dataProvider.images ) {
            var image   = map.dataProvider.images[ index ];

            if ( 'undefined' == typeof image.externalElement ) {
                image.externalElement = create_custom_marker( image );
            }

            var coords  = map.coordinatesToStageXY( image.longitude, image.latitude );

            image.externalElement.style.top     = coords.y + 'px';
            image.externalElement.style.left    = coords.x + 'px';
        }
    };

    var toggle_tooltip = function( event ) {
        clearTimeout( timers.hide_tooltip );

        var element = jQuery( event.target ).parents( '.map-marker' );

        if ( element.length ) {
            element.siblings( '.map-marker' ).removeClass( 'map-marker--open' );

            switch ( event.type.toLowerCase() ) {
                case 'click' :
                    element.toggleClass( 'map-marker--open' );
                    break;

                case 'mouseover' :
                    element.addClass( 'map-marker--open' );
                    break;

                case 'mouseout' :
                    timers.hide_tooltip     = setTimeout( hide_all_tooltips, 300, event );
                    break;
            }
        }
    };

    var hide_all_tooltips = function( event ) {
        var element = jQuery( event.target ).parents( '.location-map' );

        if ( element.length ) {
            element.find( '.map-marker' ).removeClass( 'map-marker--open' );
        }
    };

    var expand_display = function( event ) {
        var element = jQuery( event.target ).parents( '.map-marker' );

        if ( ( element.length ) && ( element.hasClass( 'map-marker--large' ) ) ) {
            var id          = parseInt( element.data( 'id' ) );

            element.addClass( 'map-marker--sticky' );

            if ( ( !isNaN( id ) ) && ( id > 0 ) ) {
                var content     = jQuery( '.locations__poi[data-id="' + id + '"]' );
                var map         = element.parents( '.location-map' );

                if ( ( content.length ) && ( map.length ) ) {
                    if ( !element.hasClass( 'map-marker--expanded' ) ) {
                        element.addClass( 'map-marker--expanded' );

                        content
                            .css(
                                {
                                    left: ( element.offset().left - ( 200 / 2 ) ) + 'px',
                                    top: ( element.offset().top - 165 ) + 'px'
                                }
                            )
                            .addClass( 'locations__poi--showing' );

                        setTimeout(
                            function() {
                                content
                                    .css(
                                        {
                                            left: 0 + 'px',
                                            top: map.offset().top + 'px'
                                        }
                                    )
                                    .removeClass( 'locations__poi--showing' )
                                    .addClass( 'locations__poi--open' );
                            },
                            250
                        );
                    }
                }
            }
        }
    };

    var contract_display = function( event ) {
        event.preventDefault();

        var content = jQuery( event.target ).parents( '.locations__poi' );

        if ( ( content.length ) && ( content.hasClass( 'locations__poi--open' ) ) ) {
            var id          = parseInt( content.data( 'id' ) );

            if ( ( !isNaN( id ) ) && ( id > 0 ) ) {
                var element     = jQuery( '.map-marker[data-id="' + id + '"]' );
                var map         = element.parents( '.location-map' );

                if ( ( element.length ) && ( map.length ) ) {
                    if ( element.hasClass( 'map-marker--expanded' ) ) {
                        setTimeout( function() { element.removeClass( 'map-marker--expanded' ); }, 500 );

                        content
                            .removeClass( 'locations__poi--open' )
                            .addClass( 'locations__poi--hiding' );

                        setTimeout(
                            function() {
                                content
                                    .css(
                                        {
                                            left: ( element.offset().left - ( 200 / 2 ) ) + 'px',
                                            top: ( element.offset().top - 165 ) + 'px'
                                        }
                                    )
                                    .removeClass( 'locations__poi--hiding' )
                                    .addClass( 'locations__poi--closing' )
                            },
                            750
                        );

                        setTimeout(
                            function() {
                                content
                                    .removeClass( 'locations__poi--closing' );
                            },
                            1250
                        );

                        setTimeout(
                            function() {
                                element
                                    .removeClass( 'map-marker--sticky' );
                            },
                            2350
                        );
                    }
                }
            }
        }
    };

    var create_custom_marker = function( image ) {
        var marker  = jQuery( '<div>' );
        var dot     = jQuery( '<div>' );
        var tooltip = jQuery( '<div>' );
        var cover   = jQuery( '<div>' );

        marker
            .addClass( 'map-marker' )
            .attr( 'aria-hidden', 'true' )
            .attr( 'data-id', image.id );

        if ( image.cover.length ) {
            marker
                .addClass( 'map-marker--large' );

            tooltip
                .addClass( 'map-marker__tooltip--card' );

            cover
                .addClass( 'map-marker__tooltip-image' )
                .css( 'background-image', 'url(' + image.cover + ')' );

            tooltip
                .append( cover );
        }

        tooltip
            .addClass( 'map-marker__tooltip' )
            .append( '<span class="map-marker__tooltip-title">' + image.title + '</span>' )
            .append( '<span class="map-marker__tooltip-purpose">' + image.purpose + '</span>' );

        dot
            .addClass( 'map-marker__dot' )
            .on( 'click mouseover mouseout', toggle_tooltip );

        tooltip
            .on( 'mouseover mouseout', toggle_tooltip );

        marker
            .append( dot )
            .append( tooltip );

        image.chart.chartDiv.appendChild( marker[ 0 ] );

        return marker[ 0 ];
    };

    return {
        settings: {
            maps: maps,
            settings: settings
        },
        init: init
    };
} )();