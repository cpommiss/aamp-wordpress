if ( typeof AAMPGlobal !== 'object' ) var AAMPGlobal = {};

AAMPGlobal.Core = ( function() {
    // queue initialization routines
    jQuery( document ).ready( function() {
        for ( var Module in AAMPGlobal ) {
            if ( ( AAMPGlobal[ Module ].hasOwnProperty( 'init' ) ) && ( typeof AAMPGlobal[ Module ].init === 'function' ) ) {
                AAMPGlobal[ Module ].init();
            }
        }
    } );

    return {
    }
} )();
