<?php $team = \AAMPGlobal_Team_Members::get_team_members(); ?>
<?php if ( count( $team ) ) : ?>
<div class="team__cards row">
    <?php foreach ( $team as $member ) : ?>
    <div class="team__card col-xl-3 col-lg-4 col-md-6 col-sm-12">
        <div class="team__card-inner">
            <header class="team__card-header">
                <h4 class="team__card-header-title">
                    <?php echo $member[ 'name' ]; ?>

                    <?php if ( !empty( $member[ 'title' ] ) ) : ?><span class="team__card-header-subtitle"><?php echo $member[ 'title' ]; ?></span><?php endif; ?>
                    <?php if ( !empty( $member[ 'tagline' ] ) ) : ?><span class="team__card-header-tagline"><?php echo $member[ 'tagline' ]; ?></span><?php endif; ?>
                </h4>
            </header>

            <section class="team__card-content">
                <?php echo $member[ 'biography' ]; ?>
            </section>

            <div class="team__card-imagery">
                <div class="team__card-imagery--serious"<?php if ( !empty( $member[ 'imagery' ][ 'serious' ] ) ) : ?> style="background-image: url(<?php echo wp_get_attachment_image_url( $member[ 'imagery' ][ 'serious' ], 'full' ); ?>);"<?php endif; ?>></div>
                <div class="team__card-imagery--candid"<?php if ( !empty( $member[ 'imagery' ][ 'candid' ] ) ) : ?> style="background-image: url(<?php echo wp_get_attachment_image_url( $member[ 'imagery' ][ 'candid' ], 'full' ); ?>);"<?php endif; ?>></div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>