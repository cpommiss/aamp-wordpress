<?php

    /*
    Widget Name: AAMP Global - Team Member Grid
    Description: Creates a grid containing the team members defined within the "Team Member" custom post type.
    */

    class AAMPGlobal_Team_Members extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-team-members',

                __( CHILD__NAME . ' - Team Member Grid', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates a grid containing the team members entered into the "Team Member" custom post type.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }

        public static function get_team_members() {
            $dataset    = array();
            $query      =   new WP_Query(
                                array(
                                    'post_type' => 'team',
                                    'post_status' => 'publish',
                                    'orderby' => 'menu_order',
                                    'order' => 'ASC',
                                    'posts_per_page' => -1
                                )
                            );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();

                    array_push( $dataset,
                                array(
                                    'name'      => get_the_title( get_the_ID() ),
                                    'title'     => get_field( 'team_title', get_the_ID() ),
                                    'tagline'   => get_field( 'team_tagline', get_the_ID() ),
                                    'biography' => wpautop( get_the_content() ),
                                    'imagery'   => array(
                                        'serious'   => get_field( 'team_portrait_serious', get_the_ID() ),
                                        'candid'    => get_field( 'team_portrait_candid', get_the_ID() )
                                    )
                                ) );
                }

                wp_reset_query();
                wp_reset_postdata();
            }

            return $dataset;
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-team-members', __FILE__, 'AAMPGlobal_Team_Members' );
