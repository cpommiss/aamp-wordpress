<?php

    /*
    Widget Name: AAMP Global - Location Map
    Description: Creates an interactive map containing the locations defined within the "Locations" custom post type.
    */

    class AAMPGlobal_Location_Map extends SiteOrigin_Widget {
        function __construct() {
            parent::__construct(
                CHILD__TEXTDOMAIN . '-location-map',

                __( CHILD__NAME . ' - Location Map', CHILD__TEXTDOMAIN ),

                array(
                    'description' => __( 'Creates an interactive map containing the locations defined within the "Locations" custom post type.', CHILD__TEXTDOMAIN )
                ),

                array(),

                array(
                ),

                plugin_dir_path( __FILE__ )
            );
        }

        function get_template_name( $instance ) {
            return 'default';
        }

        function get_template_dir( $instance ) {
            return 'templates';
        }

        public static function get_locations() {
            $dataset    = array();
            $query      =   new WP_Query(
                                array(
                                    'post_type'     => 'location',
                                    'post_status'   => 'publish',
                                    'orderby' => 'menu_order',
                                    'order' => 'ASC',
                                    'posts_per_page' => -1
                                )
                            );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();

                    array_push( $dataset,
                                array(
                                    'id'        => get_the_ID(),
                                    'title'     => get_the_title(),
                                    'function'  => get_field( 'location_title' ),
                                    'tagline'   => get_field( 'location_tagline' ),
                                    'address'   => get_field( 'location_physical' ),
                                    'content'   => get_the_content(),
                                    'url'       => get_the_permalink(),
                                    'imagery'   => array(
                                        'cover'     => ( ( $id_image_cover = get_field( 'location_image_cover' ) ) ? wp_get_attachment_image_url( $id_image_cover, 'medium' ) : '' ),
                                        'bg'        => ( ( $id_image_bg = get_field( 'location_image_bg' ) ) ? wp_get_attachment_image_url( $id_image_bg, 'full' ) : '' )
                                    )
                                ) );
                }

                wp_reset_postdata();
                wp_reset_query();
            }

            return $dataset;
        }
    }

    siteorigin_widget_register( CHILD__TEXTDOMAIN . '-location-map', __FILE__, 'AAMPGlobal_Location_Map' );
