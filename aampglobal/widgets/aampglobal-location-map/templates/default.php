<?php $dataset = \AAMPGlobal_Location_Map::get_locations(); ?>
<div class="locations">
    <?php foreach ( $dataset as $location ) : ?>
        <?php if ( !empty( $location[ 'address' ] ) ) : ?>
        <div class="locations__poi<?php echo ( ( !empty( $location[ 'imagery' ][ 'bg' ] ) ) ? ' locations__poi--large' : '' ); ?>" data-id="<?php echo $location[ 'id' ]; ?>" data-address-lat="<?php echo $location[ 'address' ][ 'lat' ]; ?>" data-address-lng="<?php echo $location[ 'address' ][ 'lng' ]; ?>" data-image-cover="<?php echo $location[ 'imagery' ][ 'cover' ]; ?>" data-image-bg="<?php echo $location[ 'imagery' ][ 'bg' ]; ?>"<?php if ( !empty( $location[ 'imagery' ][ 'bg' ] ) ) : ?> style="background-image: url(<?php echo $location[ 'imagery' ][ 'bg' ]; ?>);"<?php endif; ?>>
            <h3 class="locations__poi-title"><?php echo $location[ 'title' ]; ?></h3>
            <p class="locations__poi-function"><?php echo $location[ 'function' ]; ?></p>
            <?php if ( strlen( $location[ 'tagline' ] ) ) : ?><p class="locations__poi-tagline"><?php echo $location[ 'tagline' ]; ?></p><?php endif; ?>
            <a href="javascript:;" class="locations__poi-close" aria-hidden="true"><?php _e( 'Close', CHILD__TEXTDOMAIN ); ?></a>

            <div class="locations__poi-content"><?php echo $location[ 'content' ]; ?></div>
        </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
<div id="<?php echo 'map-' . uniqid(); ?>" class="location-map"></div>
