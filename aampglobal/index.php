<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <article class="container">
        <div class="row">
            <div class="content col-lg-8 col-md-12">
                <?php if ( is_active_sidebar( 'sidebar-blog-before' ) ) : ?>
                <aside class="sidebar-before">
                    <?php dynamic_sidebar( 'sidebar-blog-before' ); ?>
                </aside>
                <?php endif; ?>

                <section data-load="post" data-load-per-page="10" data-load-current-page="1" class="section post-content post-blocks">
                    <div data-load-results="default" class="post-content__results">
                    </div>
                </section>
            </div>
            <div class="sidebar col-lg-4 col-md-12">
                <?php dynamic_sidebar( 'sidebar-primary' ); ?>
            </div>
        </div>

        <?php if ( is_active_sidebar( 'sidebar-blog-after' ) ) : ?>
        <br />
        <hr />
        <br />

        <div class="row">
            <div class="col-lg-12">
                <aside class="sidebar-after">
                    <?php dynamic_sidebar( 'sidebar-blog-after' ); ?>
                </aside>
            </div>
        </div>
        <?php endif; ?>
    </article>
<?php else : ?>
    <?php get_template_part( 'parts/none/none', 'post' ); ?>
<?php endif; ?>

<?php get_footer(); ?>